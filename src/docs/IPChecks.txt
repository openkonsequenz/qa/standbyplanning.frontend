/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
==============================================================
Status of the IPChecks (may not be up to date)
==============================================================

"dependencies": {
    "@angular/animations": "^6.1.0"                 - requested
    "@angular/common": "^6.1.0"                     - requested
    "@angular/compiler": "^6.1.0"                   - requested
    "@angular/core": "^6.1.0"                       - requested
    "@angular/forms": "^6.1.0"                      - requested
    "@angular/http": "^6.1.0"                       - requested
    "@angular/platform-browser": "^6.1.0"           - requested
    "@angular/platform-browser-dynamic": "^6.1.0"   - requested
    "@angular/router": "^6.1.0"                     - requested
    "@auth0/angular-jwt": "^2.0.0"                  - requested
    "@ng-bootstrap/ng-bootstrap": "^2.2.0"          - passed
    "ag-grid": "^18.0.1"                            - passed
    "ag-grid-angular": "^18.0.1"                    - passed
    "bootstrap": "^4.1.1"                           - passed
    "classlist.js": "^1.1.20150312"                 - already there - piggyback
    "core-js": "^2.5.4"                             - requested
    "font-awesome": "^4.7.0"                        - passed
    "primeicons": "^1.0.0-beta.9"                   - requested
    "primeng": "^6.0.0"                             - passed
    "rxjs": "^6.2.2"                                - requested
    "web-animations-js": "^2.3.1"                   - requested
    "zone.js": "^0.8.26"                            - already there - piggyback
  },
  "devDependencies": {
    "@angular/compiler-cli": "^6.1.0"               - MIT
    "@angular-devkit/build-angular": "~0.6.3"       - MIT
    "typescript": "~2.7.2"                          - Apache 2.0
    "@angular/cli": "~6.1.1"                        - MIT
    "@angular/language-service": "^6.1.0"           - MIT
    "@types/jasmine": "~2.8.6"                      - MIT
    "@types/jasminewd2": "~2.0.3"                   - MIT
    "@types/node": "~10.5.2"                        - MIT
    "codelyzer": "~4.4.2"                           - MIT
    "jasmine-core": "~3.1.0"                        - MIT
    "jasmine-spec-reporter": "~4.2.1"               - MIT
    "karma": "~2.0.4"                               - MIT
    "karma-chrome-launcher": "~2.2.0"               - MIT
    "karma-coverage-istanbul-reporter": "~2.0.1"    - MIT
    "karma-jasmine": "~1.1.1"                       - MIT
    "karma-jasmine-html-reporter": "^1.2.0"         - MIT
    "protractor": "~5.3.0"                          - MIT
    "ts-node": "~7.0.0"                             - MIT
    "tslint": "~5.10.0"                             - Apache 2.0
  }
