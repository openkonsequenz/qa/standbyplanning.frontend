/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CanDeactivateGuard } from '@core/guards/can-deactivate.guard';
import { AuthGuard } from '@core/guards/auth.guard';
import { RoleGuard } from '@core/guards/role.guard';

import { OverviewComponent } from '@masterdata/components/overview/overview.component';
import { UserComponent } from '@masterdata/components/user/user.component';
import { UserlistComponent } from '@masterdata/components/userlist/userlist.component';
import { LocationlistComponent } from '@masterdata/components/locationlist/locationlist.component';
import { LocationComponent } from '@masterdata/components/location/location.component';
import { RegionlistComponent } from '@masterdata/components/regionlist/regionlist.component';
import { RegionComponent } from '@masterdata/components/region/region.component';
import { FunctionlistComponent } from '@masterdata/components/functionlist/functionlist.component';
import { FunctionComponent } from '@masterdata/components/function/function.component';
import { StandbygrouplistComponent } from '@masterdata/components/standbygrouplist/standbygrouplist.component';
import { StandbygroupComponent } from '@masterdata/components/standbygroup/standbygroup.component';
import { OrganisationComponent } from '@masterdata/components/organisation/organisation.component';
import { OrganisationlistComponent } from '@masterdata/components/organisationlist/organisationlist.component';
import { StandbylisttableComponent } from '@masterdata/components/standbylisttable/standbylisttable.component';
import { StandbylistComponent } from '@masterdata/components/standbylist/standbylist.component';
import { CalendarlistComponent } from '@masterdata/components/calendarlist/calendarlist.component';
import { CalendarComponent } from '@masterdata/components/calendar/calendar.component';

export const routes: Routes = [
  {
    path: 'stammdatenverwaltung',
    component: OverviewComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin', 'BP_Gruppenleiter']
    }
  },
  {
    path: 'stammdatenverwaltung/mitarbeiter',
    component: UserlistComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin', 'BP_Gruppenleiter']
    }
  },
  {
    path: 'stammdatenverwaltung/mitarbeiter/:id',
    component: UserComponent,
    canDeactivate: [CanDeactivateGuard],
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin', 'BP_Gruppenleiter']
    }
  },
  {
    path: 'stammdatenverwaltung/lokation',
    component: LocationlistComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin', 'BP_Gruppenleiter']
    }
  },
  {
    path: 'stammdatenverwaltung/lokation/:id',
    component: LocationComponent,
    canDeactivate: [CanDeactivateGuard],
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin', 'BP_Gruppenleiter']
    }
  },
  {
    path: 'stammdatenverwaltung/region',
    component: RegionlistComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin', 'BP_Gruppenleiter']
    }
  },
  {
    path: 'stammdatenverwaltung/region/:id',
    component: RegionComponent,
    canDeactivate: [CanDeactivateGuard],
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin', 'BP_Gruppenleiter']
    }
  },
  {
    path: 'stammdatenverwaltung/funktion',
    component: FunctionlistComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin', 'BP_Gruppenleiter']
    }
  },
  {
    path: 'stammdatenverwaltung/funktion/:id',
    component: FunctionComponent,
    canDeactivate: [CanDeactivateGuard],
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin', 'BP_Gruppenleiter']
    }
  },
  {
    path: 'stammdatenverwaltung/bereitschaftsgruppe',
    component: StandbygrouplistComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin', 'BP_Gruppenleiter']
    }
  },
  {
    path: 'stammdatenverwaltung/bereitschaftsgruppe/:id',
    component: StandbygroupComponent,
    canDeactivate: [CanDeactivateGuard],
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin', 'BP_Gruppenleiter']
    }
  },
  {
    path: 'stammdatenverwaltung/organisation',
    component: OrganisationlistComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin', 'BP_Gruppenleiter']
    }
  },
  {
    path: 'stammdatenverwaltung/organisation/:id',
    component: OrganisationComponent,
    canDeactivate: [CanDeactivateGuard],
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin', 'BP_Gruppenleiter']
    }
  },
  {
    path: 'stammdatenverwaltung/bereitschaftsliste',
    component: StandbylisttableComponent,
    canDeactivate: [CanDeactivateGuard],
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin', 'BP_Gruppenleiter']
    }
  },
  {
    path: 'stammdatenverwaltung/bereitschaftsliste/:id',
    component: StandbylistComponent,
    canDeactivate: [CanDeactivateGuard],
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin', 'BP_Gruppenleiter']
    }
  },
  {
    path: 'stammdatenverwaltung/kalender',
    component: CalendarlistComponent,
    canDeactivate: [CanDeactivateGuard],
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin', 'BP_Gruppenleiter']
    }
  },
  {
    path: 'stammdatenverwaltung/kalender/:id',
    component: CalendarComponent,
    canDeactivate: [CanDeactivateGuard],
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin', 'BP_Gruppenleiter']
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterdataManagementRoutingModule { }
