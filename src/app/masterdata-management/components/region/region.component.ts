/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subject, Subscription, Observable } from 'rxjs';
import { Params } from '@angular/router';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { RegionObject } from '@shared/model/RegionObject';
import { LocationObject } from '@shared/model/LocationObject';
import { FormUtil } from '@shared/utils/form.util';
import { AbstractFormComponent } from '@shared/abstract/abstract-form/abstract-form.component';
import { AuthenticationService } from '@core/services/authentication.service';

@Component({
  selector: 'ok-region',
  templateUrl: './region.component.html',
  styleUrls: ['./region.component.scss']
})
export class RegionComponent extends AbstractFormComponent implements OnInit, OnDestroy {
  decisionModalRef: NgbModalRef;
  decision = new Subject<boolean>();
  modalAction = new Subject<string>();
  isModal = false;
  instanceId: number;

  /**
   * Location
   */
  sourceLocation: Array<LocationObject> = [];
  targetLocation: Array<LocationObject> = [];

  sourceColumnDefsLocation = [];
  targetColumnDefsLocation = [];


  columnDefsLocation = [
    { headerName: 'Titel', field: 'title' },
    { headerName: 'Lokation', field: 'district' },
    { headerName: 'Gemeinde', field: 'community' }
  ];

  param$: Subscription;
  regionData$: Subscription;
  locationData$: Subscription;
  constructor(
    public authService: AuthenticationService,
    private masterDataService: MasterdataService,
    private injector: Injector
  ) {
    super(injector);
  }

  ngOnInit() {
    this.createForm();
    this.param$ = this.route.params.subscribe((params: Params) => {
      this.instanceId = params['id'];
      if (this.instanceId) {
        this.regionData$ = this.masterDataService.getRegion(this.instanceId).subscribe(
          regionRes => {
            this.form.patchValue(regionRes);
            this.targetLocation = regionRes.lsLocations;
            this.getLocationList();
          }
        );
      }
    });
  }

  getLocationList(): void {
    this.locationData$ = this.masterDataService.getLocationDataSelection().subscribe((locationRes) => {
      this.sourceLocation = FormUtil.getSubsetOfArray(locationRes, this.targetLocation, 'id', 'id');
    });
  }

  createForm() {
    this.form = this.fb.group({
      id: '',
      regionName: ['', Validators.required],
      lsLocations: ''
    });
  }
  /**
   * Save and Validation Methods
   */

  /**
   * Saves the current Form
   */
  saveRegion() {
    if (FormUtil.validate(this.form)) {
      const regionToSave = this.form.getRawValue();
      this.masterDataService.saveRegion(regionToSave).subscribe((res: RegionObject) => {
        // neccessary due to canDeactivate guard
        FormUtil.markAsPristineAndUntouched(this.form);
        if (this.isModal) {
          this.router.navigate(['/stammdatenverwaltung/region', res.id]);
          this.modalAction.next('close');
        } else {
          this.router.navigate(['/stammdatenverwaltung/region']);
        }
      });
      return true;
    }
    return false;
  }

  /**
   * MOVE FUNCTIONS LOCATION
   */

  moveToTargetLocation(dataToMove) {
    this.masterDataService.addRegionLocation(this.instanceId, dataToMove).subscribe((resLocation: LocationObject[]) => {
      this.targetLocation = resLocation;
      this.sourceLocation = FormUtil.getSubsetOfArray(this.sourceLocation, this.targetLocation, 'id', 'id');

      this.messageService.add({ severity: 'success', summary: 'Lokationen wurden gespeichert', detail: '' });
    });
  }

  moveToSourceLocation(dataToMove) {
    this.masterDataService.deleteRegionLocation(this.instanceId, dataToMove).subscribe((resLocation: LocationObject[]) => {
      this.targetLocation = resLocation;
      this.getLocationList();

      this.messageService.add({ severity: 'success', summary: 'Lokationen wurden gespeichert', detail: '' });
    });
  }

  close() {
    if (this.isModal) {
      this.modalAction.next('close');
    } else {
      this.router.navigate(['/stammdatenverwaltung/region']);
    }
  }

  ngOnDestroy() {
    if (this.param$) {
      this.param$.unsubscribe();
    }
    if (this.regionData$) {
      this.regionData$.unsubscribe();
    }
    if (this.locationData$) {
      this.locationData$.unsubscribe();
    }
  }
}
