/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { FunctionObject } from '@shared/model/FunctionObject';
import { AGGRID_LOCALETEXT, DEFAULT_COL_DEF } from '@shared/utils/list.util';
import { AuthenticationService } from '@core/services/authentication.service';

@Component({
  selector: 'ok-functionlist',
  templateUrl: './functionlist.component.html',
  styleUrls: ['./functionlist.component.scss']
})
export class FunctionlistComponent implements OnInit, OnDestroy {
  defaultColDef = DEFAULT_COL_DEF;
  columnDefs = [
    { headerName: 'Funktionsname', field: 'functionName' }
  ];

  localeText = AGGRID_LOCALETEXT;

  rowData = [];
  userData$: Subscription;

  constructor(
    public authService: AuthenticationService,
    private masterDataService: MasterdataService,
    private router: Router
  ) { }

  ngOnInit() {

  }

  onGridReady(params) {
    this.userData$ = this.masterDataService.getFunctionData().subscribe((res: FunctionObject[]) => {
      this.rowData = res;
    });
    params.api.sizeColumnsToFit();
  }

  rowClicked(event) {
    this.router.navigate(['stammdatenverwaltung/funktion', event.data.functionId]);
  }

  ngOnDestroy() {
    if (this.userData$) {
      this.userData$.unsubscribe();
    }
  }

}
