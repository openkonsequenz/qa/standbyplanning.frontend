/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subject, Subscription } from 'rxjs';
import { Params } from '@angular/router';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { LocationObject } from '@shared/model/LocationObject';
import { PostcodeObject } from '@shared/model/PostcodeObject';
import { FormUtil } from '@shared/utils/form.util';
import { BranchObject } from '@shared/model/BranchObject';
import { RegionObject } from '@shared/model/RegionObject';
import { AbstractFormComponent } from '@shared/abstract/abstract-form/abstract-form.component';
import { AuthenticationService } from '@core/services/authentication.service';


@Component({
  selector: 'ok-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent extends AbstractFormComponent implements OnInit, OnDestroy {
  decisionModalRef: NgbModalRef;
  decision = new Subject<boolean>();

  modalAction = new Subject<string>();
  isModal = false;
  instanceId: number;
  branchModalRef: NgbModalRef;

  /**
   * Postcodes
   */
  sourcePLZ: PostcodeObject[] = [];
  targetPLZ: PostcodeObject[] = [];


  sourceColumnDefsPLZ = [
    { headerName: 'Postleitzahl', field: 'pcode' }
  ];
  targetColumnDefsPLZ = [
    { headerName: 'Postleitzahl', field: 'pcode' }
  ];

  /**
  * Branches
  */
  sourceBranch: Array<BranchObject> = [];
  targetBranch: Array<BranchObject> = [];

  sourceColumnDefsBranch = [
    { headerName: 'Name', field: 'title' }
  ];
  targetColumnDefsBranch = [
    { headerName: 'Name', field: 'title' }
  ];

  /**
   * Regions
   */
  sourceRegion: Array<any> = [];
  targetRegion: Array<any> = [];
  regionModalRef: NgbModalRef;

  sourceColumnDefsRegion = [
    { headerName: 'Regionsname', field: 'regionName' },
    { headerName: 'Funktionsname', field: 'functionName' }
  ];
  targetColumnDefsRegion = [
    { headerName: 'Regionsname', field: 'regionName' }
  ];

  param$: Subscription;
  user$: Subscription;
  postcode$: Subscription;
  branch$: Subscription;
  region$: Subscription;
  constructor(
    public authService: AuthenticationService,
    private masterDataService: MasterdataService,
    private injector: Injector
  ) {
    super(injector);
  }

  ngOnInit() {
    this.createForm();
    this.param$ = this.route.params.subscribe((params: Params) => {
      this.instanceId = params['id'];
      if (this.instanceId) {
        this.user$ = this.masterDataService.getLocation(this.instanceId).subscribe(
          locationRes => {
            this.form.patchValue(locationRes);
            this.targetPLZ = locationRes.lsPostcode;
            this.targetBranch = locationRes.lsLocationForBranches;
            this.targetRegion = locationRes.lsRegions;
            this.getPostcodeList();
            this.getBranchList();
            this.getRegionList();
          }
        );
      }
    });
  }

  createForm() {
    this.form = this.fb.group({
      id: '',
      title: '',
      district: ['', Validators.required],
      shorttext: '',
      community: '',
      wgs84zonedistrict: '',
      latitudedistrict: '',
      longitudedistrict: ''
    });
  }

  getBranchList(): void {
    this.branch$ = this.masterDataService.getBranchDataSelection().subscribe((branchRes: BranchObject[]) => {
      this.sourceBranch = FormUtil.getSubsetOfArray(branchRes, this.targetBranch, 'id', 'branchId');
    });
  }

  getPostcodeList(): void {
    this.postcode$ = this.masterDataService.getPostcodeData().subscribe((postcodeRes: PostcodeObject[]) => {
      this.sourcePLZ = FormUtil.getSubsetOfArray(postcodeRes, this.targetPLZ, 'id', 'id');
    });
  }

  getRegionList(): void {
    this.region$ = this.masterDataService.getRegionDataSelection().subscribe((regionRes: RegionObject[]) => {
      this.sourceRegion = FormUtil.getSubsetOfArray(regionRes, this.targetRegion, 'id', 'id');
    });
  }

  /**
   * Save and Validation Methods
   */

  /**
   * Saves the current Form
   */
  saveLocation() {
    if (FormUtil.validate(this.form)) {
      const locationToSave = this.form.getRawValue();
      this.masterDataService.saveLocation(locationToSave).subscribe((res: LocationObject) => {
        // neccessary due to canDeactivate guard
        FormUtil.markAsPristineAndUntouched(this.form);
        if (this.isModal) {
          this.router.navigate(['/stammdatenverwaltung/lokation', res.id]);
          this.modalAction.next('close');
        } else {
          this.router.navigate(['/stammdatenverwaltung/lokation']);
        }
      });
      return true;
    }
    return false;
  }

  /**
   * MOVE FUNCTIONS BRANCHES
   */

  moveToTargetBranch(dataToMove) {
    for (let i = 0; i < dataToMove.length; i++) {
      dataToMove[i].locationId = this.instanceId;
      dataToMove[i].branchId = dataToMove[i].id;
      delete dataToMove[i].id;
    }
    this.masterDataService.addLocationBranch(this.instanceId, dataToMove).subscribe((resBranch: BranchObject[]) => {
      this.targetBranch = resBranch;
      this.sourceBranch = FormUtil.getSubsetOfArray(this.sourceBranch, this.targetBranch, 'id', 'branchId');

      this.messageService.add({ severity: 'success', summary: 'Sparten wurden gespeichert', detail: '' });
    });
  }

  moveToSourceBranch(dataToMove) {
    this.masterDataService.deleteLocationBranch(this.instanceId, dataToMove).subscribe((resBranch: BranchObject[]) => {
      this.targetBranch = resBranch;
      this.getBranchList();

      this.messageService.add({ severity: 'success', summary: 'Sparten wurden gespeichert', detail: '' });
    });
  }

  /**
   * MOVE FUNCTIONS REGION
   */

  moveToTargetRegion(dataToMove) {
    this.masterDataService.addLocationRegion(this.instanceId, dataToMove).subscribe((resRegionFunction: RegionObject[]) => {
      this.targetRegion = resRegionFunction;
      this.sourceRegion = FormUtil.getSubsetOfArray(this.sourceRegion, this.targetRegion, 'id', 'id');

      this.messageService.add({ severity: 'success', summary: 'Regionen wurden gespeichert', detail: '' });
    });
  }

  moveToSourceRegion(dataToMove) {
    this.masterDataService.deleteLocationRegion(this.instanceId, dataToMove).subscribe((resRegionFunction: RegionObject[]) => {
      this.targetRegion = resRegionFunction;
      this.getRegionList();

      this.messageService.add({ severity: 'success', summary: 'Regionen wurden gespeichert', detail: '' });
    });
  }

  /**
   * MOVE FUNCTIONS POSTCODE
   */

  moveToTargetPostcode(dataToMove) {
    this.masterDataService.addPostcodes(this.instanceId, dataToMove).subscribe((resPostcode: PostcodeObject[]) => {
      this.targetPLZ = resPostcode;
      this.sourcePLZ = FormUtil.getSubsetOfArray(this.sourcePLZ, this.targetPLZ, 'id', 'id');

      this.messageService.add({ severity: 'success', summary: 'Postleitzahlen wurden gespeichert', detail: '' });
    });
  }

  moveToSourcePostcode(dataToMove) {
    this.masterDataService.deletePostcodes(this.instanceId, dataToMove).subscribe((resPostcode: PostcodeObject[]) => {
      this.targetPLZ = resPostcode;
      this.getPostcodeList();

      this.messageService.add({ severity: 'success', summary: 'Postleitzahlen wurden gespeichert', detail: '' });
    });
  }

  close() {
    if (this.isModal) {
      this.modalAction.next('close');
    } else {
      this.router.navigate(['/stammdatenverwaltung/lokation']);
    }
  }

  ngOnDestroy() {
    if (this.param$) {
      this.param$.unsubscribe();
    }
    if (this.user$) {
      this.user$.unsubscribe();
    }
    if (this.postcode$) {
      this.postcode$.unsubscribe();
    }
  }
}
