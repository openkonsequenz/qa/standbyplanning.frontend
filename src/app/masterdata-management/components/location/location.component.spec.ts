/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { ActivatedRoute, Routes, Router } from '@angular/router';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { SharedModule } from '@shared/shared.module';
import { LocationMockObjects } from '@shared/testing/location';
import { LocationComponent } from '@masterdata/components/location/location.component';
import { AlertComponent } from '@shared/components/alert/alert.component';
import { MessageService } from 'primeng/components/common/messageservice';
import { MasterdataManagementModule } from '@masterdata/masterdata-management.module';

const locationMockObjects = new LocationMockObjects;

export class MasterDataServiceMock {
  saveLocation() {
    return of(locationMockObjects.LOCATION_OBJECT);
  } // or whatever dummy state value you want to use
  getLocation() {
    return of(locationMockObjects.LOCATION_OBJECT);
  }
  getPostcodeData() {
    return of(locationMockObjects.POSTCODE_ARRAY);
  }
  getBranchDataSelection() {
    return of(locationMockObjects.BRANCH_ARRAY);
  }
  getRegionDataSelection() {
    return of(locationMockObjects.REGION_SELECTION_ARRAY);
  }
  addLocationRegion() {
    return of(locationMockObjects.REGION_SELECTION_ARRAY);
  }
  deleteLocationRegion() {
    return of(locationMockObjects.REGION_SELECTION_ARRAY);
  }
  addPostcodes() {
    return of(locationMockObjects.POSTCODE_ARRAY);
  }
  deletePostcodes() {
    return of(locationMockObjects.POSTCODE_ARRAY);
  }
  addLocationBranch() {
    return of(locationMockObjects.BRANCH_ARRAY);
  }
  deleteLocationBranch() {
    return of(locationMockObjects.BRANCH_ARRAY);
  }
}

describe('LocationComponent', () => {
  let component: LocationComponent;
  let fixture: ComponentFixture<LocationComponent>;
  const router = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    const routes: Routes = [
      {
        path: '**',
        component: AlertComponent
      }
    ];
    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        SharedModule,
        MasterdataManagementModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        {
          provide: MasterdataService,
          useClass: MasterDataServiceMock
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ id: 123 })
          }
        },
        { provide: Router, useValue: router },
        MessageService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationComponent);
    component = fixture.componentInstance;
    component.targetPLZ = [{ id: 1, pcode: '33100' }];
    fixture.detectChanges();
  });

  it('should create', () => {
    component.targetPLZ = [{ id: 1, pcode: '33100' }];
    expect(component).toBeTruthy();
  });

  describe('validateAllFormFields', () => {
    it('should not save the form if there are validation errors', () => {
      expect(component.saveLocation()).toBeFalsy();
    });

    it('should save the form if there are no validation errors', () => {
      const formGroup: FormGroup = new FormGroup({
        title: new FormControl(''),
        district: new FormControl('test', Validators.required),
        shorttext: new FormControl(''),
        community: new FormControl('')
      });
      component.form = formGroup;
      expect(component.saveLocation()).toBeTruthy();
    });

    it('should save the form if the component is opened in a modal and navigate away', () => {
      const formGroup: FormGroup = new FormGroup({
        title: new FormControl(''),
        district: new FormControl('test', Validators.required),
        shorttext: new FormControl(''),
        community: new FormControl('')
      });
      component.form = formGroup;
      component.isModal = true;
      component.saveLocation();
      expect(router.navigate).toHaveBeenCalledWith(['/stammdatenverwaltung/lokation', 1]);
    });
  });

  describe('close()', () => {
    it('should navigate away when the component is not a modal', () => {
      component.isModal = false;
      component.close();
      expect(router.navigate).toHaveBeenCalledWith(['/stammdatenverwaltung/lokation']);
    });

    it('should dispatch an event with content "close" if it is a modal and close() is called', () => {
      component.isModal = true;
      component.modalAction.subscribe((res) => {
        expect(res).toBe('close');
      });
      component.close();
    });
  });

  describe('move functions region', () => {
    it('should move regions from source to target', () => {
      component.moveToTargetRegion(locationMockObjects.REGION_SELECTION_ARRAY);
      expect(component.targetRegion.length).toBeGreaterThan(0);
    });

    it('should move regions from target to source', () => {
      component.moveToSourceRegion(locationMockObjects.REGION_SELECTION_ARRAY);
      expect(component.sourceRegion.length).toBe(0);
    });
  });

  describe('move functions postcode', () => {
    it('should move postcodes from source to target', () => {
      component.moveToTargetPostcode(locationMockObjects.POSTCODE_ARRAY);
      expect(component.targetPLZ.length).toBeGreaterThan(0);
    });

    it('should move postcodes from target to source', () => {
      component.moveToSourcePostcode(locationMockObjects.POSTCODE_ARRAY);
      expect(component.sourcePLZ.length).toBe(0);
    });
  });

  describe('move functions branch', () => {
    it('should move branch from source to target', () => {
      component.moveToTargetBranch(locationMockObjects.BRANCH_ARRAY);
      expect(component.targetBranch.length).toBeGreaterThan(0);
    });

    it('should move branch from target to source', () => {
      component.moveToSourceBranch(locationMockObjects.BRANCH_ARRAY);
      expect(component.sourceBranch.length).toBe(3);
    });
  });
});
