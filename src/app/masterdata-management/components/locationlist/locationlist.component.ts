/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { LocationObject } from '@shared/model/LocationObject';
import { AGGRID_LOCALETEXT, DEFAULT_COL_DEF } from '@shared/utils/list.util';
import { LocationComponent } from '@masterdata/components/location/location.component';
import { AuthenticationService } from '@core/services/authentication.service';

@Component({
  selector: 'ok-locationlist',
  templateUrl: './locationlist.component.html',
  styleUrls: ['./locationlist.component.scss']
})
export class LocationlistComponent implements OnInit, OnDestroy {
  defaultColDef = DEFAULT_COL_DEF;
  columnDefs = [
    { headerName: 'Titel', field: 'title' },
    { headerName: 'Lokation', field: 'district' },
    { headerName: 'Kürzel', field: 'shorttext' },
    { headerName: 'Gemeinde', field: 'community' },
    { headerName: 'Geografische Zone', field: 'wgs84zonedistrict' },
    { headerName: 'Geografische Breite', field: 'latitudedistrict' },
    { headerName: 'Geografische Länge', field: 'longitudedistrict' }
  ];

  localeText = AGGRID_LOCALETEXT;

  rowData = [];
  locationData$: Subscription;
  modalRef: NgbModalRef;

  constructor(
    public authService: AuthenticationService,
    private masterDataService: MasterdataService,
    private router: Router,
    private modalService: NgbModal
  ) { }

  ngOnInit() {

  }

  onGridReady(params) {
    this.locationData$ = this.masterDataService.getLocationData().subscribe((res: LocationObject[]) => {
      this.rowData = res;
    });
    params.api.sizeColumnsToFit();
  }

  rowClicked(event) {
    this.router.navigate(['stammdatenverwaltung/lokation', event.data.id]);
  }

  ngOnDestroy() {
    if (this.locationData$) {
      this.locationData$.unsubscribe();
    }
  }

  openModal() {
    this.modalRef = this.modalService.open(LocationComponent, { size: 'lg', backdrop: 'static' });
    this.modalRef.componentInstance.isModal = true;
    const modalAction: Subscription = this.modalRef.componentInstance.modalAction.subscribe((res) => {
      if (res === 'close') {
        this.modalRef.close();
        modalAction.unsubscribe();
      }
    });
  }

}
