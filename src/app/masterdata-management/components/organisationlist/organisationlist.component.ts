/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { AGGRID_LOCALETEXT, DEFAULT_COL_DEF } from '@shared/utils/list.util';
import { OrganisationObject } from '@shared/model/OrganisationObject';
import { AuthenticationService } from '@core/services/authentication.service';

@Component({
  selector: 'ok-organisationlist',
  templateUrl: './organisationlist.component.html',
  styleUrls: ['./organisationlist.component.scss']
})
export class OrganisationlistComponent implements OnInit, OnDestroy {
  defaultColDef = DEFAULT_COL_DEF;
  columnDefs = [
    { headerName: 'Organisationsname', field: 'orgaName' },
    { headerName: 'PLZ', field: 'address.postcode' },
    { headerName: 'Ort', field: 'address.community' },
    { headerName: 'Straße', field: 'address.street' },
    { headerName: 'Hausnummer', field: 'address.housenumber' },
  ];

  localeText = AGGRID_LOCALETEXT;

  rowData: OrganisationObject[] = [];
  organisationData$: Subscription;

  constructor(
    public authService: AuthenticationService,
    private masterDataService: MasterdataService,
    private router: Router
  ) { }

  ngOnInit() {

  }

  onGridReady(params) {
    this.organisationData$ = this.masterDataService.getOrganisationData().subscribe((res: OrganisationObject[]) => {
      this.rowData = res;
    });
    params.api.sizeColumnsToFit();
  }

  rowClicked(event) {
    this.router.navigate(['stammdatenverwaltung/organisation', event.data.id]);
  }

  ngOnDestroy() {
    if (this.organisationData$) {
      this.organisationData$.unsubscribe();
    }
  }

}
