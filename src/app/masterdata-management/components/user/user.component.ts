/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy, Injector } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subject, Subscription } from 'rxjs';
import { Params } from '@angular/router';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { UserObject } from '@shared/model/UserObject';
import { FormUtil } from '@shared/utils/form.util';
import { RegionObject } from '@shared/model/RegionObject';
import { FunctionObject } from '@shared/model/FunctionObject';
import { OrganisationObject } from '@shared/model/OrganisationObject';
import { AbstractFormComponent } from '@shared/abstract/abstract-form/abstract-form.component';
import { DatepickerValidator } from '@shared/validators/datepicker.validator';
import { AuthenticationService } from '@core/services/authentication.service';
import { LoosingDataDialogComponent } from '@masterdata/selection-dialogs/loosing-data-dialog/loosing-data-dialog.component';

@Component({
  selector: 'ok-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent extends AbstractFormComponent implements OnInit, OnDestroy {
  decisionModalRef: NgbModalRef;
  decision = new Subject<boolean>();

  modalAction = new Subject<string>();
  isModal = false;
  instanceId: number;
  organisationList: Array<OrganisationObject>;

  /**
   * Regions
   */
  sourceRegion: Array<any> = [];
  targetRegion: Array<any> = [];
  region$: Subscription;
  regionModalRef: NgbModalRef;

  sourceColumnDefsRegion = [
    { headerName: 'Regionsname', field: 'regionName' }
  ];
  targetColumnDefsRegion = [
    { headerName: 'Regionsname', field: 'regionName' }
  ];

  /**
   * Functions
   */
  sourceFunction: Array<FunctionObject> = [];
  targetFunction: Array<any> = [];
  functionData$: Subscription;
  functionModalRef: NgbModalRef;

  sourceColumnDefsFunction = [
    { headerName: 'Funktionsname', field: 'functionName' }
  ];
  targetColumnDefsFunction = [
    { headerName: 'Funktionsname', field: 'functionName' }
  ];

  param$: Subscription;
  user$: Subscription;
  organisation$: Subscription;
  constructor(
    public authService: AuthenticationService,
    private masterDataService: MasterdataService,
    private injector: Injector
  ) {
    super(injector);
  }

  ngOnInit() {
    if (this.isModal) {
      this.createFormModal();
    } else {
      this.createForm();
    }
    this.param$ = this.route.params.subscribe((params: Params) => {
      this.instanceId = params['id'];
      if (this.instanceId) {
        this.user$ = this.masterDataService.getUser(this.instanceId).subscribe(
          data => {
            data.date = {
              validFrom: this.ngbDateParserFormatter.parse(data.validFrom),
              validTo: this.ngbDateParserFormatter.parse(data.validTo)
            };
            this.form.patchValue(data);
            this.targetRegion = data.lsUserInRegions;
            this.targetFunction = data.lsUserFunctions;
            this.getRegionList();
            this.getFunctionList();
            this.getOrganisationList();
            if (data.organisation) {
              this.getOrganisationAndPatch(data.organisation.id);
            }
            this.form.controls.organisation.get('id').valueChanges.subscribe((changedValue) => {
              this.getOrganisationAndPatch(changedValue);
            });
          }
        );
      }
    });
  }

  getRegionList(): void {
    this.region$ = this.masterDataService.getRegionDataSelection().subscribe((regionRes: RegionObject[]) => {
      this.sourceRegion = FormUtil.getSubsetOfArray(regionRes, this.targetRegion, 'id', 'regionId');
    });
  }

  getFunctionList(): void {
    this.functionData$ = this.masterDataService.getFunctionDataSelection().subscribe((functionRes: FunctionObject[]) => {
      this.sourceFunction = FormUtil.getSubsetOfArray(functionRes, this.targetFunction, 'functionId', 'functionId');
    });
  }

  getOrganisationList(): void {
    this.organisation$ = this.masterDataService.getOrganisationDataSelection().subscribe((organisationRes: OrganisationObject[]) => {
      this.organisationList = organisationRes;
    });
  }

  getOrganisationAndPatch(id: number) {
    if (id) {
      this.organisation$ = this.masterDataService.getOrganisation(id).subscribe(
        (organisationRes: OrganisationObject) => {
          this.form.controls.organisation.get('address').patchValue(organisationRes.address);
        });
    } else {
      this.form.controls.organisation.get('address').reset();
    }
  }

  createForm() {
    this.form = this.fb.group({
      id: '',
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      isCompany: false,
      organisation: this.createOrganisation(),
      businessContactData: this.createBusinessContact(),
      privateContactData: this.createPrivateContact(),
      privateAddress: this.createAddress(),
      date: this.fb.group({
        validFrom: ['', Validators.required],
        validTo: ['', Validators.required],
      }, { validator: [DatepickerValidator.dateRangeTo('')] }),
      modificationDate: '',
      notes: '',
      hrNumber: '',
      userKey: ''
    });
  }

  createFormModal() {
    this.form = this.fb.group({
      id: '',
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      isCompany: false,
      date: this.fb.group({
        validFrom: ['', Validators.required],
        validTo: ['', [Validators.required, DatepickerValidator.dateRangeTo('validFrom')]],
      }, { validator: [DatepickerValidator.dateRangeTo('')] }),
      notes: '',
      hrNumber: '',
      userKey: ''
    });
  }
  /**
   * Save and Validation Methods
   */

  /**
   * Saves the current Form
   */
  saveUser() {
    if (this.form.controls.organisation) {
      this.form.controls.organisation.get('address').clearValidators();
    }
    if (FormUtil.validate(this.form)) {
      const userToSave = this.form.getRawValue();
      FormUtil.formatDates(userToSave, ['validFrom', 'validTo']);
      this.masterDataService.saveUser(userToSave).subscribe((res: UserObject) => {
        // neccessary due to canDeactivate guard
        FormUtil.markAsPristineAndUntouched(this.form);
        if (this.isModal) {
          this.router.navigate(['/stammdatenverwaltung/mitarbeiter', res.id]);
          this.modalAction.next('close');
        } else {
          this.router.navigate(['/stammdatenverwaltung/mitarbeiter']);
        }
      });
      return true;
    }
    return false;
  }

  /**
   * Anonymize the current Form
   */
  anonymizeUser() {

    if (this.form.controls.organisation) {
      this.form.controls.organisation.get('address').clearValidators();
    }
    if (FormUtil.validate(this.form)) {
      const userId: number = this.form.get('id').value;

      this.masterDataService.anonymizeUser(userId).subscribe((res: UserObject) => {
        // neccessary due to canDeactivate guard
        FormUtil.markAsPristineAndUntouched(this.form);
        if (this.isModal) {
          this.router.navigate(['/stammdatenverwaltung/mitarbeiter', res.id]);
          this.modalAction.next('close');
        } else {
          this.router.navigate(['/stammdatenverwaltung/mitarbeiter']);
        }
      });
      return true;
    }
    return false;
  }

  anonymizeDialog() {
    const userModalWarningRef = this.modalService.open(LoosingDataDialogComponent);
    userModalWarningRef.componentInstance.decision.subscribe(resUserModal => {
      if (resUserModal) {
        this.anonymizeUser();
      }
      userModalWarningRef.close();
    });
  }

  /**
   * ****Helpermethods for creating the form****
   */

  /**
   * Creates the Formgroup BusinessContact
   */
  createBusinessContact() {
    return this.fb.group({
      id: '',
      phone: ['', Validators.required],
      cellphone: '',
      radiocomm: '',
      email: '',
      pager: '',
      isPrivate: false
    });
  }

  /**
   * Creates the Formgroup PrivateContact
   */
  createPrivateContact() {
    return this.fb.group({
      id: '',
      phone: ['', Validators.required],
      cellphone: '',
      isPrivate: true
    });
  }

  /**
   * Creates the Formgroup Organisation
   */
  createOrganisation() {
    const organisationControl: FormGroup = this.fb.group({
      id: ['', Validators.required],
      orgaName: '',
      address: this.createAddress(),
    });
    organisationControl.controls['address'].disable();
    return organisationControl;
  }

  /**
   * Creates the Formgroup Addresses
   */
  createAddress() {
    return this.fb.group({
      id: '',
      postcode: ['', Validators.required],
      community: ['', Validators.required],
      communitySuffix: '',
      street: ['', Validators.required],
      housenumber: ['', Validators.required],
      wgs84zone: '',
      latitude: '',
      longitude: ''
    });
  }

  /**
   * Sets the dates to default values
   */
  setDefaultDate(field: string) {
    FormUtil.setDefaultDate(this.form, field);
  }

  /**
   * MOVE FUNCTIONS REGION
   */

  moveToTargetRegion(dataToMove) {
    for (let i = 0; i < dataToMove.length; i++) {
      dataToMove[i].userId = this.instanceId;
      dataToMove[i].regionId = dataToMove[i].id;
      delete dataToMove[i].id;
    }
    this.masterDataService.addUserRegion(this.instanceId, dataToMove).subscribe((resRegionFunction: RegionObject[]) => {
      this.targetRegion = resRegionFunction;
      this.sourceRegion = FormUtil.getSubsetOfArray(this.sourceRegion, this.targetRegion, 'id', 'regionId');

      this.messageService.add({ severity: 'success', summary: 'Regionen wurden gespeichert', detail: '' });
    });
  }

  moveToSourceRegion(dataToMove) {
    this.masterDataService.deleteUserRegion(this.instanceId, dataToMove).subscribe((resRegionFunction: RegionObject[]) => {
      this.targetRegion = resRegionFunction;
      this.getRegionList();

      this.messageService.add({ severity: 'success', summary: 'Regionen wurden gespeichert', detail: '' });
    });
  }

  /**
 * MOVE FUNCTIONS FUNCTION
 */

  moveToTargetFunction(dataToMove) {
    this.masterDataService.addUserFunction(this.instanceId, dataToMove).subscribe((resFunctions: FunctionObject[]) => {
      this.targetFunction = resFunctions;
      this.sourceFunction = FormUtil.getSubsetOfArray(this.sourceFunction, this.targetFunction, 'functionId', 'functionId');

      this.messageService.add({ severity: 'success', summary: 'Funktionen wurden gespeichert', detail: '' });
    });
  }

  moveToSourceFunction(dataToMove) {
    this.masterDataService.deleteUserFunction(this.instanceId, dataToMove).subscribe((resFunctions: FunctionObject[]) => {
      this.targetFunction = resFunctions;
      this.getFunctionList();

      this.messageService.add({ severity: 'success', summary: 'Funktionen wurden gespeichert', detail: '' });
    });
  }

  close() {
    if (this.isModal) {
      this.modalAction.next('close');
    } else {
      this.router.navigate(['/stammdatenverwaltung/mitarbeiter']);
    }
  }

  ngOnDestroy() {
    if (this.param$) {
      this.param$.unsubscribe();
    }
    if (this.user$) {
      this.user$.unsubscribe();
    }
  }
}
