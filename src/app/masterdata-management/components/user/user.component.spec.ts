/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { ActivatedRoute, Routes, Router } from '@angular/router';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { UserComponent } from '@masterdata/components/user/user.component';
import { SharedModule } from '@shared/shared.module';
import { AlertComponent } from '@shared/components/alert/alert.component';
import { UserMockObjects } from '@shared/testing/user';
import { MessageService } from 'primeng/components/common/messageservice';

const userMockObjects = new UserMockObjects;

export class MasterDataServiceMock {
  saveUser() {
    return of(userMockObjects.USER_OBJECT);
  }
  getUser() {
    return of(userMockObjects.USER_OBJECT);
  }
  getRegionDataSelection() {
    return of(userMockObjects.REGION_SELECTION_ARRAY);
  }
  getFunctionDataSelection() {
    return of(userMockObjects.FUNCTION_SELECTION_ARRAY);
  }
  getOrganisation() {
    return of(userMockObjects.ORGANISATION_OBJECT);
  }
  getOrganisationDataSelection() {
    return of([userMockObjects.ORGANISATION_OBJECT]);
  }
  addUserRegion() {
    return of(userMockObjects.REGION_SELECTION_ARRAY);
  }
  deleteUserRegion() {
    return of(userMockObjects.REGION_SELECTION_ARRAY);
  }
  addUserFunction() {
    return of(userMockObjects.FUNCTION_SELECTION_ARRAY);
  }
  deleteUserFunction() {
    return of(userMockObjects.FUNCTION_SELECTION_ARRAY);
  }
}

describe('UserComponent', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;

  const router = {
    navigate: jasmine.createSpy('navigate')
  };
  beforeEach(async(() => {
    const routes: Routes = [
      {
        path: '**',
        component: AlertComponent
      }
    ];
    TestBed.configureTestingModule({
      declarations: [
        UserComponent
      ],
      imports: [
        SharedModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        {
          provide: MasterdataService,
          useClass: MasterDataServiceMock
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ id: 123 })
          }
        },
        { provide: Router, useValue: router },
        MessageService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
    component.targetColumnDefsRegion = [];
    component.targetColumnDefsFunction = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('validateAllFormFields', () => {
    it('should not save the form if there are validation errors', () => {
      expect(component.saveUser()).toBeFalsy();
    });

    it('should save the form if there are no validation errors', () => {
      component.createFormModal(); // doing this for not having to patch all the fields
      component.form.patchValue({
        firstname: 'test',
        lastname: 'test',
        date: {
          validFrom: { day: 1, month: 1, year: 2018 },
          validTo: { day: 1, month: 1, year: 2018 }
        }
      });
      expect(component.saveUser()).toBeTruthy();
    });

    it('should save the form if the component is opened in a modal and navigate away', () => {
      component.createFormModal(); // doing this for not having to patch all the fields
      component.form.patchValue({
        firstname: 'test',
        lastname: 'test',
        date: {
          validFrom: { day: 1, month: 1, year: 2018 },
          validTo: { day: 1, month: 1, year: 2018 }
        }
      });
      component.isModal = true;
      component.saveUser();
      expect(router.navigate).toHaveBeenCalledWith(['/stammdatenverwaltung/mitarbeiter', 1]);
    });
  });

  describe('close()', () => {
    it('should navigate away when the component is not a modal', () => {
      component.isModal = false;
      component.close();
      expect(router.navigate).toHaveBeenCalledWith(['/stammdatenverwaltung/mitarbeiter']);
    });

    it('should dispatch an event with content "close" if it is a modal and close() is called', () => {
      component.isModal = true;
      component.modalAction.subscribe((res) => {
        expect(res).toBe('close');
      });
      component.close();
    });
  });

  describe('setDefaultDate', () => {
    it('should set validFrom to current day', () => {
      const date = new Date();
      component.setDefaultDate('validFrom');
      expect(component.form.get('date').get('validFrom').value).toEqual({
        day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear()
      });
    });

    it('should set validTo to current day + 15 years', () => {
      const date = new Date();
      component.setDefaultDate('validTo');
      expect(component.form.get('date').get('validTo').value).toEqual({
        day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear() + 15
      });
    });
  });

  describe('move functions region', () => {
    it('should move regions from source to target', () => {
      component.moveToTargetRegion(userMockObjects.REGION_SELECTION_ARRAY);
      expect(component.targetRegion.length).toBeGreaterThan(0);
    });

    it('should move regions from target to source', () => {
      component.moveToSourceRegion(userMockObjects.REGION_SELECTION_ARRAY);
      expect(component.targetRegion.length).toBe(2);
    });
  });

  describe('move functions userfunction', () => {
    it('should move userfunction from source to target', () => {
      component.moveToTargetFunction(userMockObjects.FUNCTION_SELECTION_ARRAY);
      expect(component.targetFunction.length).toBeGreaterThan(0);
    });

    it('should move userfunction from target to source', () => {
      component.moveToSourceFunction(userMockObjects.FUNCTION_SELECTION_ARRAY);
      expect(component.targetFunction.length).toBe(2);
    });
  });

  describe('getOrganisationAndPatch()', () => {
    it('should get an organisation with id 1 and patch it´s values', () => {
      component.getOrganisationAndPatch(1);
      expect(component.form.getRawValue().organisation.address.community).toBe('Paderborn');
    });

    it('should reset all values in organisation form', () => {
      component.getOrganisationAndPatch(undefined);
      expect(component.form.getRawValue().organisation.address.community).toBe(null);
    });
  });
});
