/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { AGGRID_LOCALETEXT, ListUtil, DEFAULT_COL_DEF } from '@shared/utils/list.util';
import { DateObject } from '@shared/model/DateObject';
import { AuthenticationService } from '@core/services/authentication.service';

@Component({
  selector: 'ok-calendarlist',
  templateUrl: './calendarlist.component.html',
  styleUrls: ['./calendarlist.component.scss']
})
export class CalendarlistComponent implements OnInit, OnDestroy {
  defaultColDef = DEFAULT_COL_DEF;
  columnDefs = [
    { headerName: 'Name', field: 'name' },
    {
      headerName: 'Datum', field: 'dateIndex', filter: 'agDateColumnFilter', valueFormatter: ListUtil.formatDate,
      filterParams: {
        comparator: ListUtil.compareDates
      }
    }
  ];

  localeText = AGGRID_LOCALETEXT;

  rowData = [];
  userData$: Subscription;

  constructor(
    public authService: AuthenticationService,
    private masterDataService: MasterdataService,
    private router: Router
  ) { }

  ngOnInit() {

  }

  onGridReady(params) {
    this.userData$ = this.masterDataService.getCalendarData().subscribe((res: DateObject[]) => {
      this.rowData = res;
    });
    params.api.sizeColumnsToFit();
  }

  rowClicked(event) {
    this.router.navigate(['stammdatenverwaltung/kalender', event.data.id]);
  }

  ngOnDestroy() {
    if (this.userData$) {
      this.userData$.unsubscribe();
    }
  }

}
