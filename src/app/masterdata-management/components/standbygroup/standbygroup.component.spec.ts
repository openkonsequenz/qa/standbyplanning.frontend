/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { ActivatedRoute, Routes, Router } from '@angular/router';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { SharedModule } from '@shared/shared.module';


import { StandbygroupComponent } from '@masterdata/components/standbygroup/standbygroup.component';
import { AlertComponent } from '@shared/components/alert/alert.component';
import { MessageService } from 'primeng/components/common/messageservice';
import { UserObject } from '@shared/model/UserObject';
import { MasterdataManagementModule } from '@masterdata/masterdata-management.module';
import { StandbyScheduleModule } from '@schedule/standby-schedule.module';
import { StandbygroupMockObjects } from '@shared/testing/standbygroup';

const standbygroupMockObjects = new StandbygroupMockObjects;

export class MasterDataServiceMock {
  saveStandbygroup() {
    return of(standbygroupMockObjects.STANDBYGROUP_OBJECT);
  } // or whatever dummy state value you want to use
  getStandbygroup() {
    return of(standbygroupMockObjects.STANDBYGROUP_OBJECT);
  }
  getFunctionDataSelection() {
    return of(standbygroupMockObjects.FUNCTION_SELECTION_ARRAY);
  }

  getRegionData() {
    return of(standbygroupMockObjects.REGION_SELECTION_ARRAY);
  }
  getUserDataSelection() {
    return of(standbygroupMockObjects.USER_SELECTION_ARRAY);
  }

  getBranchDataSelection() {
    return of(standbygroupMockObjects.BRANCH_ARRAY);
  }

  filterUsersByStandbygroup() {
    return of(standbygroupMockObjects.USER_SELECTION_ARRAY);
  }

  addStandbyGroupRegion() {
    return of(standbygroupMockObjects.REGION_SELECTION_ARRAY);
  }

  deleteStandbyGroupRegion() {
    return of(standbygroupMockObjects.REGION_SELECTION_ARRAY);
  }

  addStandbyGroupUser() {
    return of(standbygroupMockObjects.USERDATA_WITH_PROTOCOL_OBJECT);
  }

  deleteStandbyGroupUser() {
    return of(standbygroupMockObjects.USERDATA_WITH_PROTOCOL_OBJECT);
  }

  addStandbyGroupBranch() {
    return of(standbygroupMockObjects.BRANCH_ARRAY);
  }

  deleteStandbyGroupBranch() {
    return of(standbygroupMockObjects.BRANCH_ARRAY);
  }

  addStandbygroupFunction() {
    return of(standbygroupMockObjects.FUNCTION_SELECTION_ARRAY_SINGLE);
  }

  deleteStandbygroupFunction() {
    return of(standbygroupMockObjects.FUNCTION_SELECTION_ARRAY_SINGLE);
  }

  deleteStandbygroupDuration() {
    return of([standbygroupMockObjects.DURATION_OBJECT]);
  }
  saveStandbygroupDuration() {
    return of(standbygroupMockObjects.PROTOCOL_OBJECT);
  }

  getCalendarData() {
    return of(standbygroupMockObjects.CALENDAR_ARRAY);
  }

  deleteStandbygroupCalendar() {
    return of(standbygroupMockObjects.CALENDAR_ARRAY);
  }
  addStandbygroupCalendar() {
    return of(standbygroupMockObjects.CALENDAR_ARRAY);
  }
  copyStandbygroup() {
    return of(standbygroupMockObjects.STANDBYGROUP_OBJECT);
  }
}

describe('StandbygroupComponent', () => {
  let component: StandbygroupComponent;
  let fixture: ComponentFixture<StandbygroupComponent>;
  const router = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    const routes: Routes = [
      {
        path: '**',
        component: AlertComponent
      }
    ];
    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        SharedModule,
        MasterdataManagementModule,
        StandbyScheduleModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        {
          provide: MasterdataService,
          useClass: MasterDataServiceMock
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ id: 123 })
          }
        },
        { provide: Router, useValue: router },
        MessageService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandbygroupComponent);
    component = fixture.componentInstance;
    component.targetColumnDefsRegion = [];
    component.targetColumnDefsUser = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('getWeekDay()', () => {
    it('should return Montag to number 1', () => {
      expect(component.getWeekDay(1)).toBe('Montag');
    });

    it('should return Samstag to number 6', () => {
      expect(component.getWeekDay(6)).toBe('Samstag');
    });

    it('should return Sonntag to number 0', () => {
      expect(component.getWeekDay(7)).toBe('Sonntag');
    });

    it('should return undefined to a non existing weekday number', () => {
      expect(component.getWeekDay(0)).toBe(undefined);
    });
  });

  describe('close()', () => {
    it('should navigate away when the component is not a modal', () => {
      component.isModal = false;
      component.close();
      expect(router.navigate).toHaveBeenCalledWith(['/stammdatenverwaltung/bereitschaftsgruppe']);
    });

    it('should dispatch an event with content "close" if it is a modal and close() is called', () => {
      component.isModal = true;
      component.modalAction.subscribe((res) => {
        expect(res).toBe('close');
      });
      component.close();
    });
  });

  describe('saveStandbyGroup()', () => {
    it('should not save the form if there are validation errors', () => {
      expect(component.saveStandbygroup()).toBeFalsy();
    });

    it('should save the form if there are no validation errors', () => {
      component.form.patchValue({ title: 'test' });
      expect(component.saveStandbygroup()).toBeTruthy();
    });

    it('should save the form if the component is opened in a modal and navigate away', () => {
      component.form.patchValue({ title: 'test' });
      component.isModal = true;
      component.saveStandbygroup();
      expect(router.navigate).toHaveBeenCalledWith(['/stammdatenverwaltung/bereitschaftsgruppe', 1]);
    });
  });

  describe('saveStandbyDuration()', () => {
    it('should not save the form if there are validation errors', () => {
      component.standbyDurationsForm = component.createStandbyDuration();
      expect(component.saveStandbyDuration()).toBeFalsy();
    });

    it('should save the form if there are no validation errors', () => {
      component.addDuration();
      component.standbyDurationsForm.patchValue({
        validDayFrom: 1,
        validDayTo: 3,
        validFrom: {
          hour: 11,
          minute: 11,
          second: 11
        }, validTo: {
          hour: 22,
          minute: 22,
          second: 2
        }
      });
      component.form.patchValue({ standbyGroupId: 1 });
      expect(component.saveStandbyDuration()).toBeTruthy();
    });
  });

  describe('handle Durations', () => {
    it('should add a duration', () => {
      component.addDuration();
      expect(component.standbyDurationsForm).toBeTruthy();
    });

    it('should remove a duration', () => {
      component.removeDuration(1);
      expect(component.standbyDurations.length).toBeTruthy();
    });
  });

  describe('move functions region', () => {
    it('should move regions from source to target', () => {
      component.moveToTargetRegion(standbygroupMockObjects.REGION_SELECTION_ARRAY);
      expect(component.targetRegion.length).toBeGreaterThan(0);
    });

    it('should move regions from target to source', () => {
      component.moveToSourceRegion(standbygroupMockObjects.REGION_SELECTION_ARRAY);
      expect(component.sourceRegion.length).toBe(0);
    });
  });

  describe('move functions user', () => {
    it('should move user from source to target', () => {
      component.moveToTargetUser(standbygroupMockObjects.USER_SELECTION_ARRAY, 'Verschieben');
      component.userModalRef.componentInstance.decision.next({ id: 1, validFrom: '2018-12-12', validTo: '2018-12-14' });
      expect(component.sourceUser.length).toBeGreaterThan(0);
    });

    it('shouldn´t move user from source to target if no data is entered in modal', () => {
      component.moveToTargetUser(standbygroupMockObjects.USER_SELECTION_ARRAY, 'Verschieben');
      component.userModalRef.componentInstance.decision.next(undefined);
      expect(component.targetUser.length).toBe(0);
    });

    it('should edit the user in targetList if saveButtonLabel is not "Verschieben"', () => {
      component.moveToTargetUser(standbygroupMockObjects.USER_SELECTION_ARRAY, 'Daten ändern');
      component.userModalRef.componentInstance.decision.next({ id: 1, userId: 1, validFrom: '2018-12-12', validTo: '2018-12-14' });
      expect(component.targetUser.length).toBe(2);
    });

    it('should move user and update in helper function', () => {
      const userObj = new UserObject();
      component.moveUserAndUpdate([userObj]);
      expect(component.targetUser.length).toBe(1);
    });

    it('should move users from target to source', () => {
      component.moveToSourceUser(standbygroupMockObjects.USER_SELECTION_ARRAY);
      expect(component.sourceUser.length).toBe(2);
    });

    describe('copyUsersToTarget', () => {
      it('should move users from target to source', () => {
        component.copyUserToTarget(standbygroupMockObjects.USER_SELECTION_ARRAY);
        expect(component.targetUser.length).toBe(2);
      });
    });
  });

  describe('move functions branch', () => {
    it('should move branch from source to target', () => {
      component.moveToTargetBranch(standbygroupMockObjects.BRANCH_ARRAY);
      expect(component.targetBranch.length).toBeGreaterThan(0);
    });

    it('should move branch from target to source', () => {
      component.moveToSourceBranch(standbygroupMockObjects.BRANCH_ARRAY);
      expect(component.targetBranch.length).toBe(3);
    });
  });

  describe('move functions userfunction', () => {
    it('should move ONE userfunction from source to target', () => {
      component.targetFunction = [];
      component.moveToTargetFunction(standbygroupMockObjects.FUNCTION_SELECTION_ARRAY_SINGLE);
      expect(component.targetFunction.length).toBe(1);
    });

    it('shouldn´t move more than one userfunction from source to target', () => {
      component.targetFunction = standbygroupMockObjects.FUNCTION_SELECTION_ARRAY_SINGLE;
      component.moveToTargetFunction(standbygroupMockObjects.FUNCTION_SELECTION_ARRAY_SINGLE);
      expect(component.targetFunction.length).toBe(1);
    });

    it('should move userfunction from target to source', () => {
      component.targetFunction = [];
      component.moveToSourceFunction(standbygroupMockObjects.FUNCTION_SELECTION_ARRAY);
      expect(component.targetFunction.length).toBe(1);
    });
  });

  describe('move functions calendar', () => {
    it('should move calender objects from source to target', () => {
      component.moveToTargetCalendar(standbygroupMockObjects.CALENDAR_ARRAY);
      expect(component.targetCalendar.length).toBeGreaterThan(0);
    });

    it('should move calender objects from target to source', () => {
      component.moveToSourceCalendar(standbygroupMockObjects.CALENDAR_ARRAY);
      expect(component.sourceCalendar.length).toBe(0);
    });
  });
});
