/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy, ViewChild, ElementRef, Injector } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subject, Subscription } from 'rxjs';
import { Params } from '@angular/router';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { FormUtil } from '@shared/utils/form.util';
import { StandbygroupObject } from '@shared/model/StandbygroupObject';
import { ListUtil } from '@shared/utils/list.util';
import { RegionObject } from '@shared/model/RegionObject';
import { UserObject } from '@shared/model/UserObject';
import { ValidityDialogComponent } from '@masterdata/selection-dialogs/validity-dialog/validity-dialog.component';
import { StandbyDurationObject } from '@shared/model/StandbyDurationObject';
import { BranchObject } from '@shared/model/BranchObject';
import { AbstractFormComponent } from '@shared/abstract/abstract-form/abstract-form.component';
import { FunctionObject } from '@shared/model/FunctionObject';
import { InformationComponent } from '@shared/components/information/information.component';
import { DateObject } from '@shared/model/DateObject';
import { LoosingDataDialogComponent } from '@masterdata/selection-dialogs/loosing-data-dialog/loosing-data-dialog.component';
import { ProtocolObject } from '@shared/model/ProtocolObject';
import { ProtocolDialogComponent } from '@schedule/dialogs/protocol-dialog/protocol-dialog.component';
import { CopyDialogComponent } from '@masterdata/selection-dialogs/copy-dialog/copy-dialog.component';
import { AuthenticationService } from '@core/services/authentication.service';
import { ProtocolMessageObject } from '@shared/model/ProtocolMessageObject';


@Component({
  selector: 'ok-standbygroup',
  templateUrl: './standbygroup.component.html',
  styleUrls: ['./standbygroup.component.scss']
})
export class StandbygroupComponent extends AbstractFormComponent implements OnInit, OnDestroy {
  decisionModalRef: NgbModalRef;
  regionModalRef: NgbModalRef;
  copyModalRef: NgbModalRef;
  decision = new Subject<boolean>();
  modalAction = new Subject<string>();
  isModal = false;
  instanceId: number;
  addZero = FormUtil.addZero;

  @ViewChild('standbyDurationsModal')
  standbyDurationsModal: ElementRef;
  standbyDurationsModalRef: NgbModalRef;
  standbyDurationsForm: FormGroup;

  /**
   * Regions
   */
  sourceRegion: Array<any> = [];
  targetRegion: Array<any> = [];

  sourceColumnDefsRegion = [
    { headerName: 'Regionsname', field: 'regionName' }
  ];
  targetColumnDefsRegion = [
    { headerName: 'Regionsname', field: 'regionName' }
  ];

  /**
  * Users
  */
  sourceUser: Array<any> = [];
  targetUser: Array<any> = [];

  userModalRef: NgbModalRef;
  userModalWarningRef: NgbModalRef;

  sourceColumnDefsUser = [
    { headerName: 'Vorname', field: 'firstname' },
    { headerName: 'Nachname', field: 'lastname' },
    { headerName: 'OrganisationsName', field: 'organisationName' },
    { headerName: 'Funktionen', field: 'userHasUserFunctionStr' },
    { headerName: 'Regionen', field: 'userRegionStr' }
  ];
  targetColumnDefsUser = [
    { headerName: 'Position', field: 'position' },
    { headerName: 'Vorname', field: 'firstname' },
    { headerName: 'Nachname', field: 'lastname' },
    { headerName: 'OrganisationsName', field: 'organisationName' },
    { headerName: 'Funktionen', field: 'userHasUserFunctionStr' },
    { headerName: 'Regionen', field: 'userRegionStr' },
    {
      headerName: 'Gültig von', field: 'validFrom',
      filter: 'agDateColumnFilter',
      valueFormatter: ListUtil.formatDate,
      filterParams: {
        comparator: ListUtil.compareDates
      }
    },
    {
      headerName: 'Gültig bis',
      field: 'validTo',
      valueFormatter: ListUtil.formatDate,
      filterParams: {
        comparator: ListUtil.compareDates
      }
    }
  ];

  /**
  * Branches
  */
  sourceBranch: Array<BranchObject> = [];
  targetBranch: Array<BranchObject> = [];

  sourceColumnDefsBranch = [
    { headerName: 'Name', field: 'title' }
  ];
  targetColumnDefsBranch = [
    { headerName: 'Name', field: 'title' }
  ];

  /**
  * StandbyDurations
  */
  standbyDurations: Array<StandbyDurationObject>;
  modalProtocolRef: NgbModalRef;

  /**
   * Functions
   */
  sourceFunction: Array<FunctionObject> = [];
  targetFunction: Array<any> = [];
  function$: Subscription;
  functionModalRef: NgbModalRef;

  sourceColumnDefsFunction = [
    { headerName: 'Funktionsname', field: 'functionName' }
  ];
  targetColumnDefsFunction = [
    { headerName: 'Funktionsname', field: 'functionName' }
  ];

  /**
   * Calendar
   */
  sourceCalendar: Array<any> = [];
  targetCalendar: Array<any> = [];
  calender$: Subscription;

  sourceColumnDefsCalendar = [
    { headerName: 'Name', field: 'name' },
    {
      headerName: 'Datum', field: 'dateIndex',
      filter: 'agDateColumnFilter',
      valueFormatter: ListUtil.formatDate,
      filterParams: {
        comparator: ListUtil.compareDates
      }
    }
  ];
  targetColumnDefsCalendar = [
    { headerName: 'Name', field: 'name' },
    {
      headerName: 'Datum', field: 'dateIndex',
      filter: 'agDateColumnFilter',
      valueFormatter: ListUtil.formatDate,
      filterParams: {
        comparator: ListUtil.compareDates
      }
    }
  ];

  informationModalRef: NgbModalRef;
  param$: Subscription;
  standbygroup$: Subscription;
  region$: Subscription;
  user$: Subscription;
  branch$: Subscription;
  filterUser$: Subscription;
  constructor(
    public authService: AuthenticationService,
    private masterDataService: MasterdataService,
    private injector: Injector
  ) {
    super(injector);
  }

  ngOnInit() {
    this.createForm();
    this.param$ = this.route.params.subscribe((params: Params) => {
      this.instanceId = params['id'];
      if (this.instanceId) {
        this.standbygroup$ = this.masterDataService.getStandbygroup(this.instanceId).subscribe(
          standbygroupRes => {
            this.form.patchValue(standbygroupRes);
            this.targetRegion = standbygroupRes.lsRegions;
            this.targetUser = standbygroupRes.lsUserInStandbyGroups;
            this.targetBranch = standbygroupRes.lsBranches;
            this.targetFunction = standbygroupRes.lsUserFunction;
            this.standbyDurations = standbygroupRes.lsStandbyDurations;
            this.targetCalendar = standbygroupRes.lsIgnoredCalendarDays;
            this.getRegionList();
            this.getBranchList();
            this.getFunctionList();
            this.getCalendarList();
            this.filterUsersByStandbygroup();
          }
        );
      }
    });
  }

  createForm() {
    this.form = this.fb.group({
      id: '',
      title: ['', Validators.required],
      modificationDate: '',
      note: '',
      nextUserInNextCycle: false,
      extendStandbyTime: false,
      userFunctionId: ''
    });
  }

  getRegionList(): void {
    this.region$ = this.masterDataService.getRegionData().subscribe((regionRes: RegionObject[]) => {
      this.sourceRegion = FormUtil.getSubsetOfArray(regionRes, this.targetRegion, 'id', 'id');
    });
  }

  filterUsersByStandbygroup() {
    this.filterUser$ = this.masterDataService.filterUsersByStandbygroup(this.instanceId).subscribe((userRes: UserObject[]) => {
      this.sourceUser = userRes;
    });
  }

  getBranchList(): void {
    this.branch$ = this.masterDataService.getBranchDataSelection().subscribe((branchRes: BranchObject[]) => {
      this.sourceBranch = FormUtil.getSubsetOfArray(branchRes, this.targetBranch, 'id', 'id');
    });
  }

  getFunctionList(): void {
    this.function$ = this.masterDataService.getFunctionDataSelection().subscribe((functionRes: FunctionObject[]) => {
      this.sourceFunction = FormUtil.getSubsetOfArray(functionRes, this.targetFunction, 'functionId', 'functionId');
    });
  }

  getCalendarList(): void {
    this.calender$ = this.masterDataService.getCalendarData().subscribe((calenderRes: DateObject[]) => {
      this.sourceCalendar = FormUtil.getSubsetOfArray(calenderRes, this.targetCalendar, 'id', 'id');
    });
  }

  /**
  * Saves the current Form
  */
  saveStandbygroup(): boolean {
    if (FormUtil.validate(this.form)) {
      const standbygroupToSave = this.form.getRawValue();
      this.masterDataService.saveStandbygroup(standbygroupToSave).subscribe((res: StandbygroupObject) => {
        // neccessary due to canDeactivate guard
        FormUtil.markAsPristineAndUntouched(this.form);
        if (this.isModal) {
          this.router.navigate(['/stammdatenverwaltung/bereitschaftsgruppe', res.id]);
          this.modalAction.next('close');
        } else {
          this.router.navigate(['/stammdatenverwaltung/bereitschaftsgruppe']);
        }
      });
      return true;
    }
    return false;
  }

  /**
   * STANDBY DURATIONS
   */
  createStandbyDuration(): FormGroup {
    return this.fb.group({
      validDayFrom: ['', Validators.required],
      validDayTo: ['', Validators.required],
      validFrom: ['', Validators.required],
      validTo: ['', Validators.required],
      nextUserInNextDuration: false
    });
  }

  addDuration() {
    this.standbyDurationsForm = this.createStandbyDuration();
    this.standbyDurationsModalRef = this.modalService.open(this.standbyDurationsModal, { size: 'lg' });
  }

  removeDuration(index: number) {
    this.masterDataService.deleteStandbygroupDuration(this.instanceId, [this.standbyDurations[index]]).subscribe(
      (standbyDurationsRes: StandbyDurationObject[]) => {
        this.standbyDurations = standbyDurationsRes;
      }
    );
  }

  saveStandbyDuration() {
    if (FormUtil.validate(this.standbyDurationsForm)) {
      const standbygroupDurationToSave = this.standbyDurationsForm.getRawValue();
      standbygroupDurationToSave.standbyGroupId = this.instanceId;
      this.masterDataService.saveStandbygroupDuration(this.instanceId, [standbygroupDurationToSave]).subscribe(
        (standbydurationRes: ProtocolObject) => {
          // neccessary due to canDeactivate guard
          FormUtil.markAsPristineAndUntouched(this.standbyDurationsForm);
          this.standbyDurations = standbydurationRes.data;
          this.standbyDurationsModalRef.close();
        }
      );
      return true;
    }
    return false;
  }

  /**
   * MOVE FUNCTIONS REGION
   */

  moveToTargetRegion(dataToMove) {
    this.masterDataService.addStandbyGroupRegion(this.instanceId, dataToMove).subscribe((resRegionFunction: RegionObject[]) => {
      this.targetRegion = resRegionFunction;
      this.sourceRegion = FormUtil.getSubsetOfArray(this.sourceRegion, this.targetRegion, 'id', 'id');
      this.filterUsersByStandbygroup();

      this.messageService.add({ severity: 'success', summary: 'Regionen wurden gespeichert', detail: '' });
    });
  }

  moveToSourceRegion(dataToMove) {
    this.masterDataService.deleteStandbyGroupRegion(this.instanceId, dataToMove).subscribe((resRegionFunction: RegionObject[]) => {
      this.targetRegion = resRegionFunction;
      this.getRegionList();
      this.filterUsersByStandbygroup();

      this.messageService.add({ severity: 'success', summary: 'Regionen wurden gespeichert', detail: '' });
    });
  }

  /**
   * MOVE FUNCTIONS USER
   */

  moveToTargetUser(dataToMove, saveButtonLabel: string) {
    this.userModalRef = this.modalService.open(ValidityDialogComponent, { size: 'lg' });
    if (dataToMove.length === 1) {
      this.userModalRef.componentInstance.form.patchValue(
        {
          date: {
            validFrom: this.ngbDateParserFormatter.parse(dataToMove[0].validFrom),
            validTo: this.ngbDateParserFormatter.parse(dataToMove[0].validTo),
          },
          position: dataToMove[0].position
        }
      );
    }
    if (saveButtonLabel === 'Verschieben' || dataToMove.length > 1) {
      this.userModalRef.componentInstance.form.controls.position.disable();
    }
    this.userModalRef.componentInstance.saveButtonLabel = saveButtonLabel;
    this.userModalRef.componentInstance.decision.subscribe(resUserModal => {
      if (resUserModal) {
        for (let i = 0; i < dataToMove.length; i++) {
          dataToMove[i].validFrom = resUserModal.validFrom;
          dataToMove[i].validTo = resUserModal.validTo;
          if (saveButtonLabel === 'Verschieben') {
            dataToMove[i].userId = dataToMove[i].id;
            delete dataToMove[i].id;
          } else if (saveButtonLabel === 'Daten ändern' && dataToMove.length === 1) {
            dataToMove[i].position = resUserModal.position;
          }
          delete dataToMove[i].isCompany;
          delete dataToMove[i].notes;
          delete dataToMove[i].modificationDate;
        }
        this.masterDataService.addStandbyGroupUser(this.instanceId, dataToMove).subscribe((resUser: ProtocolObject) => {
          this.moveUserAndUpdate(resUser.data);
          if (saveButtonLabel === 'Daten ändern' && dataToMove.length === 1) {
            this.openProtocol(resUser);
          }
        });
      }
      this.userModalRef.close();
    });
  }

  copyUserToTarget(dataToMove) {
    for (let i = 0; i < dataToMove.length; i++) {
      const dateFrom = new Date(dataToMove[i].validFrom);
      const dateTo = new Date(dataToMove[i].validTo);
      dateFrom.setFullYear(dateFrom.getFullYear() + 1);
      dateTo.setFullYear(dateTo.getFullYear() + 1);
      dataToMove[i].validFrom = FormUtil.dateToISOString(dateFrom);
      dataToMove[i].validTo = FormUtil.dateToISOString(dateTo);
      delete dataToMove[i].position;
      delete dataToMove[i].id;
    }
    this.masterDataService.addStandbyGroupUser(this.instanceId, dataToMove).subscribe((resUser: ProtocolObject) => {
      this.moveUserAndUpdate(resUser.data);
    });
  }

  moveToSourceUser(dataToMove) {
    this.userModalWarningRef = this.modalService.open(LoosingDataDialogComponent);
    this.userModalWarningRef.componentInstance.decision.subscribe(resUserModal => {
      if (resUserModal) {
        this.masterDataService.deleteStandbyGroupUser(this.instanceId, dataToMove).subscribe((resUser: ProtocolObject) => {
          this.moveUserAndUpdate(resUser.data);
          this.openProtocol(resUser);
        });
      }
      this.userModalWarningRef.close();
    });
  }

  moveUserAndUpdate(resUser: UserObject[]) {
    this.targetUser = resUser;
    this.filterUsersByStandbygroup();

    this.messageService.add({ severity: 'success', summary: 'Bereitschaften wurden gespeichert', detail: '' });
  }

  /**
   * MOVE FUNCTIONS FUNCTION
   */

  moveToTargetFunction(dataToMove) {
    if (this.targetFunction.length > 0) {
      this.informationModalRef = this.modalService.open(InformationComponent);
    } else {
      this.masterDataService.addStandbygroupFunction(this.instanceId, dataToMove).subscribe((resFunctions: FunctionObject[]) => {
        this.targetFunction = resFunctions;
        this.sourceFunction = FormUtil.getSubsetOfArray(this.sourceFunction, this.targetFunction, 'functionId', 'functionId');
        this.filterUsersByStandbygroup();

        this.messageService.add({ severity: 'success', summary: 'Funktionen wurden gespeichert', detail: '' });
      });
    }
  }

  moveToSourceFunction(dataToMove) {
    this.masterDataService.deleteStandbygroupFunction(this.instanceId, dataToMove).subscribe((resFunctions: FunctionObject[]) => {
      this.targetFunction = resFunctions;
      this.getFunctionList();
      this.filterUsersByStandbygroup();

      this.messageService.add({ severity: 'success', summary: 'Funktionen wurden gespeichert', detail: '' });
    });
  }

  /**
  * MOVE FUNCTIONS BRANCHES
  */

  moveToTargetBranch(dataToMove) {
    this.masterDataService.addStandbyGroupBranch(this.instanceId, dataToMove).subscribe((resBranch: BranchObject[]) => {
      this.targetBranch = resBranch;
      this.sourceBranch = FormUtil.getSubsetOfArray(this.sourceBranch, this.targetBranch, 'id', 'id');

      this.messageService.add({ severity: 'success', summary: 'Sparten wurden gespeichert', detail: '' });
    });
  }

  moveToSourceBranch(dataToMove) {
    this.masterDataService.deleteStandbyGroupBranch(this.instanceId, dataToMove).subscribe((resBranch: BranchObject[]) => {
      this.targetBranch = resBranch;
      this.getBranchList();

      this.messageService.add({ severity: 'success', summary: 'Funktionen wurden gespeichert', detail: '' });
    });
  }

  /**
   * MOVE FUNCTIONS CALENDAR
   */

  moveToTargetCalendar(dataToMove) {
    this.masterDataService.addStandbygroupCalendar(this.instanceId, dataToMove).subscribe((resCalendar: DateObject[]) => {
      this.targetCalendar = resCalendar;
      this.sourceCalendar = FormUtil.getSubsetOfArray(this.sourceCalendar, this.targetCalendar, 'id', 'id');

      this.messageService.add({ severity: 'success', summary: 'Daten wurden gespeichert', detail: '' });
    });
  }

  moveToSourceCalendar(dataToMove) {
    this.masterDataService.deleteStandbygroupCalendar(this.instanceId, dataToMove).subscribe((resCalendar: DateObject[]) => {
      this.targetCalendar = resCalendar;
      this.getCalendarList();

      this.messageService.add({ severity: 'success', summary: 'Daten wurden gespeichert', detail: '' });
    });
  }

  /**
   * COPY STANDBYGROUP
   */
  openCopyDialog() {
    this.copyModalRef = this.modalService.open(CopyDialogComponent, { size: 'lg' });
    this.copyModalRef.componentInstance.decision.subscribe((dataToCopy) => {
      if (dataToCopy) {
        this.masterDataService.copyStandbygroup(this.instanceId, dataToCopy).subscribe((standbyGroup) => {
          this.router.navigate(['stammdatenverwaltung/bereitschaftsgruppe', standbyGroup.id]);
        });
      }
      this.copyModalRef.close();
    });
  }

  /**
   * UTIL METHODS
   */

  openProtocol(protocolObject: ProtocolObject) {
    if (protocolObject.lsMsg.length > 0) {
      this.modalProtocolRef = this.modalService.open(ProtocolDialogComponent, { windowClass: 'modal-xl', backdrop: 'static' });
      this.modalProtocolRef.componentInstance.protocol = protocolObject;
    }
  }

  close() {
    if (this.isModal) {
      this.modalAction.next('close');
    } else {
      this.router.navigate(['/stammdatenverwaltung/bereitschaftsgruppe']);
    }
  }

  public getWeekDay(weekDay: number) {
    const weekDays = {
      1: 'Montag',
      2: 'Dienstag',
      3: 'Mittwoch',
      4: 'Donnerstag',
      5: 'Freitag',
      6: 'Samstag',
      7: 'Sonntag'
    };

    return weekDays[weekDay];
  }

  ngOnDestroy() {
    if (this.param$) {
      this.param$.unsubscribe();
    }
    if (this.standbygroup$) {
      this.standbygroup$.unsubscribe();
    }
    if (this.region$) {
      this.region$.unsubscribe();
    }
    if (this.branch$) {
      this.branch$.unsubscribe();
    }
  }
}
