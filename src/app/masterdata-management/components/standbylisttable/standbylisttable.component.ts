/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { ListUtil, AGGRID_LOCALETEXT, DEFAULT_COL_DEF } from '@shared/utils/list.util';
import { StandbylistObject } from '@shared/model/StandbylistObject';
import { StandbylistComponent } from '@masterdata/components/standbylist/standbylist.component';
import { AuthenticationService } from '@core/services/authentication.service';

@Component({
  selector: 'ok-standbylisttable',
  templateUrl: './standbylisttable.component.html',
  styleUrls: ['./standbylisttable.component.scss']
})
export class StandbylisttableComponent implements OnInit, OnDestroy {
  defaultColDef = DEFAULT_COL_DEF;
  columnDefs = [
    { headerName: 'Titel', field: 'title' },
    {
      headerName: 'Änderungsdatum', field: 'modificationDate', valueFormatter: ListUtil.formatDate,
      filterParams: {
        comparator: ListUtil.compareDates
      }
    },
    { headerName: 'Bereitschaftsgruppen', field: 'lsStandbyGroupsStr', autoHeight: true }
  ];

  localeText = AGGRID_LOCALETEXT;

  rowData: StandbylistObject[] = [];
  standbylist$: Subscription;
  modalRef: NgbModalRef;

  constructor(
    public authService: AuthenticationService,
    private masterDataService: MasterdataService,
    private router: Router,
    private modalService: NgbModal
  ) { }

  ngOnInit() {

  }

  onGridReady(params) {
    this.standbylist$ = this.masterDataService.getStandbyListSelection().subscribe((res: StandbylistObject[]) => {
      this.rowData = res;
    });
    params.api.sizeColumnsToFit();
  }

  rowClicked(event) {
    this.router.navigate(['stammdatenverwaltung/bereitschaftsliste', event.data.id]);
  }

  ngOnDestroy() {
    if (this.standbylist$) {
      this.standbylist$.unsubscribe();
    }
  }

  openModal() {
    this.modalRef = this.modalService.open(StandbylistComponent, { size: 'lg', backdrop: 'static' });
    this.modalRef.componentInstance.isModal = true;
    const modalAction: Subscription = this.modalRef.componentInstance.modalAction.subscribe((res) => {
      if (res === 'close') {
        this.modalRef.close();
        modalAction.unsubscribe();
      }
    });
  }

}
