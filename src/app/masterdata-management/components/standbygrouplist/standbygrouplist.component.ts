/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { StandbygroupObject } from '@shared/model/StandbygroupObject';
import { StandbygroupComponent } from '@masterdata/components/standbygroup/standbygroup.component';
import { ListUtil, AGGRID_LOCALETEXT, DEFAULT_COL_DEF } from '@shared/utils/list.util';
import { AuthenticationService } from '@core/services/authentication.service';

@Component({
  selector: 'ok-standbygrouplist',
  templateUrl: './standbygrouplist.component.html',
  styleUrls: ['./standbygrouplist.component.scss']
})
export class StandbygrouplistComponent implements OnInit, OnDestroy {
  defaultColDef = DEFAULT_COL_DEF;
  columnDefs = [
    { headerName: 'Titel', field: 'title' },
    {
      headerName: 'Änderungsdatum', field: 'modificationDate', filter: 'agDateColumnFilter', valueFormatter: ListUtil.formatDate,
      filterParams: {
        comparator: ListUtil.compareDates
      }
    }
  ];
  localeText = AGGRID_LOCALETEXT;

  rowData = [];
  standbyGroupData$: Subscription;
  modalRef: NgbModalRef;

  constructor(
    public authService: AuthenticationService,
    private masterDataService: MasterdataService,
    private router: Router,
    private modalService: NgbModal
  ) { }

  ngOnInit() {

  }

  onGridReady(params) {
    this.standbyGroupData$ = this.masterDataService.getStandbygroupData().subscribe((res: StandbygroupObject[]) => {
      this.rowData = res;
    });
    params.api.sizeColumnsToFit();
  }

  rowClicked(event) {
    this.router.navigate(['stammdatenverwaltung/bereitschaftsgruppe', event.data.id]);
  }

  ngOnDestroy() {
    if (this.standbyGroupData$) {
      this.standbyGroupData$.unsubscribe();
    }
  }

  openModal() {
    this.modalRef = this.modalService.open(StandbygroupComponent, { size: 'lg', backdrop: 'static' });
    this.modalRef.componentInstance.isModal = true;
    const modalAction: Subscription = this.modalRef.componentInstance.modalAction.subscribe((res) => {
      if (res === 'close') {
        this.modalRef.close();
        modalAction.unsubscribe();
      }
    });
  }

}
