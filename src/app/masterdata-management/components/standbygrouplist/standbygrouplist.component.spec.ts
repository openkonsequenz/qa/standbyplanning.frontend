/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { SharedModule } from '@shared/shared.module';
import { StandbygrouplistComponent } from '@masterdata/components/standbygrouplist/standbygrouplist.component';
import { Routes } from '@angular/router';
import { AlertComponent } from '@shared/components/alert/alert.component';
import { StandbygroupMockObjects } from '@shared/testing/standbygroup';
import { MasterdataManagementModule } from '@masterdata/masterdata-management.module';
import { MessageService } from 'primeng/components/common/messageservice';

const standbygroupMockObjects = new StandbygroupMockObjects;

export class MasterDataServiceMock {
  getStandbygroupData() {
    return of(standbygroupMockObjects.STANDBYGROUP_ARRAY);
  }
}

describe('UserlistComponent', () => {
  let component: StandbygrouplistComponent;
  let fixture: ComponentFixture<StandbygrouplistComponent>;

  const routes: Routes = [
    {
      path: '**',
      component: AlertComponent
    }
  ];
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        SharedModule,
        RouterTestingModule.withRoutes(routes),
        MasterdataManagementModule // needed in order to have the LocationComponent for the openModal method
      ],
      providers: [
        MasterdataService,
        MessageService,
        {
          provide: MasterdataService,
          useClass: MasterDataServiceMock
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandbygrouplistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call onGridReady without error', () => {
    const params = {
      api: {
        sizeColumnsToFit: () => { }
      }
    };
    try {
      component.onGridReady(params);
    } catch (e) {
      expect(e).toBeFalsy();
    }
  });

  it('should navigate to a details view on rowClicked()', () => {
    const event = { data: { id: 1 } };
    component.rowClicked(event);
  });

  it('should open a modal and close it on any action', () => {
    component.openModal();
    component.modalRef.componentInstance.modalAction.next('close');
  });

  it('should open a modal and shouldn´t close it is the result is not "close"', () => {
    component.openModal();
    component.modalRef.componentInstance.modalAction.next('stay');
  });
});
