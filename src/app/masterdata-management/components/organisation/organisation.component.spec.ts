/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { ActivatedRoute, Routes } from '@angular/router';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { OrganisationComponent } from '@masterdata/components/organisation/organisation.component';
import { SharedModule } from '@shared/shared.module';
import { OrganisationMockObjects } from '@shared/testing/organisation';
import { AlertComponent } from '@shared/components/alert/alert.component';
import { MessageService } from 'primeng/components/common/messageservice';

const organisationMockObjects = new OrganisationMockObjects;

export class MasterDataServiceMock {
  saveOrganisation() {
    return of(organisationMockObjects.ORGANISATION_OBJECT);
  } // or whatever dummy state value you want to use
  getOrganisation() {
    return of(organisationMockObjects.ORGANISATION_OBJECT);
  }
}

describe('OrganisationComponent', () => {
  let component: OrganisationComponent;
  let fixture: ComponentFixture<OrganisationComponent>;


  beforeEach(async(() => {
    const routes: Routes = [
      {
        path: '**',
        component: AlertComponent
      }
    ];
    TestBed.configureTestingModule({
      declarations: [
        OrganisationComponent
      ],
      imports: [
        SharedModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        {
          provide: MasterdataService,
          useClass: MasterDataServiceMock
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ id: 123 })
          }
        },
        MessageService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('validateAllFormFields', () => {
    it('should not save the form if there are validation errors', () => {
      expect(component.saveOrganisation()).toBeFalsy();
    });

    it('should save the form if there are no validation errors', () => {
      const formGroup: FormGroup = new FormGroup({
        id: new FormControl(''),
        orgaName: new FormControl('test', Validators.required)
      });
      component.form = formGroup;
      expect(component.saveOrganisation()).toBeTruthy();
    });
  });
});
