/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { MasterdataManagementModule } from '@masterdata/masterdata-management.module';

describe('MasterdataManagementModule', () => {
  let masterdataManagementModule: MasterdataManagementModule;

  beforeEach(() => {
    masterdataManagementModule = new MasterdataManagementModule();
  });

  it('should create an instance', () => {
    expect(masterdataManagementModule).toBeTruthy();
  });
});
