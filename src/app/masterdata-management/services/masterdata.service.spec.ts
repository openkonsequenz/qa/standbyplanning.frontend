/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { TestBed, inject } from '@angular/core/testing';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { UtilService } from '@core/services/util.service';
import { UserObject } from '@shared/model/UserObject';
import { LocationObject } from '@shared/model/LocationObject';
import { RegionObject } from '@shared/model/RegionObject';
import { FunctionObject } from '@shared/model/FunctionObject';
import { StandbygroupObject } from '@shared/model/StandbygroupObject';
import { StandbyDurationObject } from '@shared/model/StandbyDurationObject';
import { BranchObject } from '@shared/model/BranchObject';
import { OrganisationObject } from '@shared/model/OrganisationObject';
import { PostcodeObject } from '@shared/model/PostcodeObject';
import { StandbylistObject } from '@shared/model/StandbylistObject';
import { DateObject } from '@shared/model/DateObject';

describe('MasterdataService', () => {
  let masterDataservice: MasterdataService;
  let utilService: UtilService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        MasterdataService,
        UtilService
      ]
    });

    masterDataservice = TestBed.get(MasterdataService);
    utilService = TestBed.get(UtilService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', inject([MasterdataService], (service: MasterdataService) => {
    expect(service).toBeTruthy();
  }));

  /**
   * User
   */
  it('should get User Data', () => {
    masterDataservice.getUserData().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/user/list`);
  });

  it('should get User Selection Data', () => {
    masterDataservice.getUserDataSelection().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/user/selectionlist`);
  });

  it('should get User 1', () => {
    masterDataservice.getUser(1).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/user/1`);
  });

  it('should save User', () => {
    const userObj = new UserObject();
    masterDataservice.saveUser(userObj).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/user/save`);
  });

  it('should get Users for Standbylist (source)', () => {
    masterDataservice.getUserInStandbygroup().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbygroupuser/list`);
  });

  it('should save a list of User to a standbygroup', () => {
    const userObj = new UserObject();
    masterDataservice.addStandbyGroupUser(1, [userObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbygroup/1/user/save/list`);
  });

  it('should delete a list of User to a standbygroup', () => {
    const userObj = new UserObject();
    masterDataservice.deleteStandbyGroupUser(1, [userObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbygroup/1/user/delete/list`);
  });

  it('should save a list of regions to a user', () => {
    const regionObj = new RegionObject();
    masterDataservice.addUserRegion(1, [regionObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/user/1/region/save/list`);
  });

  it('should delete a list of regions in a user', () => {
    const regionObj = new RegionObject();
    masterDataservice.deleteUserRegion(1, [regionObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/user/1/region/delete/list`);
  });

  it('should save a list of functions to a user', () => {
    const functionObj = new FunctionObject();
    masterDataservice.addUserFunction(1, [functionObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/user/1/userfunction/save/list`);
  });

  it('should delete a list of functions in a user', () => {
    const functionObj = new FunctionObject();
    masterDataservice.deleteUserFunction(1, [functionObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/user/1/userfunction/delete/list`);
  });

  it('should filter Users for Standbylist', () => {
    masterDataservice.filterUsersByStandbygroup(1).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/user/filter/standbygroup/1`);
  });

  it('should return a list of users for dropdownlists', () => {
    masterDataservice.getUserlistDropdown().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/user/dropdownlist`);
  });

  /**
   * Organisation
   */
  it('should get Organisation Data', () => {
    masterDataservice.getOrganisationData().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/organisation/list`);
  });

  it('should get Organisation Selection Data', () => {
    masterDataservice.getOrganisationDataSelection().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/organisation/selectionlist`);
  });

  it('should get Organisation 1', () => {
    masterDataservice.getOrganisation(1).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/organisation/1`);
  });

  it('should save Organisation', () => {
    const orgaObj = new OrganisationObject();
    masterDataservice.saveOrganisation(orgaObj).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/organisation/save`);
  });

  /**
   * Location
   */
  it('should get Location Data', () => {
    masterDataservice.getLocationData().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/location/list`);
  });

  it('should get a selection of Location Data', () => {
    masterDataservice.getLocationDataSelection().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/location/selectionlist`);
  });

  it('should save Location', () => {
    const locationObj = new LocationObject();
    masterDataservice.saveLocation(locationObj).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/location/save`);
  });

  it('should get Location 1', () => {
    masterDataservice.getLocation(1).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/location/1`);
  });

  it('should save a list of regions to a location', () => {
    const regionObj = new RegionObject();
    masterDataservice.addLocationRegion(1, [regionObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/location/1/region/save/list`);
  });

  it('should delete a list of regions in a location', () => {
    const regionObj = new RegionObject();
    masterDataservice.deleteLocationRegion(1, [regionObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/location/1/region/delete/list`);
  });

  it('should save a list of postcodes to a location', () => {
    const postcodeObj = new PostcodeObject();
    masterDataservice.addPostcodes(1, [postcodeObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/location/1/postcode/save/list`);
  });

  it('should delete a list of postcodes in a location', () => {
    const postcodeObj = new PostcodeObject();
    masterDataservice.deletePostcodes(1, [postcodeObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/location/1/postcode/delete/list`);
  });

  /**
   * Region
   */
  it('should get Region Data', () => {
    masterDataservice.getRegionData().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/region/list`);
  });

  it('should get a selection of Region Data', () => {
    masterDataservice.getRegionDataSelection().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/region/selectionlist`);
  });

  it('should get a list of aggregated regions and functions', () => {
    masterDataservice.getRegionHasFunction().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/regionhasfunction/list`);
  });

  it('should save Region', () => {
    const regionObj = new RegionObject();
    masterDataservice.saveRegion(regionObj).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/region/save`);
  });

  it('should get Region 1', () => {
    masterDataservice.getRegion(1).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/region/1`);
  });

  it('should save a list of Regions to a standbygroup', () => {
    const regionObj = new RegionObject();
    masterDataservice.addStandbyGroupRegion(1, [regionObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbygroup/1/region/save/list`);
  });

  it('should delete a list of Regions to a standbygroup', () => {
    const regionObj = new RegionObject();
    masterDataservice.deleteStandbyGroupRegion(1, [regionObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbygroup/1/region/delete/list`);
  });

  it('should save a list of locations to a region', () => {
    const locationObj = new LocationObject();
    masterDataservice.addRegionLocation(1, [locationObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/region/1/location/save/list`);
  });

  it('should delete a list of regions in a location', () => {
    const locationObj = new LocationObject();
    masterDataservice.deleteRegionLocation(1, [locationObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/region/1/location/delete/list`);
  });

  it('should save a list of functions to a region', () => {
    const functionObj = new FunctionObject();
    masterDataservice.addRegionFunction(1, [functionObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/region/1/userfunction/save/list`);
  });

  it('should delete a list of functions in a location', () => {
    const functionObj = new FunctionObject();
    masterDataservice.deleteRegionFunction(1, [functionObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/region/1/userfunction/delete/list`);
  });

  /**
   * Function
   */
  it('should get Function Data', () => {
    masterDataservice.getFunctionData().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/userfunction/list`);
  });

  it('should get a selection of Function Data', () => {
    masterDataservice.getFunctionDataSelection().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/userfunction/selectionlist`);
  });

  it('should save Function', () => {
    const functionObj = new FunctionObject();
    masterDataservice.saveFunction(functionObj).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/userfunction/save`);
  });

  it('should get Function 1', () => {
    masterDataservice.getFunction(1).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/userfunction/1`);
  });

  /**
  * Postcodes
  */

  it('should get Postcode Data', () => {
    masterDataservice.getPostcodeData().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/postcode/list`);
  });

  /**
   * Standbygroups
   */

  it('should get Standbygroup Data', () => {
    masterDataservice.getStandbygroupData().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbygroup/list`);
  });

  it('should get a selection of Standbygroup Data', () => {
    masterDataservice.getStandbygroupSelection().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbygroup/selectionlist`);
  });

  it('should save Region', () => {
    const standbygroupObj = new StandbygroupObject();
    masterDataservice.saveStandbygroup(standbygroupObj).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbygroup/save`);
  });

  it('should get Standbygroup 1', () => {
    masterDataservice.getStandbygroup(1).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbygroup/1`);
  });

  it('should get all Unique Users of Standbygroup 1', () => {
    masterDataservice.getStandbygroupUniqueUser(1).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbygroup/1/user/list/unique`);
  });

  it('should add userfunctions to standbygroup', () => {
    const functionObj = new FunctionObject();
    masterDataservice.addStandbygroupFunction(1, [functionObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbygroup/1/userfunction/save/list`);
  });

  it('should delete the standbyduration with id 1', () => {
    const functionObj = new FunctionObject();
    masterDataservice.deleteStandbygroupFunction(1, [functionObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbygroup/1/userfunction/delete/list`);
  });

  it('should add calendar objects to standbygroup with id 1', () => {
    const calendarObj = new DateObject();
    masterDataservice.addStandbygroupCalendar(1, [calendarObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbygroup/1/calendar/ignore/save/list`);
  });

  it('should delete the calendar objects in standbygroup with id 1', () => {
    const calendarObj = new DateObject();
    masterDataservice.deleteStandbygroupCalendar(1, [calendarObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbygroup/1/calendar/ignore/delete/list`);
  });

  it('should get Standbygroup 1', () => {
    masterDataservice.getStandbygroupsAndLists().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbygroup/transfer/list`);
  });

  /**
   * Standbygroupduration
   */

  it('should save a standbyduration', () => {
    const standbygroupObj = new StandbyDurationObject();
    masterDataservice.saveStandbygroupDuration(1, standbygroupObj).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbygroup/1/duration/save/list`);
  });

  it('should delete the standbyduration with id 1', () => {
    const standbygroupObj = new StandbyDurationObject();
    masterDataservice.deleteStandbygroupDuration(1, [standbygroupObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbygroup/1/duration/delete/list`);
  });

  /**
   * Branch
   */
  it('should get a selection of Branch Data', () => {
    masterDataservice.getBranchDataSelection().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/branch/selectionlist`);
  });

  it('should save a list of branches in a standbygroup', () => {
    const branchObj = new BranchObject();
    masterDataservice.addStandbyGroupBranch(1, [branchObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbygroup/1/branch/save/list`);
  });

  it('should delete a list of branches in standbygroup', () => {
    const branchObj = new BranchObject();
    masterDataservice.deleteStandbyGroupBranch(1, [branchObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbygroup/1/branch/delete/list`);
  });

  it('should save a list of branches in a location', () => {
    const branchObj = new BranchObject();
    masterDataservice.addLocationBranch(1, [branchObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/location/1/branch/save/list`);
  });

  it('should delete a list of branches in location', () => {
    const branchObj = new BranchObject();
    masterDataservice.deleteLocationBranch(1, [branchObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/location/1/branch/delete/list`);
  });

  /**
   * Standbylists
   */

  it('should get Standbylist Data', () => {
    masterDataservice.getStandbyListData().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbylist/list`);
  });

  it('should get a selection of Standbylist Data', () => {
    masterDataservice.getStandbyListSelection().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbylist/selectionlist`);
  });

  it('should get Standbylist Data', () => {
    masterDataservice.getStandbyListDropdown().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbylist/dropdownlist`);
  });

  it('should save Standbylist', () => {
    const standbylistObj = new StandbylistObject();
    masterDataservice.saveStandbylist(standbylistObj).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbylist/save`);
  });

  it('should get Standbylist 1', () => {
    masterDataservice.getStandbylist(1).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbylist/1`);
  });

  it('should save a list of standbygroups in a standbylist', () => {
    const standbygroupObj = new StandbygroupObject();
    masterDataservice.addStandbylistStandbygroup(1, [standbygroupObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbylist/1/standbygroup/save/list`);
  });

  it('should delete a list of standbygroups in a standbylist', () => {
    const standbygroupObj = new StandbygroupObject();
    masterDataservice.deleteStandbylistStandbygroup(1, [standbygroupObj]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbylist/1/standbygroup/delete/list`);
  });

  /**
   * Calendar
   */

  it('should get Calendar Data', () => {
    masterDataservice.getCalendarData().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/calendar/list`);
  });

  it('should save DateObject', () => {
    const dateObj = new DateObject();
    masterDataservice.saveDate(dateObj).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/calendar/save`);
  });

  it('should delete DateObject', () => {
    const dateObj = { id: 1 };
    masterDataservice.deleteDate(dateObj).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/calendar/delete`);
  });

  it('should get Date 1', () => {
    masterDataservice.getDate(1).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/calendar/1`);
  });

  afterEach(() => {
    httpMock.verify();
  });
});
