import { Component, OnInit } from '@angular/core';
/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { FormBuilder, FormGroup } from '@angular/forms';

import { AbstractDialogComponent } from '@shared/abstract/abstract-dialog/abstract-dialog.component';

@Component({
  selector: 'ok-copy-dialog',
  templateUrl: './copy-dialog.component.html',
  styleUrls: ['./copy-dialog.component.scss']
})
export class CopyDialogComponent extends AbstractDialogComponent implements OnInit {

  form: FormGroup = this.fb.group({
    copyRegion: true,
    copyFunction: true,
    copyUser: true,
    copyBranch: true,
    copyCalendar: true,
    copyDuration: true,
    userPlusOneYear: true
  });
  constructor(
    private fb: FormBuilder
  ) {
    super();
  }

  ngOnInit() {
  }

  decide(decision) {
    const dataToTransmit = this.form.getRawValue();
    super.decide(decision, dataToTransmit);
  }

}
