/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, EventEmitter, Output, Injector, Input } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { FormUtil } from '@shared/utils/form.util';
import { DatepickerValidator } from '@shared/validators/datepicker.validator';
import { AbstractDialogComponent } from '@shared/abstract/abstract-dialog/abstract-dialog.component';

@Component({
  selector: 'ok-validity-dialog',
  templateUrl: './validity-dialog.component.html',
  styleUrls: ['./validity-dialog.component.scss']
})
export class ValidityDialogComponent extends AbstractDialogComponent implements OnInit {
  @Output()
  decision: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  saveButtonLabel = 'Verschieben';

  form: FormGroup = this.fb.group({
    date: this.fb.group({
      validFrom: ['', Validators.required],
      validTo: ['', [Validators.required]],
    }, { validator: [DatepickerValidator.dateRangeTo('')] }),
    position: ''
  });
  constructor(
    private fb: FormBuilder
  ) {
    super();
  }

  ngOnInit() { }

  decide(decision) {
    const dataToTransmit = this.form.getRawValue();
    FormUtil.formatDates(dataToTransmit, ['validFrom', 'validTo']);
    super.decide(decision, dataToTransmit);
  }

  setDefaultDate(field: string) {
    FormUtil.setDefaultDate(this.form, field);
  }

}
