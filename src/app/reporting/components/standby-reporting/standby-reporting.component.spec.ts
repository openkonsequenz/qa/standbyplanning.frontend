/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { MessageService } from 'primeng/components/common/messageservice';

import { StandbyReportingComponent } from './standby-reporting.component';
import { SharedModule } from '@shared/shared.module';
import { AlertComponent } from '@shared/components/alert/alert.component';
import { MasterdataService } from '@masterdata/services/masterdata.service';
import { ReportingService } from '@reporting/services/reporting.service';
import { UserObject } from '@shared/model/UserObject';
import { ReportObject } from '@shared/model/ReportObject';
import { ReportingMockObjects } from '@shared/testing/reporting';

const reportingMockObjects = new ReportingMockObjects;

export class ReportingServiceMock {
  generateReport() {
    return of({});
  }

  getReportData() {
    return of([new ReportObject()]);
  }
}

export class MasterDataServiceMock {
  getStandbyListSelection() {
    return of([reportingMockObjects.STANDBYLIST_SELECTION_ARRAY]);
  }
  getUserData() {
    return of([new UserObject()]);
  }
  getUserlistDropdown() {
    return of(reportingMockObjects.USER_DROPDOWN_DATA_LIST);
  }
}

describe('StandbyReportingComponent', () => {
  let component: StandbyReportingComponent;
  let fixture: ComponentFixture<StandbyReportingComponent>;
  const routes: Routes = [
    {
      path: '**',
      component: AlertComponent
    }
  ];
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StandbyReportingComponent],
      imports: [
        SharedModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        MessageService,
        {
          provide: MasterdataService,
          useClass: MasterDataServiceMock
        },
        {
          provide: ReportingService,
          useClass: ReportingServiceMock
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandbyReportingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('setDefaultDate', () => {
    it('should set validFrom to current day', () => {
      const date = new Date();
      component.setDefaultDate('validFrom');
      expect(component.form.get('date').get('validFrom').value).toEqual({
        day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear()
      });
    });

    it('should set validTo to current day + 15 years', () => {
      const date = new Date();
      component.setDefaultDate('validTo');
      expect(component.form.get('date').get('validTo').value).toEqual({
        day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear() + 15
      });
    });
  });

  describe('generate()', () => {
    it('shouldn´t generate if form is invalid', () => {
      component.generate();
    });
    it('should generate if form is valid', () => {
      component.setDefaultDate('validFrom');
      component.setDefaultDate('validTo');
      component.form.patchValue({
        standById: 1,
        reportName: 'test',
        printFormat: 'pdf'
      });
      component.generate();
    });
  });
});
