/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilService } from '@core/services/util.service';
import { ReportObject } from '@shared/model/ReportObject';
import { StandbySearchList } from '@shared/model/StandbySearchList';

@Injectable({
  providedIn: 'root'
})
export class ReportingService {

  constructor(
    private http: HttpClient,
    private utilService: UtilService
  ) { }


  getReportData() {
    return this.http.get<ReportObject[]>(`${this.utilService.readConfig('basePath')}/report/list`);
  }

  generateReport(formValue: Object) {
    return this.http.put(
      `${this.utilService.readConfig('basePath')}/report/generate`, formValue,
      { responseType: 'arraybuffer' }
    );
  }

  getSearchList() {
    return this.http.get<StandbySearchList[]>(`${this.utilService.readConfig('basePath')}/location/searchlist`);
  }
}
