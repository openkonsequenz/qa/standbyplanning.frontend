/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Mettenmeier GmbH - initial implementation
 *   Basys GmbH - automatic report generation implementation
 ********************************************************************************/

import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CoreModule } from '@core/core.module';

import { MessageService } from 'primeng/components/common/messageservice';
import { NgbDateParserFormatter, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';

import { MasterdataManagementModule } from '@masterdata/masterdata-management.module';
import { AuthInterceptor } from '@core/interceptors/auth.interceptor';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ErrorInterceptor } from '@core/interceptors/error.interceptor';
import { StandbyScheduleModule } from '@schedule/standby-schedule.module';
import { NgbDateStringParserFormatter } from '@shared/utils/dateFormatter.util';
import { ReportingModule } from '@reporting/reporting.module';
import { CustomDatepickerI18nService } from '@shared/utils/custom-datepicker-i18n.service';
import { CyclicReportingModule } from '@cyclic-reporting/cyclic-reporting.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    MasterdataManagementModule,
    StandbyScheduleModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReportingModule,
    CyclicReportingModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: ErrorHandler,
      useClass: ErrorInterceptor,
    },
    {
      provide: NgbDateParserFormatter,
      useClass: NgbDateStringParserFormatter
    },
    {
      provide: NgbDatepickerI18n,
      useClass: CustomDatepickerI18nService
    },
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
