/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@env/environment';
import { Subject, BehaviorSubject } from 'rxjs';
import { ErrorObject } from '@shared/model/ErrorObject';

@Injectable({
  providedIn: 'root'
})
export class UtilService {
  error$: Subject<ErrorObject[]> = new Subject();
  loadingState$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private http: HttpClient
  ) { }

  readConfig(key: string) {
    if (environment.hasOwnProperty(key)) {
      return environment[key];
    } else {
      console.error('Configuration property not found');
      return '';
    }
  }

  throwError(errorObject: ErrorObject[]) {
    this.error$.next(errorObject);
  }

  getDocuAsPdf(path: string) {
    return this.http.get(
      `${this.readConfig('basePath')}${path}`,
      { responseType: 'blob' }
    );
  }
}
