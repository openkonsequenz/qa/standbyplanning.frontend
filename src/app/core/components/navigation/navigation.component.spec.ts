/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationComponent } from '@core/components/navigation/navigation.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';
import { Router } from '@angular/router';

import { AuthenticationService } from '@core/services/authentication.service';
import { LogoutComponent } from '@core/components/logout/logout.component';
import { LoadingSpinnerComponent } from '@core/components/loading-spinner/loading-spinner.component';
import { SharedModule } from '@shared/shared.module';

export class AuthServiceMock extends AuthenticationService {
  logout() {
    return of(true);
  }
}

describe('NavigationComponent', () => {
  let component: NavigationComponent;
  let fixture: ComponentFixture<NavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NavigationComponent, LogoutComponent, LoadingSpinnerComponent],
      imports: [
        RouterTestingModule.withRoutes(
          [{ path: 'loggedOut', component: LogoutComponent }]
        ),
        HttpClientModule,
        RouterTestingModule,
        SharedModule
      ],
      providers: [
        {
          provide: AuthenticationService,
          useClass: AuthServiceMock
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call logout without error', () => {
    const router = TestBed.get(Router);
    try {
      component.logout();
      router.navigate(['/loggedOut']);
    } catch (e) {
      expect(e).toBeFalsy();
    }
  });
});
