/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationCancel, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';

import { saveAs } from 'file-saver';

import { AuthenticationService } from '@core/services/authentication.service';
import { UtilService } from '@core/services/util.service';

@Component({
  selector: 'ok-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit, OnDestroy {
  navbarCollapsed = true;
  routerEvent$: Subscription;

  constructor(
    public authService: AuthenticationService,
    private router: Router,
    private utilService: UtilService
  ) { }

  ngOnInit() {
  }

  logout() {
    this.routerEvent$ = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.authService.logout().subscribe(() => {
          localStorage.clear();
        });
      }
      if (event instanceof NavigationCancel || event instanceof NavigationEnd) {
        this.routerEvent$.unsubscribe();
      }
    });
  }

  ngOnDestroy() {
    if (this.routerEvent$) {
      this.routerEvent$.unsubscribe();
    }
  }

  downloadPdf() {
    this.utilService.getDocuAsPdf('/documents/user/documentation').subscribe((pdf: any) => {
      saveAs(pdf, 'Benutzerdokumentation.pdf');
    });
  }
}
