/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { UtilService } from '@core/services/util.service';
import { ErrorObject } from '@shared/model/ErrorObject';
import { Router, NavigationStart } from '@angular/router';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'ok-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {
  errorMessage: ErrorObject[];
  constructor(
    public utilService: UtilService,
    private ref: ChangeDetectorRef,
    private router: Router,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.utilService.error$.subscribe((errorMessage) => {
      if (errorMessage) {
        // the toast stays on page until next change detection cycle (due to the detectChanges() call below)
        this.messageService.add({ severity: 'error', summary: 'Ein Fehler ist aufgetreten!', detail: '' });
      }
      this.errorMessage = errorMessage;
      this.ref.detectChanges(); // needed due to refresh issues
    });
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.errorMessage = undefined;
      }
    });
  }

}
