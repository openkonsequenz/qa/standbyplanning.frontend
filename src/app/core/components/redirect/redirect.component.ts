/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, InjectionToken, Inject } from '@angular/core';
import { UtilService } from '@core/services/util.service';

export const LOCATION_TOKEN = new InjectionToken<Location>('Window location object');

@Component({
  selector: 'ok-redirect',
  templateUrl: './redirect.component.html',
  styleUrls: ['./redirect.component.scss']
})
export class RedirectComponent implements OnInit {

  constructor(
    private utilService: UtilService,
    @Inject(LOCATION_TOKEN) private location: Location
  ) { }

  ngOnInit() {
    this.location.replace(`${this.utilService.readConfig('loginPage')}`);
  }

}
