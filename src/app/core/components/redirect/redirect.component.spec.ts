/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedirectComponent, LOCATION_TOKEN } from '@core/components/redirect/redirect.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('RedirectComponent', () => {
  let component: RedirectComponent;
  let fixture: ComponentFixture<RedirectComponent>;
  const location = {
    replace: jasmine.createSpy('replace')
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RedirectComponent],
      providers: [
        { provide: LOCATION_TOKEN, useValue: location }
      ],
      imports: [HttpClientTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should redirect to login page', () => {
    expect(location.replace).toHaveBeenCalled();
  });
});
