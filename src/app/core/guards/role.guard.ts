/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '@core/services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(
    private authService: AuthenticationService,
    private router: Router
  ) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let expectedRoles;
    if (next && next.data && next.data.expectedRoles) {
      expectedRoles = next.data.expectedRoles;
    }
    const userRoles = this.authService.getUserRoles();
    if (expectedRoles) {
      // check if userRoles can be found in expectedRoles
      if (!expectedRoles.some(v => userRoles.includes(v))) {
        this.router.navigate(['/dashboard']);
        return false;
      }
    }
    return true;
  }
}
