/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { TestBed, async, inject } from '@angular/core/testing';

import { CanDeactivateGuard } from '@core/guards/can-deactivate.guard';

describe('CanDeactivateGuard', () => {
  beforeEach(() => {

    TestBed.configureTestingModule({
      providers: [
        CanDeactivateGuard
      ]
    });

  });

  it('should ...', inject([CanDeactivateGuard], (guard: CanDeactivateGuard) => {
    expect(guard).toBeTruthy();
  }));

  it('should return true if the route can be deactivated ', inject([CanDeactivateGuard], (guard: CanDeactivateGuard) => {
    const component = {
      canDeactivate: () => {
        return true;
      }
    };
    const route: any = undefined;
    const state: any = undefined;
    const canDeactive = guard.canDeactivate(component, route, state);
    expect(canDeactive).toBeTruthy();
  }));

  it('should return false if the route can be deactivated ', inject([CanDeactivateGuard], (guard: CanDeactivateGuard) => {
    const component = {
      canDeactivate: () => {
        return false;
      }
    };
    const route: any = undefined;
    const state: any = undefined;
    const canDeactive = guard.canDeactivate(component, route, state);
    expect(canDeactive).toBeFalsy();
  }));
});
