/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Mettenmeier GmbH - initial implementation
 *   Basys GmbH - automatic report generation implementation
 ********************************************************************************/

import {ErrorHandler, Injectable, Injector} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {UtilService} from '@core/services/util.service';
import {ErrorObject} from '@shared/model/ErrorObject';

@Injectable()
export class ErrorInterceptor implements ErrorHandler {

  constructor(
    private injector: Injector
  ) { }


  public handleError(httpErrorResponse: any) {
    if (httpErrorResponse instanceof HttpErrorResponse) {
      const utilService = this.injector.get(UtilService);
      let errorMessage: ErrorObject[];
      if (Array.isArray(httpErrorResponse.error)) { // should be any kind of validation error
        httpErrorResponse.error.forEach(error => {
          error.httpStatus = httpErrorResponse.status;
        });
        errorMessage = httpErrorResponse.error;
      } else if (httpErrorResponse.error && httpErrorResponse.error.byteLength !== undefined) { // reports error
        const encodedString = String.fromCharCode.apply(null, new Uint8Array(httpErrorResponse.error));
        const decodedString = decodeURIComponent(escape(encodedString));
        if (decodedString) {
          errorMessage = [JSON.parse(decodedString)];
        }
      } else { // any other error
        errorMessage = [{
          httpStatus: httpErrorResponse.error.httpStatus,
          message: httpErrorResponse.error.message,
          localizedMessage: httpErrorResponse.error.localizedMessage
        }];
        if (errorMessage[0].httpStatus === undefined) {
          errorMessage[0].httpStatus = httpErrorResponse.status;
          errorMessage[0].message = httpErrorResponse.statusText;
          errorMessage[0].localizedMessage = httpErrorResponse.statusText;
        }
      }
      utilService.throwError(errorMessage);
    } else {
      console.error(httpErrorResponse);
    }
  }
}
