/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from '@angular/core';
import {CyclicReportObject} from '@shared/model/CyclicReportObject';
import {SelectOptionObject} from '@shared/model/SelectOptionObject';

@Injectable({ providedIn: 'root' })
export class CyclicReportingUtilService {

  public readonly dateReplacementTokens: {[token: string]: string} = {
    '{Date}': 'yyyyMMdd',
    '{Time}': 'HHmm',
    '{Week}': 'ww'
  };

  public readonly excludedReportNames: string[] = ['Persönlicher_Einsatzplan'];

  public readonly printFormatOptions: SelectOptionObject<string>[] = ['pdf', 'xlsx']
    .map((value) => ({value, label: value.toUpperCase()}));

  public readonly planStatusOptions: SelectOptionObject<number>[] = [ 'Plan-Ebene', 'Ist-Ebene' ]
    .map((label, index) => ({ label, value: index + 1 })).reverse();

  public readonly weekDayOptions: SelectOptionObject<number>[] = [
    'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag'
  ].map((label, index) => ({label, value: index + 1}));

  public readonly oneDayInMs = 1000 * 60 * 60 * 24;


  public formatWeekDay = (weekDay: number) => {
    return this.extractLabelForValue((weekDay + 6) % 7 + 1, this.weekDayOptions);
  }

  public extractLabelForValue = <T>(value: T, options: Array<{ label: string, value: T }>): string => {
    const element = Array.isArray(options) ? options.find((el) => el.value === value) : undefined;
    return element == null ? undefined : element.label;
  }

  public cyclicReportTriggerComparator = (reportA: CyclicReportObject, reportB: CyclicReportObject): number => {
    if (reportA == null || reportB == null || reportA === reportB) {
      return 0;
    }
    return ['triggerWeekDay', 'triggerHour', 'triggerMinute']
      .filter((key) => [reportA, reportB].every((report) => Number.isInteger(report[key])))
      .map((key) => reportA[key] - reportB[key])
      .reduce<number>((result, current) => result !== 0 ? result : current, 0);
  }

  public getNextTriggerDate = (report: CyclicReportObject, from: Date = new Date()) => {
    const isTriggerValid = report != null && [report.triggerWeekDay, report.triggerHour, report.triggerMinute].every(Number.isInteger);

    if (!isTriggerValid) {
      return undefined;
    }

    const fromDay = from.getDay();
    const fromHour = from.getHours();
    const fromMinute = from.getMinutes();

    const triggerDay = report.triggerWeekDay % 7;
    const triggerHour = report.triggerHour;
    const triggerMinute = report.triggerMinute;

    const isTriggerInNextWeek = fromDay === triggerDay &&
      (triggerHour < fromHour || triggerHour === fromHour && triggerMinute < fromMinute);
    const dayDiff = isTriggerInNextWeek ? 7 : triggerDay - fromDay;

    return this.moveDateByDays(from, dayDiff < 0 ? 7 + dayDiff : dayDiff, triggerHour, triggerMinute);
  }

  public moveDateByDays = (date: Date, dayDiff: number, hour: number, minute: number) => {
    if (date == null || ![dayDiff, hour, minute].every(Number.isInteger)) {
      return undefined;
    }
    const result = new Date();
    result.setTime(date.getTime() + dayDiff * this.oneDayInMs);
    result.setHours(hour, minute, 0, 0);
    return result;
  }

}
