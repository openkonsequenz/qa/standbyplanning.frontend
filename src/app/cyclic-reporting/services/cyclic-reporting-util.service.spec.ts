/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {TestBed} from '@angular/core/testing';
import {CyclicReportObject} from '@shared/model/CyclicReportObject';
import {CyclicReportingUtilService} from './cyclic-reporting-util.service';

describe('CyclicReportingUtilService', () => {

  let cyclicReportingUtilService: CyclicReportingUtilService;

  function createData(data?: Partial<CyclicReportObject>): CyclicReportObject {
    return {
      ...{} as CyclicReportObject,
      ...data
    };
  }

  beforeEach(() => {
    cyclicReportingUtilService = TestBed.get(CyclicReportingUtilService);
  });

  it('formatWeekDay', () => {
    expect(cyclicReportingUtilService.formatWeekDay(0)).toBe('Sonntag');
    expect(cyclicReportingUtilService.formatWeekDay(1)).toBe('Montag');
    expect(cyclicReportingUtilService.formatWeekDay(2)).toBe('Dienstag');
    expect(cyclicReportingUtilService.formatWeekDay(3)).toBe('Mittwoch');
    expect(cyclicReportingUtilService.formatWeekDay(4)).toBe('Donnerstag');
    expect(cyclicReportingUtilService.formatWeekDay(5)).toBe('Freitag');
    expect(cyclicReportingUtilService.formatWeekDay(6)).toBe('Samstag');
    expect(cyclicReportingUtilService.formatWeekDay(7)).toBe('Sonntag');
  });

  it('extractLabelForValue', () => {
    const options = ['A', 'B'].map((label, value) => ({ label, value }));
    expect(cyclicReportingUtilService.extractLabelForValue(0, options)).toBe('A');
    expect(cyclicReportingUtilService.extractLabelForValue(1, null)).toBe(undefined);
    expect(cyclicReportingUtilService.extractLabelForValue(19, options)).toBe(undefined);
  });

  it('cyclicReportTriggerComparator', () => {
    const dataA = createData({ triggerWeekDay: 1, triggerHour: 1, triggerMinute: 1 });
    const dataB = createData({ triggerWeekDay: 1, triggerHour: 1, triggerMinute: 2 });
    const dataC = createData({ triggerWeekDay: 1, triggerHour: 2, triggerMinute: 1 });
    const dataD = createData({ triggerWeekDay: 2, triggerHour: 1, triggerMinute: 1 });

    expect(cyclicReportingUtilService.cyclicReportTriggerComparator(null, null)).toBe(0);
    expect(cyclicReportingUtilService.cyclicReportTriggerComparator(dataA, null)).toBe(0);
    expect(cyclicReportingUtilService.cyclicReportTriggerComparator(null, dataA)).toBe(0);
    expect(cyclicReportingUtilService.cyclicReportTriggerComparator(dataA, dataA)).toBe(0);
    expect(cyclicReportingUtilService.cyclicReportTriggerComparator(dataA, dataB)).toBe(-1);
    expect(cyclicReportingUtilService.cyclicReportTriggerComparator(dataB, dataA)).toBe(1);
    expect(cyclicReportingUtilService.cyclicReportTriggerComparator(dataA, dataC)).toBe(-1);
    expect(cyclicReportingUtilService.cyclicReportTriggerComparator(dataC, dataA)).toBe(1);
    expect(cyclicReportingUtilService.cyclicReportTriggerComparator(dataA, dataD)).toBe(-1);
    expect(cyclicReportingUtilService.cyclicReportTriggerComparator(dataD, dataA)).toBe(1);
  });

  it('getNextTriggerDate', () => {
    let data: CyclicReportObject;
    let expectedDate: Date;
    const from = new Date(2020, 11, 24, 19, 0);

    function createDataForDate(date: Date): CyclicReportObject {
      return createData({triggerWeekDay: date.getDay(), triggerHour: date.getHours(), triggerMinute: date.getMinutes()});
    }

    expectedDate = new Date(from);
    expectedDate.setHours(from.getHours() + 24, from.getMinutes(), 0, 0);
    data = createDataForDate(expectedDate);
    expect(cyclicReportingUtilService.getNextTriggerDate(data, from).getTime())
      .toBe(from.getTime() + 1000 * 60 * 60 * 24);

    expectedDate = new Date(from);
    expectedDate.setHours(from.getHours() + 2, from.getMinutes(), 0, 0);
    data = createDataForDate(expectedDate);
    expect(cyclicReportingUtilService.getNextTriggerDate(data, from).getTime())
      .toBe(from.getTime() + 1000 * 60 * 60 * 2);

    expectedDate = new Date(from);
    expectedDate.setHours(from.getHours(), from.getMinutes() + 15, 0, 0);
    data = createDataForDate(expectedDate);
    expect(cyclicReportingUtilService.getNextTriggerDate(data, from).getTime())
      .toBe(from.getTime() + 1000 * 60 * 15);

    expectedDate = new Date(from);
    expectedDate.setHours(from.getHours(), from.getMinutes() - 15, 0, 0);
    data = createDataForDate(expectedDate);
    expect(cyclicReportingUtilService.getNextTriggerDate(data, from).getTime())
      .toBe(from.getTime() - 1000 * 60 * 15 + 1000 * 60 * 60 * 24 * 7);

    expect(cyclicReportingUtilService.getNextTriggerDate(null, from)).not.toBeDefined();
    expect(cyclicReportingUtilService.getNextTriggerDate(
      createData({triggerWeekDay: null, triggerHour: 1, triggerMinute: 1}), from)
    ).not.toBeDefined();
    expect(cyclicReportingUtilService.getNextTriggerDate(
      createData({triggerWeekDay: 1, triggerHour: null, triggerMinute: 1}), from)
    ).not.toBeDefined();
    expect(cyclicReportingUtilService.getNextTriggerDate(
      createData({triggerWeekDay: 1, triggerHour: 1, triggerMinute: null}), from)
    ).not.toBeDefined();
  });

  it('moveDateByDays', () => {
    const from = new Date(2020, 11, 24, 19, 0);
    const to = new Date(from);

    to.setTime(from.getTime() + 1000 * 60 * 60 * 24 * 7);
    expect(cyclicReportingUtilService.moveDateByDays(from, 7, to.getHours(), to.getMinutes()).getTime()).toBe(to.getTime());

    to.setTime(from.getTime() - 1000 * 60 * 60 * 24 * 3);
    to.setHours(12, 11, 0, 0);
    expect(cyclicReportingUtilService.moveDateByDays(from, -3, to.getHours(), to.getMinutes()).getTime()).toBe(to.getTime());

    expect(cyclicReportingUtilService.moveDateByDays(null, 1, 1, 1)).not.toBeDefined();
    expect(cyclicReportingUtilService.moveDateByDays(from, null, null, null)).not.toBeDefined();
    expect(cyclicReportingUtilService.moveDateByDays(from, null, 1, null)).not.toBeDefined();
    expect(cyclicReportingUtilService.moveDateByDays(from, null, null, 1)).not.toBeDefined();
  });

});
