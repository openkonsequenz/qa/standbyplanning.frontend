/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UtilService} from '@core/services/util.service';
import {CyclicReportObject} from '@shared/model/CyclicReportObject';
import {Observable} from 'rxjs';

@Injectable({ providedIn: 'root' })
export class CyclicReportingService {

  constructor(
    private http: HttpClient,
    private utilService: UtilService
  ) {

  }

  public getCyclicReports(): Observable<CyclicReportObject[]> {
    return this.http.get<CyclicReportObject[]>(`${this.utilService.readConfig('basePath')}/auto-reports`);
  }

  public putCyclicReport(data: CyclicReportObject): Observable<CyclicReportObject> {
    return this.http.put<CyclicReportObject>(`${this.utilService.readConfig('basePath')}/auto-reports`, data);
  }

  public postCyclicReport(data: CyclicReportObject) {
    return this.http.post<CyclicReportObject>(`${this.utilService.readConfig('basePath')}/auto-reports/${data.id}`, data);
  }

  public deleteCyclicReport(id: number) {
    return this.http.delete(`${this.utilService.readConfig('basePath')}/auto-reports/${id}`);
  }

}
