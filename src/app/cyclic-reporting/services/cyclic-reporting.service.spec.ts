/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {UtilService} from '@core/services/util.service';
import {CyclicReportObject} from '@shared/model/CyclicReportObject';
import {Observable, Subscription} from 'rxjs';
import {CyclicReportingService} from './cyclic-reporting.service';

describe('CyclicReportingService', () => {

  let cyclicReportingService: CyclicReportingService;
  let httpTestingController: HttpTestingController;
  let utilService: UtilService;

  let data: CyclicReportObject;
  let subscriptions: Subscription[];
  let hasFinished: boolean;

  function subscribeForTest<T>(observable: Observable<T>, results: T[] = []): Subscription {
    const subscription = observable.subscribe((value) => results.push(value), null, () => hasFinished = true);
    subscriptions.push(subscription);
    return subscription;
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });

    cyclicReportingService = TestBed.get(CyclicReportingService);
    httpTestingController = TestBed.get(HttpTestingController);
    utilService = TestBed.get(UtilService);
    data = { ...{} as CyclicReportObject, id: 19 };

    hasFinished = false;
    subscriptions = [];
  });

  afterEach(() => {
    expect(hasFinished).toBe(true);
    subscriptions.forEach((subscription) => subscription.unsubscribe());
    httpTestingController.verify();
  });

  it('getCyclicReports', () => {
    const results: CyclicReportObject[][] = [];
    subscribeForTest(cyclicReportingService.getCyclicReports(), results);
    const testRequest = httpTestingController.expectOne(`${utilService.readConfig('basePath')}/auto-reports`);
    testRequest.flush([data]);
    expect(testRequest.request.method).toBe('GET');
    expect(results).toEqual([[data]]);
  });

  it('putCyclicReports', () => {
    const results: CyclicReportObject[] = [];
    subscribeForTest(cyclicReportingService.putCyclicReport(data), results);
    const testRequest = httpTestingController.expectOne(`${utilService.readConfig('basePath')}/auto-reports`);
    testRequest.flush(data);
    expect(testRequest.request.method).toBe('PUT');
    expect(results).toEqual([data]);
  });

  it('postCyclicReport', () => {
    const results: CyclicReportObject[] = [];
    subscribeForTest(cyclicReportingService.postCyclicReport(data), results);
    const testRequest = httpTestingController.expectOne(`${utilService.readConfig('basePath')}/auto-reports/${data.id}`);
    testRequest.flush(data);
    expect(testRequest.request.method).toBe('POST');
    expect(results).toEqual([data]);
  });

  it('deleteCyclicReport', () => {
    subscribeForTest(cyclicReportingService.deleteCyclicReport(data.id));
    const testRequest = httpTestingController.expectOne(`${utilService.readConfig('basePath')}/auto-reports/${data.id}`);
    testRequest.flush(data);
    expect(testRequest.request.method).toBe('DELETE');
  });

});
