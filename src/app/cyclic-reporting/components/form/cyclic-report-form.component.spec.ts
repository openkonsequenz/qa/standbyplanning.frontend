/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from '@angular/common';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ActivatedRoute, Router} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {SharedModule} from '@shared/shared.module';
import {CyclicReportObject} from '@shared/model/CyclicReportObject';
import {ReportObject} from '@shared/model/ReportObject';
import {StandbylistObject} from '@shared/model/StandbylistObject';
import {FormUtil} from '@shared/utils/form.util';
import {MessageService} from 'primeng/api';
import {defer, of, throwError} from 'rxjs';
import {CyclicReportFormDateControlsComponent} from '../form-date-controls/cyclic-report-form-date-controls.component';
import {CyclicReportFormInfoComponent} from '../form-info/cyclic-report-form-info.component';
import {CyclicReportFormInputComponent} from '../form-input/cyclic-report-form-input.component';
import {CyclicReportFormSelectComponent} from '../form-select/cyclic-report-form-select.component';
import {CyclicReportFormComponent} from './cyclic-report-form.component';
import {CyclicReportFormTextareaComponent} from '@cyclic-reporting/components/form-textarea/cyclic-report-form-textarea.component';

function createMockData<T extends object>(data: Partial<T>): T {
  return (data == null ? {} : data) as T;
}

describe('CyclicReportFormComponent', () => {

  let component: CyclicReportFormComponent;
  let fixture: ComponentFixture<CyclicReportFormComponent>;
  let idParam: number | 'new';
  let report: CyclicReportObject;
  let router: Router;
  let activatedRoute: ActivatedRoute;

  const standByListData: StandbylistObject[] = Array(42).fill(0)
    .map((_, id) => createMockData<StandbylistObject>({ id: id, title: 'QVL ' + id }));

  const reportObjects: ReportObject[] = Array(42).fill(0)
    .map((_, id) => createMockData<ReportObject>({ reportName: 'ReportName' + id }));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CyclicReportFormComponent,
        CyclicReportFormDateControlsComponent,
        CyclicReportFormInfoComponent,
        CyclicReportFormInputComponent,
        CyclicReportFormSelectComponent,
        CyclicReportFormTextareaComponent
      ],
      imports: [
        CommonModule,
        SharedModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      providers: [
        MessageService,
        {
          provide: ActivatedRoute,
          useValue: { params: defer(() => of({id: idParam == null ? undefined : '' + idParam}))}
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CyclicReportFormComponent);
    component = fixture.componentInstance;
    component.excludedReportNames = reportObjects.slice(-1).map((reportObject) => reportObject.reportName);
    router = TestBed.get(Router);
    activatedRoute = TestBed.get(ActivatedRoute);
    idParam = 19;
    report = null;
    spyOn(component.reportingService, 'getReportData')
      .and.returnValue(defer(() => of(reportObjects)));
    spyOn(component.masterdataService, 'getStandbyListSelection')
      .and.returnValue(defer(() => of(standByListData)));
    spyOn(component.cyclicReportingService, 'getCyclicReports')
      .and.returnValue(defer(() => of(report == null ? [] : [report])));
  });

  describe('should create', () => {
    it('for new reports', () => {
      idParam = 'new';
      fixture.detectChanges();
      expect(component).toBeDefined();
    });

    it('for non-existing reports', () => {
      idParam = -19;
      fixture.detectChanges();
      expect(component).toBeDefined();
    });

    it('for existing report', () => {
      idParam = 19;
      report = createMockData<CyclicReportObject>({ id: 19, to: [ 'a@b.c', 'x@y.z' ], standByListId: 19 });
      fixture.detectChanges();
      expect(component).toBeDefined();
    });
  });

  it('should add and remove email controls', () => {
    idParam = 'new';
    fixture.detectChanges();
    expect(component.form.value.to).toEqual(['']);
    component.addEmailControl();
    expect(component.form.value.to).toEqual(['', '']);
    component.removeEmailControlAt(1);
    expect(component.form.value.to).toEqual(['']);
  });

  describe('should submit', () => {

    it('for new reports', () => {
      const validateSpy = spyOn(FormUtil, 'validate').and.returnValue(true);
      const putFormSpy = spyOn(component.cyclicReportingService, 'putCyclicReport').and.returnValue(of(report));
      const navigateSpy = spyOn(router, 'navigate').and.returnValue(Promise.resolve(true));

      report = {
        id: null,
        name: 'Name',
        fileNamePattern: '{Date}_{Time}_{Week}',
        subject: 'Test Subject',
        to: ['test@tld.org'],
        emailText: '',
        reportName: reportObjects[0].reportName,
        printFormat: 'pdf',
        standByListId: standByListData[0].id,
        statusId: 2,
        triggerWeekDay: 1,
        triggerHour: 8,
        triggerMinute: 0,
        validFromDayOffset: 0,
        validFromHour: 8,
        validFromMinute: 0,
        validToDayOffset: 1,
        validToHour: 8,
        validToMinute: 0
      };
      idParam = 'new';
      fixture.detectChanges();

      component.form.patchValue(report);
      component.submit();
      expect(validateSpy).toHaveBeenCalled();
      expect(putFormSpy).toHaveBeenCalledWith(report);
      expect(navigateSpy).toHaveBeenCalledWith(['..'], { relativeTo: activatedRoute });
    });

    it('for existing reports', () => {
      const validateSpy = spyOn(FormUtil, 'validate').and.returnValue(true);
      const postFormSpy = spyOn(component.cyclicReportingService, 'postCyclicReport').and.returnValue(of(report));
      const navigateSpy = spyOn(router, 'navigate').and.returnValue(Promise.resolve(true));

      report = {
        id: 19,
        name: 'Name',
        fileNamePattern: '{Date}_{Time}_{Week}',
        subject: 'Test Subject',
        to: ['test@tld.org'],
        emailText: '',
        reportName: reportObjects[0].reportName,
        printFormat: 'pdf',
        standByListId: standByListData[0].id,
        statusId: 2,
        triggerWeekDay: 1,
        triggerHour: 8,
        triggerMinute: 0,
        validFromDayOffset: 0,
        validFromHour: 8,
        validFromMinute: 0,
        validToDayOffset: 1,
        validToHour: 8,
        validToMinute: 0
      };
      idParam = 19;
      fixture.detectChanges();

      component.addEmailControl();
      component.submit();
      expect(validateSpy).toHaveBeenCalled();
      expect(postFormSpy).toHaveBeenCalledWith(report);
      expect(navigateSpy).toHaveBeenCalledWith(['..'], { relativeTo: activatedRoute });
    });

    it('and handle errors correctly', () => {
      const validateSpy = spyOn(FormUtil, 'validate').and.returnValue(false);
      const postFormSpy = spyOn(component.cyclicReportingService, 'postCyclicReport').and.returnValue(of(report));
      const navigateSpy = spyOn(router, 'navigate').and.returnValue(Promise.resolve(true));

      report = {
        id: 19,
        name: 'Name',
        fileNamePattern: '{Date}_{Time}_{Week}',
        subject: 'Test Subject',
        to: ['test@tld.org'],
        emailText: '',
        reportName: reportObjects[0].reportName,
        printFormat: 'pdf',
        standByListId: standByListData[0].id,
        statusId: 2,
        triggerWeekDay: 1,
        triggerHour: 8,
        triggerMinute: 0,
        validFromDayOffset: 0,
        validFromHour: 8,
        validFromMinute: 0,
        validToDayOffset: 1,
        validToHour: 8,
        validToMinute: 0
      };
      idParam = 19;
      fixture.detectChanges();

      component.submit();
      expect(validateSpy).toHaveBeenCalled();
      expect(postFormSpy).not.toHaveBeenCalled();

      validateSpy.and.returnValue(true);
      postFormSpy.and.returnValue(throwError('TestError'));
      component.submit();
      expect(validateSpy).toHaveBeenCalled();
      expect(postFormSpy).toHaveBeenCalledWith(report);
      expect(navigateSpy).not.toHaveBeenCalled();
      expect(component.form.enabled).toBe(true);
    });
  });

  describe('should delete', () => {
    it('existing reports', () => {
      const deleteSpy = spyOn(component.cyclicReportingService, 'deleteCyclicReport')
        .and.returnValue(of(null));
      const navigateSpy = spyOn(router, 'navigate').and.returnValue(Promise.resolve(true));

      idParam = 19;
      report = createMockData<CyclicReportObject>({ id: 19 });
      fixture.detectChanges();

      component.delete();
      expect(deleteSpy).toHaveBeenCalledWith(19);
      expect(navigateSpy).toHaveBeenCalledWith(['..'], { relativeTo: activatedRoute });
    });

    it('and handle errors correctly', () => {
      const deleteSpy = spyOn(component.cyclicReportingService, 'deleteCyclicReport')
        .and.returnValue(throwError('TestError'));
      const navigateSpy = spyOn(router, 'navigate');

      idParam = 19;
      report = createMockData<CyclicReportObject>({ id: 19 });
      fixture.detectChanges();

      component.delete();
      expect(deleteSpy).toHaveBeenCalledWith(19);
      expect(navigateSpy).not.toHaveBeenCalled();
      expect(component.form.disabled).toBe(false);
    });
  });

});
