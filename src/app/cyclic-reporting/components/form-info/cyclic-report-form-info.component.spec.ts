/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from '@angular/common';
import {SimpleChange} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {CyclicReportObject} from '@shared/model/CyclicReportObject';
import {Subject} from 'rxjs';
import {CyclicReportFormInfoComponent} from './cyclic-report-form-info.component';

describe('CyclicReportFormInfoComponent', () => {

  const refreshInterval = new Subject<number>();

  let component: CyclicReportFormInfoComponent;
  let fixture: ComponentFixture<CyclicReportFormInfoComponent>;

  let data: CyclicReportObject;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CyclicReportFormInfoComponent
      ],
      imports: [
        CommonModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CyclicReportFormInfoComponent);
    component = fixture.componentInstance;
    component.refreshInterval = refreshInterval;
    component.dateReplacementTokens = component.cyclicReportingUtilService.dateReplacementTokens;
    data = {
      id: 19,
      name: 'Cyclic Report ' + 19,
      fileNamePattern: '{Date}_{Time}_{Week}',
      subject: '{Date}_{Time}_{Week}',
      to: [],
      emailText: '',

      reportName: 'reportName',
      printFormat: 'pdf',
      standByListId: 19,
      statusId: 2,

      triggerWeekDay: 1,
      triggerHour: 7,
      triggerMinute: 30,
      validFromDayOffset: 0,
      validFromHour: 7,
      validFromMinute: 30,
      validToDayOffset: 3,
      validToHour: 18,
      validToMinute: 0
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should refresh on changes', () => {
    const refreshSpy = spyOn(component, 'refresh').and.callThrough();
    refreshSpy.calls.reset();
    component.data = data;
    component.ngOnChanges({ xyz: new SimpleChange(0, 1, false)});
    expect(refreshSpy).not.toHaveBeenCalled();
    component.ngOnChanges({ data: new SimpleChange(undefined, data, false)});
    expect(refreshSpy).toHaveBeenCalled();
  });

  it('should refresh automatically after a specific time', () => {
    const refreshSpy = spyOn(component, 'refresh').and.callThrough();
    refreshSpy.calls.reset();
    expect(refreshSpy).not.toHaveBeenCalled();
    refreshInterval.next(0);
    expect(refreshSpy).toHaveBeenCalled();
  });

  it('should ignore errors when refreshing', () => {
    component.data = data;
    spyOn(component.cyclicReportingUtilService, 'getNextTriggerDate').and.throwError('');
    spyOn(console, 'error');
    expect(() => component.refresh(new Date())).not.toThrow();
    expect(component.cyclicReportingUtilService.getNextTriggerDate).toHaveBeenCalled();
  });

  it('should replace tokens', () => {
    const date = new Date(2020, 11, 24, 19, 0);
    expect(component.replaceTokens('{Date}', date)).toBe('20201224');
    expect(component.replaceTokens(null, date)).toBe('');
    expect(component.replaceTokens('{Date}', null)).toBe('');
  });

});
