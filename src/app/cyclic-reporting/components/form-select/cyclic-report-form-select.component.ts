/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {SelectOptionObject} from '@shared/model/SelectOptionObject';

@Component({
  selector: 'ok-cyclic-report-form-select',
  styleUrls: ['cyclic-report-form-select.component.scss'],
  templateUrl: 'cyclic-report-form-select.component.html'
})
export class CyclicReportFormSelectComponent {

  private static id = 0;

  @Input()
  public controlId = `CyclicReportFormSelectComponent-${CyclicReportFormSelectComponent.id++}`;

  @Input()
  public form: FormGroup;

  @Input()
  public options: SelectOptionObject[] = [];

  @Input()
  public filter: boolean;

  @Input()
  public key: string | number;

}
