/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from '@angular/common';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormControl, FormGroup} from '@angular/forms';
import {SharedModule} from '@shared/shared.module';
import {CyclicReportFormInputComponent} from './cyclic-report-form-input.component';

describe('CyclicReportFormInputComponent', () => {

  let component: CyclicReportFormInputComponent;
  let fixture: ComponentFixture<CyclicReportFormInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CyclicReportFormInputComponent
      ],
      imports: [
        CommonModule,
        SharedModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CyclicReportFormInputComponent);
    component = fixture.componentInstance;
    const key = 'test';
    component.form = new FormGroup({ [key]: new FormControl()});
    component.key = key;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

});
