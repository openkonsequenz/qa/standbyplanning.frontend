/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { DatepickerValidator } from '@shared/validators/datepicker.validator';
import { FormGroup, FormControl } from '@angular/forms';
import { FormUtil } from '@shared/utils/form.util';

describe('DatepickerValidator', () => {
    it('should be null if the formcontrols are empty', () => {
        const formGroup: FormGroup = new FormGroup({
            title: new FormControl(''),
            district: new FormControl('test'),
            date: new FormGroup({
                validFrom: new FormControl(''),
                validTo: new FormControl('')
            }, [DatepickerValidator.dateRangeTo('validFrom')])
        });
        FormUtil.validate(formGroup);
        expect(formGroup.get('date').errors).toBe(null);
    });

    it('should be null if the formcontrols are not empty and from is before to date', () => {
        const formGroup: FormGroup = new FormGroup({
            title: new FormControl(''),
            district: new FormControl('test'),
            date: new FormGroup({
                validFrom: new FormControl({ day: 10, month: 10, year: 2018 }),
                validTo: new FormControl('')
            }, [DatepickerValidator.dateRangeTo('validFrom')])
        });
        formGroup.patchValue({ date: { validTo: { day: 11, month: 10, year: 2018 } } });
        expect(formGroup.get('date').errors).toBe(null);
    });

    it('should be null if the formcontrols are not empty and from is equal to to date', () => {
        const formGroup: FormGroup = new FormGroup({
            title: new FormControl(''),
            district: new FormControl('test'),
            date: new FormGroup({
                validFrom: new FormControl({ day: 10, month: 10, year: 2018 }),
                validTo: new FormControl('')
            }, [DatepickerValidator.dateRangeTo('validFrom')])
        });
        formGroup.patchValue({ date: { validTo: { day: 10, month: 10, year: 2018 } } });
        expect(formGroup.get('date').errors).toBe(null);
    });

    it('should be truthy if the formcontrols are not empty and to is before from date', () => {
        const formGroup: FormGroup = new FormGroup({
            title: new FormControl(''),
            district: new FormControl('test'),
            date: new FormGroup({
                validFrom: new FormControl({ day: 2, month: 10, year: 2018 }),
                validTo: new FormControl('')
            }, [DatepickerValidator.dateRangeTo('validFrom')])
        });
        formGroup.patchValue({ date: { validTo: { day: 1, month: 10, year: 2018 } } });
        expect(formGroup.get('date').errors).toBeTruthy();
    });

    it('should be truthy if the user enters another date in from field', () => {
        const formGroup: FormGroup = new FormGroup({
            title: new FormControl(''),
            district: new FormControl('test'),
            date: new FormGroup({
                validFrom: new FormControl({ day: 2, month: 10, year: 2018 }),
                validTo: new FormControl('')
            }, [DatepickerValidator.dateRangeTo('')])
        });
        formGroup.patchValue({ date: { validTo: { day: 10, month: 10, year: 2018 } } });
        formGroup.patchValue({ date: { validFrom: { day: 20, month: 10, year: 2018 } } });
        expect(formGroup.get('date').errors).toBeTruthy();
    });
});
