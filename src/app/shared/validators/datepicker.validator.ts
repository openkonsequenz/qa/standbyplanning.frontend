/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ValidatorFn, AbstractControl, FormGroup } from '@angular/forms';

export class DatepickerValidator {
    static dateRangeTo(test: string): ValidatorFn {
        return (form: FormGroup): { [key: string]: boolean } | null => {
            const validFrom: AbstractControl = form.get('validFrom');
            const validTo: AbstractControl = form.get('validTo');
            if (validFrom && validFrom.value && validTo.value) {
                const validFromValue = validFrom.value;
                const validFromDateObj = new Date(validFromValue.year, validFromValue.month - 1, validFromValue.day, 0, 0, 0, 0);
                const validToValue = validTo.value;
                const validToDateObj = new Date(validToValue.year, validToValue.month - 1, validToValue.day, 0, 0, 0, 0);
                if (validFromDateObj > validToDateObj) {
                    return { invalidDate: true };
                }
            }
            return null;
        };
    }
}
