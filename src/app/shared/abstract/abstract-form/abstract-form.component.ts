/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injector, Component } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgbModalRef, NgbModal, NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Subject, Observable } from 'rxjs';
import { AlertComponent } from '@shared/components/alert/alert.component';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'ok-abstract-form',
  templateUrl: './abstract-form.component.html',
  styles: ['']
})
export class AbstractFormComponent {
  protected modalService: NgbModal;
  protected fb: FormBuilder;
  protected route: ActivatedRoute;
  protected router: Router;
  protected ngbDateParserFormatter: NgbDateParserFormatter;
  protected messageService: MessageService;

  form: FormGroup;

  decisionModalRef: NgbModalRef;
  decision = new Subject<boolean>();

  constructor(injector: Injector) {
    this.modalService = injector.get(NgbModal);
    this.fb = injector.get(FormBuilder);
    this.route = injector.get(ActivatedRoute);
    this.router = injector.get(Router);
    this.ngbDateParserFormatter = injector.get(NgbDateParserFormatter);
    this.messageService = injector.get(MessageService);
  }

  /**
   * Implementation of canDeactivate. This is invoked through a routing guard and is the special implementation only for this
   * component. The routing guard defined in core/guards/can-deactivate.guard.ts invokes this.
   * If the form is dirty (so any changes have been made) a modal dialog is raised to ask if the user really wants to leave
   * without saving. Then we call a method to subscribe to the modals decision. We return a subject of the decision. The
   * can-deactivate guard knows how to handle the subject.
   */
  canDeactivate(): Observable<boolean> | boolean {
    if (this.form.dirty) {
      this.decisionModalRef = this.modalService.open(AlertComponent);
      this.listenForDecision();
      return this.decision;
    }
    return true;
  }
  /**
   * This method subscribes to the buttonclick in the modal in order to pass the chosen value to the subject and close the
   * modal.
   */
  listenForDecision() {
    this.decisionModalRef.componentInstance.decision.subscribe(res => {
      this.decisionModalRef.close();
      this.decision.next(res);
    });
  }
}
