/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormUtil } from '@shared/utils/form.util';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'ok-abstract-dialog',
  templateUrl: './abstract-dialog.component.html',
  styleUrls: ['./abstract-dialog.component.scss']
})
export class AbstractDialogComponent implements OnInit {
  @Output()
  decision: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  saveButtonLabel = 'Verschieben';
  form: FormGroup;
  constructor() {
  }

  ngOnInit() { }

  decide(decision, dataToTransmit) {
    if (decision) {
      if (FormUtil.validate(this.form)) {
        if (dataToTransmit) {
          this.decision.emit(dataToTransmit);
        } else {
          this.decision.emit(true);
        }
      }
    } else {
      this.decision.emit();
    }
  }

  setDefaultDate(field: string) {
    FormUtil.setDefaultDate(this.form, field);
  }
}
