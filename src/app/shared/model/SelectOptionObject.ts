/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

export interface SelectOptionObject<T = any> {
  label: string;
  value: T;
}
