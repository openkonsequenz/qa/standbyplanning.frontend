/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { RegionObject } from '@shared/model/RegionObject';
import { FunctionObject } from '@shared/model/FunctionObject';

export class UserObject {
    id: number;
    firstname: string;
    lastname: string;
    isCompany: boolean;
    userRole: string[];
    userRegion: string[];
    businessContactData: {
        phone: string;
        cellphone: string;
        radiocomm: string;
        email: string;
        pager: string;
    };
    privateContactData: { phone: string };
    privateAddress: {
        postcode: string;
        community: string;
        communitySuffix: string;
        street: string;
        housenumber: string;
        wgs84zone: string;
        latitude: string;
        longitude: string;
        isPrivate: boolean;
    };
    organisation: {
        id: number;
        orgaName: string;
    };
    lsUserInRegions: Array<RegionObject>;
    lsUserFunctions: Array<FunctionObject>;
    date?: {
        validFrom: any;
        validTo: any;
    };
    validFrom: any; // has to be any since the datepicker uses a different format than the rest service
    validTo: any; // has to be any since the datepicker uses a different format than the rest service
    modificationDate: string;
    notes: string;
}
