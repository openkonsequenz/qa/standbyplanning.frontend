/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { SearchBodiesObject } from '@shared/model/SearchBodiesObject';
import { GroupBodyObject } from '@shared/model/GroupBodyObject';

export class PlanBodyObject {
    filter: SearchBodiesObject;
    listPlanRows: Array<{
        label: string,
        listGroupBodies: Array<
        Array<GroupBodyObject>
        >
    }>;
    planHeader: {
        label: string,
        listGroups: Array<{
            id: number,
            modificationDate: any,
            title: string
        }>
    };
}
