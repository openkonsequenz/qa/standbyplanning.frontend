/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { TransferGroupObject } from '@shared/model/TransferGroupObject';

export class CopyBodiesObject {
    lsTransferGroup: TransferGroupObject[];
    overwrite: boolean;
    statusId: number;
    validFrom: any;
    validTo: any;
}
