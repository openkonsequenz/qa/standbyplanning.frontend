/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { StandbygroupObject } from '@shared/model/StandbygroupObject';
import { UserObject } from '@shared/model/UserObject';

export class QueryBodiesObject {
    id: number;
    standbyGroup: StandbygroupObject;
    user: UserObject;
    status: {
        id: number,
        title: string
    };
    validFrom: any;
    validTo: any;
}
