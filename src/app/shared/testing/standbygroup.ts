/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { StandbyDurationObject } from '@shared/model/StandbyDurationObject';
import { DateObject } from '@shared/model/DateObject';
import { ProtocolObject } from '@shared/model/ProtocolObject';

export class StandbygroupMockObjects {

    STANDBYGROUP_OBJECT = {
        id: 1,
        title: '',
        lsRegions: [],
        lsUserInStandbyGroups: [],
        lsBranches: [],
        lsStandbyDurations: [],
        lsUserFunction: [],
        lsIgnoredCalendarDays: [],
        userFunctionId: 1
    };

    FUNCTION_SELECTION_ARRAY = [{
        functionId: 1,
        functionName: 'LKW Fahrer'
    }, {
        functionId: 2,
        functionName: 'Elektriker'
    }];

    FUNCTION_SELECTION_ARRAY_SINGLE = [{
        functionId: 1,
        functionName: 'LKW Fahrer'
    }];

    REGION_SELECTION_ARRAY = [{
        id: 1,
        regionName: 'Nord'
    }, {
        id: 2,
        regionName: 'Süd'
    }];

    USER_SELECTION_ARRAY = [{
        id: 1,
        firstname: 'Max',
        lastname: 'Muster'
    }, {
        id: 2,
        firstname: 'Ida',
        lastname: 'fröhlich'
    }];

    USERDATA_WITH_PROTOCOL_OBJECT: ProtocolObject = {
        data: [{
            id: 1,
            firstname: 'Max',
            lastname: 'Muster'
        }, {
            id: 2,
            firstname: 'Ida',
            lastname: 'fröhlich'
        }],
        lsMsg: []
    };

    BRANCH_ARRAY = [
        {
            id: 1,
            title: 'Strom'
        },
        {
            id: 2,
            title: 'Gas'
        },
        {
            id: 3,
            title: 'Wasser'
        }
    ];

    DURATION_OBJECT: StandbyDurationObject = {
        validTo: {
            hour: 10,
            minute: 10,
            second: 10
        },
        validFrom: {
            hour: 10,
            minute: 10,
            second: 10
        },
        validDayFrom: 3,
        validDayTo: 6,
        standbyGroupId: 1,
        standbyDurationId: 1,
        modificationDate: '2018-12-12'
    };

    CALENDAR_ARRAY: DateObject[] = [
        {
            id: 1,
            name: 'Ostern',
            dateIndex: '2018-04-20'
        },
        {
            id: 2,
            name: 'Neujahr',
            dateIndex: '2019-01-01'
        }
    ];

    PROTOCOL_OBJECT: ProtocolObject = {
        lsMsg: [
            {
                'msg': 'Die Validierung wird jetzt für den Zeitraum vom',
                'type': 'INFO',
                'cssType': 'info'
            }
        ],
        data: [
            {
                'standbyDurationId': 35,
                'validFrom': { 'hour': 23, 'minute': 59, 'second': 0 },
                'validDayFrom': 1, 'validTo': { 'hour': 23, 'minute': 59, 'second': 0 },
                'validDayTo': 2,
                'modificationDate': '2018-11-29T06:39:34.736',
                'standbyGroupId': 8
            },
            {
                'standbyDurationId': 38,
                'validFrom': { 'hour': 23, 'minute': 58, 'second': 0 },
                'validDayFrom': 1,
                'validTo': { 'hour': 23, 'minute': 59, 'second': 0 },
                'validDayTo': 3,
                'modificationDate': '2018-11-29T06:57:57.277',
                'standbyGroupId': 8
            }
        ]
    };

    STANDBYGROUP_ARRAY = [{
        title: 'test',
        district: 'test',
        shorttext: 'test',
        community: 'test'
    },
    {
        title: 'test',
        district: 'test',
        shorttext: 'test',
        community: 'test'
    }];
}
