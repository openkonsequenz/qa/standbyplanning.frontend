/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { UserDropdownObject } from '@shared/model/UserDropdownObject';

export class ReportingMockObjects {
    QUERY_BODIES_OBJECT = [
        {
            'id': 468,
            'validFrom': '2018-10-16T16:00:00.000Z',
            'validTo': '2018-10-16T23:59:59.000Z',
            'user': {
                'id': 1,
                'firstname': 'Max',
                'lastname': 'Mustermann',
                'isCompany': false,
                'notes': 'Test-Notiz',
                'validFrom': '2018-01-01T02:00:00.000Z',
                'validTo': '2022-01-01T02:00:00.000Z',
                'modificationDate': '2018-08-02T16:57:07.064Z',
                'organisation': {
                    'id': 1,
                    'orgaName': 'TestOrga',
                    'address': {
                        'id': 1,
                        'postcode': '33100',
                        'community': 'community-sued',
                        'communitySuffix': '_sued',
                        'street': 'Teststraße 1',
                        'housenumber': '1',
                        'wgs84zone': 'EPSG-xy',
                        'latitude': '1.0000',
                        'longitude': '2.0000',
                        'urlMap': 'www.url.de'
                    }
                },
                'privateAddress': {
                    'id': 3,
                    'postcode': '33100',
                    'community': 'community-sued',
                    'communitySuffix': '_sued',
                    'street': 'Private Str. 1a',
                    'housenumber': '1',
                    'wgs84zone': 'EPSG-xy',
                    'latitude': '1.0000',
                    'longitude': '2.0000',
                    'urlMap': 'www.url.de'
                },
                'businessContactData': {
                    'id': 1,
                    'isPrivate': true,
                    'phone': '0123 456789',
                    'cellphone': '0123 456789',
                    'radiocomm': '01234656789',
                    'email': 'test@test.de',
                    'pager': '0123 456789'
                },
                'privateContactData': {
                    'id': 3,
                    'isPrivate': true,
                    'phone': '0123 456789',
                    'cellphone': '0123 456789',
                    'radiocomm': '01234656789',
                    'email': 'test@test.de',
                    'pager': '0123 456789'
                },
                'lsUserInRegions': [
                    {
                        'id': 1,
                        'regionId': 1,
                        'regionName': 'Darmstadt',
                        'userId': 1
                    },
                    {
                        'id': 4,
                        'regionId': 2,
                        'regionName': 'Erbach',
                        'userId': 1
                    },
                    {
                        'id': 5,
                        'regionId': 4,
                        'regionName': 'Heppenheim',
                        'userId': 1
                    },
                    {
                        'id': 6,
                        'regionId': 3,
                        'regionName': 'Groß-umstadt',
                        'userId': 1
                    }
                ],
                'lsUserFunctions': [
                    {
                        'id': 1,
                        'functionId': 1,
                        'functionName': 'Meister Betrieb Strom'
                    },
                    {
                        'id': 2,
                        'functionId': 2,
                        'functionName': 'Fachkraft Betrieb Strom'
                    },
                    {
                        'id': 3,
                        'functionId': 3,
                        'functionName': 'Ingenieure Strom/Gas/Wasser'
                    },
                    {
                        'id': 4,
                        'functionId': 4,
                        'functionName': 'Meister Betrieb Gas'
                    },
                    {
                        'id': 5,
                        'functionId': 5,
                        'functionName': 'Meister Betrieb Wasser'
                    },
                    {
                        'id': 6,
                        'functionId': 6,
                        'functionName': 'Fachkraft Betrieb Gas'
                    },
                    {
                        'id': 7,
                        'functionId': 7,
                        'functionName': 'Mitarbeiter ohne Bereitschaft'
                    },
                    {
                        'id': 8,
                        'functionId': 8,
                        'functionName': 'STÖRUNGSVERANTWORTLICHE'
                    },
                    {
                        'id': 9,
                        'functionId': 9,
                        'functionName': 'Fuhrpark'
                    }
                ]
            },
            'status': {
                'id': 2,
                'title': 'Ist-Plan'
            },
            'standbyGroup': {
                'id': 1,
                'title': 'group1',
                'note': 'Test-Notiz',
                'modificationDate': '2018-08-06T02:00:00.000Z',
                'nextUserInNextCycle': false,
                'extendStandbyTime': true,
                'lsUserInStandbyGroups': [
                    {
                        'id': 1,
                        'standbyGroupId': 1,
                        'userId': 1,
                        'firstname': 'Max',
                        'lastname': 'Mustermann',
                        'validFrom': '2018-01-01T00:00:00.000Z',
                        'validTo': '2018-12-31T02:00:00.000Z',
                        'userRegionStr': 'Darmstadt,Erbach,Heppenheim,Groß-umstadt',
                        'userHasUserFunctionStr': 'Meister Betrieb Strom',
                        'organisationName': 'TestOrga',
                        'position': 1
                    },
                    {
                        'id': 2,
                        'standbyGroupId': 1,
                        'userId': 2,
                        'firstname': 'Erika',
                        'lastname': 'Musterfrau',
                        'validFrom': '2018-01-01T00:00:00.000Z',
                        'validTo': '2018-12-31T02:00:00.000Z',
                        'userRegionStr': 'Darmstadt,Erbach',
                        'userHasUserFunctionStr': '',
                        'organisationName': 'Mustermann GmbH',
                        'position': 2
                    },
                    {
                        'id': 3,
                        'standbyGroupId': 1,
                        'userId': 3,
                        'firstname': 'N',
                        'lastname': 'N 1',
                        'validFrom': '2018-01-01T00:00:00.000Z',
                        'validTo': '2018-12-31T02:00:00.000Z',
                        'userRegionStr': '',
                        'userHasUserFunctionStr': '',
                        'organisationName': 'TestOrga',
                        'position': 3
                    },
                    {
                        'id': 4,
                        'standbyGroupId': 1,
                        'userId': 4,
                        'firstname': 'N',
                        'lastname': 'N 2',
                        'validFrom': '2018-01-01T00:00:00.000Z',
                        'validTo': '2018-12-31T02:00:00.000Z',
                        'userRegionStr': '',
                        'userHasUserFunctionStr': '',
                        'organisationName': 'TestOrga',
                        'position': 4
                    }
                ],
                'lsStandbyDurations': [
                    {
                        'standbyDurationId': 1,
                        'validFrom': {
                            'hour': 16,
                            'minute': 0,
                            'second': 0
                        },
                        'validDayFrom': 1,
                        'validTo': {
                            'hour': 8,
                            'minute': 0,
                            'second': 0
                        },
                        'validDayTo': 2,
                        'modificationDate': '2018-09-01T02:00:00.000Z',
                        'standbyGroupId': 1,
                        'nextUserInNextDuration': true
                    },
                    {
                        'standbyDurationId': 2,
                        'validFrom': {
                            'hour': 16,
                            'minute': 0,
                            'second': 0
                        },
                        'validDayFrom': 2,
                        'validTo': {
                            'hour': 8,
                            'minute': 0,
                            'second': 0
                        },
                        'validDayTo': 3,
                        'modificationDate': '2018-09-01T02:00:00.000Z',
                        'standbyGroupId': 1,
                        'nextUserInNextDuration': true
                    },
                    {
                        'standbyDurationId': 3,
                        'validFrom': {
                            'hour': 16,
                            'minute': 0,
                            'second': 0
                        },
                        'validDayFrom': 3,
                        'validTo': {
                            'hour': 8,
                            'minute': 0,
                            'second': 0
                        },
                        'validDayTo': 4,
                        'modificationDate': '2018-09-01T02:00:00.000Z',
                        'standbyGroupId': 1,
                        'nextUserInNextDuration': true
                    },
                    {
                        'standbyDurationId': 4,
                        'validFrom': {
                            'hour': 16,
                            'minute': 0,
                            'second': 0
                        },
                        'validDayFrom': 4,
                        'validTo': {
                            'hour': 8,
                            'minute': 0,
                            'second': 0
                        },
                        'validDayTo': 5,
                        'modificationDate': '2018-09-01T02:00:00.000Z',
                        'standbyGroupId': 1,
                        'nextUserInNextDuration': true
                    },
                    {
                        'standbyDurationId': 5,
                        'validFrom': {
                            'hour': 8,
                            'minute': 0,
                            'second': 0
                        },
                        'validDayFrom': 5,
                        'validTo': {
                            'hour': 8,
                            'minute': 0,
                            'second': 0
                        },
                        'validDayTo': 6,
                        'modificationDate': '2018-09-01T02:00:00.000Z',
                        'standbyGroupId': 1,
                        'nextUserInNextDuration': true
                    },
                    {
                        'standbyDurationId': 6,
                        'validFrom': {
                            'hour': 8,
                            'minute': 0,
                            'second': 0
                        },
                        'validDayFrom': 6,
                        'validTo': {
                            'hour': 8,
                            'minute': 0,
                            'second': 0
                        },
                        'validDayTo': 7,
                        'modificationDate': '2018-09-01T02:00:00.000Z',
                        'standbyGroupId': 1,
                        'nextUserInNextDuration': true
                    },
                    {
                        'standbyDurationId': 19,
                        'validFrom': {
                            'hour': 8,
                            'minute': 0,
                            'second': 0
                        },
                        'validDayFrom': 7,
                        'validTo': {
                            'hour': 8,
                            'minute': 0,
                            'second': 0
                        },
                        'validDayTo': 1,
                        'modificationDate': '2018-09-01T02:00:00.000Z',
                        'standbyGroupId': 1,
                        'nextUserInNextDuration': true
                    }
                ],
                'lsBranches': [

                ],
                'lsUserFunction': [
                    {
                        'functionId': 1,
                        'functionName': 'Meister Betrieb Strom'
                    }
                ],
                'lsRegions': [

                ],
                'lsIgnoredCalendarDays': [

                ]
            }
        }
    ];

    LOCATION_OBJECT_WITHOUT_BRANCHES = {
        id: 1,
        title: 'test',
        district: '',
        shorttext: 'test',
        community: 'test',
        lsPostcode: [{ id: '1', pcode: '33100' }],
        lsLocationForBranches: [],
        lsRegions: []
    };

    LOCATION_OBJECT_WITH_BRANCHES = {
        id: 1,
        title: 'test',
        district: '',
        shorttext: 'test',
        community: 'test',
        lsPostcode: [{ id: '1', pcode: '33100' }],
        lsLocationForBranches: [{ id: 1, branchId: 1, title: 'Gas' }],
        lsRegions: []
    };

    STANDBYLIST_SELECTION_ARRAY = [{
        title: 'test'
    },
    {
        title: 'test2'
    }];

    USER_DROPDOWN_DATA_LIST: UserDropdownObject[] = [
        {
            label: 'Erna Meier',
            value: 10
        }
    ];
}
