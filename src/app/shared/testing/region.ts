/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { RegionObject } from '@shared/model/RegionObject';
import { LocationObject } from '@shared/model/LocationObject';

export class RegionMockObjects {
    REGION_OBJECT: RegionObject = {
        id: 1,
        regionName: '',
        lsLocations: [{
            id: 2,
            title: 'abc',
            district: '',
            shorttext: 'def',
            community: 'ghi',
            lsPostcode: [{ id: 1, pcode: '33102' }],
            lsRegions: [],
            lsLocationForBranches: []
        }]
    };

    REGION_ARRAY: RegionObject[] = [{
        id: 1,
        regionName: '',
        lsLocations: [{
            id: 2,
            title: 'abc',
            district: '',
            shorttext: 'def',
            community: 'ghi',
            lsPostcode: [{ id: 1, pcode: '33102' }],
            lsRegions: [],
            lsLocationForBranches: []
        }]
    }, {
        id: 2,
        regionName: '',
        lsLocations: [{
            id: 3,
            title: 'adfd',
            district: '',
            shorttext: 'dfsafd',
            community: 'dfasdf',
            lsPostcode: [{ id: 1, pcode: '33102' }],
            lsRegions: [],
            lsLocationForBranches: []
        }]
    }];

    LOCATION_ARRAY: LocationObject[] = [{
        id: 1,
        title: 'test',
        district: '',
        shorttext: 'test',
        community: 'test',
        lsPostcode: [{ id: 1, pcode: '33100' }],
        lsRegions: [],
        lsLocationForBranches: []
    }, {
        id: 2,
        title: 'abc',
        district: '',
        shorttext: 'def',
        community: 'ghi',
        lsPostcode: [{ id: 1, pcode: '33102' }],
        lsRegions: [],
        lsLocationForBranches: []
    }];

    LOCATION_SELECTION_ARRAY = [{
        id: 1,
        title: 'test',
        district: '',
        shorttext: 'test',
        community: 'test'
    }, {
        id: 2,
        title: 'abc',
        district: '',
        shorttext: 'def',
        community: 'ghi'
    }];

    FUNCTION_SELECTION_ARRAY = [{
        id: 1,
        functionName: 'LKW Fahrer'
    }, {
        id: 2,
        functionName: 'Elektriker'
    }];

    REGION_ARRAY_2 = [{
        regionName: 'test'
    },
    {
        regionName: 'test'
    }];
}
