/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { UserDropdownObject } from '@shared/model/UserDropdownObject';

export class ReplaceMockObjects {
    USER_UNIQUE_LIST = [
        {
            'id': 1,
            'firstname': 'Max',
            'lastname': 'Mustermann',
            'isCompany': false,
            'notes': 'Test-Notiz',
            'validFrom': '2018-01-01T02:00:00.000Z',
            'validTo': '2022-01-01T02:00:00.000Z',
            'modificationDate': '2018-08-02T16:57:07.064Z',
            'organisation': {
                'id': 1,
                'orgaName': 'TestOrga',
                'address': {
                    'id': 1,
                    'postcode': '33100',
                    'community': 'community-sued',
                    'communitySuffix': '_sued',
                    'street': 'Teststraße 1',
                    'housenumber': '1',
                    'wgs84zone': 'EPSG-xy',
                    'latitude': '1.0000',
                    'longitude': '2.0000',
                    'urlMap': 'www.url.de'
                }
            },
            'privateAddress': {
                'id': 3,
                'postcode': '33100',
                'community': 'community-sued',
                'communitySuffix': '_sued',
                'street': 'Private Str. 1a',
                'housenumber': '1',
                'wgs84zone': 'EPSG-xy',
                'latitude': '1.0000',
                'longitude': '2.0000',
                'urlMap': 'www.url.de'
            },
            'businessContactData': {
                'id': 1,
                'isPrivate': true,
                'phone': '0123 456789',
                'cellphone': '0123 456789',
                'radiocomm': '01234656789',
                'email': 'test@test.de',
                'pager': '0123 456789'
            },
            'privateContactData': {
                'id': 3,
                'isPrivate': true,
                'phone': '0123 456789',
                'cellphone': '0123 456789',
                'radiocomm': '01234656789',
                'email': 'test@test.de',
                'pager': '0123 456789'
            },
            'lsUserInRegions': [
                {
                    'id': 1,
                    'regionId': 1,
                    'regionName': 'Darmstadt',
                    'userId': 1
                }
            ],
            'lsUserFunctions': [

            ]
        }];

    USER_DATA_SELECTION_LIST = [
        {
            'id': 1,
            'firstname': 'Max',
            'lastname': 'Mustermann',
            'isCompany': false,
            'notes': 'Test-Notiz',
            'validFrom': '2018-01-01T02:00:00.000Z',
            'validTo': '2022-01-01T02:00:00.000Z',
            'modificationDate': '2018-08-02T16:57:07.064Z',
            'userRegionStr': 'Darmstadt',
            'userHasUserFunctionStr': '',
            'organisationName': 'TestOrga'
        }
    ];

    USER_DROPDOWN_DATA_LIST: UserDropdownObject[] = [
        {
            label: 'Erna Meier',
            value: 10
        }
    ];
}
