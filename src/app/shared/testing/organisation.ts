/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export class OrganisationMockObjects {
    ORGANISATION_OBJECT = {
        id: 1,
        orgaName: 'test'
    };

    ORGANISATION_ARRAY = [{
        id: 1,
        orgaName: 'test'
    },
    {
        id: 2,
        orgaName: 'test2'
    }];
}
