/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export class PicklistMockObjects {
    OBJECT_ARRAY_SOURCE = [
        { functionName: 'Netzmonteur' },
        { functionName: 'Geschäftsführer' },
        { functionName: 'Sekretariat' }
    ];

    OBJECT_ARRAY_TARGET = [
        { functionName: 'Einsatzleiter Wasser' },
        { functionName: 'Einsatzleiter Strom' },
        { functionName: 'Einsatzleiter Gas' }
    ];

    SELECTED_ROWS_OBJECT_SOURCE_ARRAY = [
        { functionName: 'Netzmonteur' },
    ];

    SELECTED_ROWS_OBJECT_TARGET_ARRAY = [
        { functionName: 'Einsatzleiter Gas' },
    ];
}
