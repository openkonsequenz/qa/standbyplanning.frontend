/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { TransferGroupObject } from '@shared/model/TransferGroupObject';

export class TransferplanMockObjects {
    BODY_GROUP_ARRAY: TransferGroupObject[] = [
        {
            'groupName': 'group1',
            'groupId': 1,
            'listName': 'Einsatzleiter Strom',
            'listId': 1
        },
        {
            'groupName': 'group2',
            'groupId': 2,
            'listName': 'Einsatzleiter Strom',
            'listId': 1
        }
    ];
}
