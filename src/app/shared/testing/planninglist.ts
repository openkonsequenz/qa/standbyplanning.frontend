/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ProtocolObject } from '@shared/model/ProtocolObject';

export class PlanninglistMockObjects {
    SCHEDULE_PLANNING_ARRAY = [{
        standbyGroupId: 1,
        standbyListId: 1,
        validFrom: { day: 1, month: 1, year: 2018 },
        validTo: { day: 1, month: 1, year: 2018 },
        startIdOfUser: 1
    },
    {
        standbyGroupId: 2,
        standbyListId: 2,
        validFrom: { day: 1, month: 1, year: 2018 },
        validTo: { day: 1, month: 1, year: 2018 },
        startIdOfUser: 2
    }];

    STANDBYLIST_SELECTION_ARRAY = [{
        title: 'test'
    },
    {
        title: 'test2'
    }];

    PROTOCOL_OBJECT: ProtocolObject = {
        lsMsg: []
    };
}
