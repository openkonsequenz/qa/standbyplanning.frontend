/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {StepperControlComponent} from '@shared/components/stepper-control/stepper-control.component';
import {CommonModule} from '@angular/common';
import {SimpleChange} from '@angular/core';

describe('StepperControlComponent', () => {

  let component: StepperControlComponent;
  let fixture: ComponentFixture<StepperControlComponent>;
  let inputElement: HTMLInputElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        StepperControlComponent
      ],
      imports: [
        CommonModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepperControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    inputElement = fixture.debugElement.nativeElement.querySelector('input');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should parse input', () => {
    component.parseInput('19');
    expect(component.value).toBe(19);
    component.parseInput('-19');
    expect(component.value).toBe(-19);
  });

  it('should increment value by step size', () => {
    component.increment(undefined);
    expect(component.value).toBe(1);
    component.increment(5);
    expect(component.value).toBe(6);
  });

  it('should decrement value by step size', () => {
    component.decrement(undefined);
    expect(component.value).toBe(-1);
    component.decrement(5);
    expect(component.value).toBe(-6);
  });

  it('should set value on input', () => {
    component.step = null;
    component.setNewValue(19);
    expect(component.value).toBe(19);

    component.step = 15;
    component.setNewValue(19);
    expect(component.value).toBe(15);

    component.step = 15;
    component.setNewValue(19);
    expect(component.value).toBe(15);

    component.step = 30;
    component.minValue = 0;
    component.maxValue = 30;

    component.setNewValue(0, false);
    expect(component.value).toBe(0);
    component.setNewValue(-30, false);
    expect(component.value).toBe(0);
    component.setNewValue(30, false);
    expect(component.value).toBe(30);
    component.setNewValue(60, false);
    expect(component.value).toBe(30);

    component.setNewValue(0, true);
    expect(component.value).toBe(0);
    component.setNewValue(60, true);
    expect(component.value).toBe(0);
    component.setNewValue(30, true);
    expect(component.value).toBe(30);
    component.setNewValue(-30, true);
    expect(component.value).toBe(30);
  });

  it('should reformat input', () => {
    component.value = null;
    component.reformatInput();
    expect(inputElement.value).toBe('');

    component.digits = 2;
    component.value = 7;
    component.reformatInput();
    expect(inputElement.value).toBe('07');

    component.value = -7;
    component.ngOnChanges({ value: new SimpleChange(7, -7, false)});
    expect(inputElement.value).toBe('-07');

    component.writeValue({});
    expect(inputElement.value).toBe('-07');

    component.writeValue(19);
    expect(inputElement.value).toBe('19');
  });

  it('should set disabled state', () => {
    component.isDisabled = false;
    component.setDisabledState(true);
    expect(component.isDisabled).toBe(true);
    component.setDisabledState(false);
    expect(component.isDisabled).toBe(false);
  });

  it('should provide for NG_VALUE_ACCESSOR', () => {
    expect(() => component.registerOnChange(() => null)).not.toThrow();
    expect(() => component.registerOnTouched(() => null)).not.toThrow();
  });

});
