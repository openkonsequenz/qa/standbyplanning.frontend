/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Mettenmeier GmbH - initial implementation
 *   Basys GmbH - automatic report generation implementation
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ErrorComponent} from '@shared/components/error/error.component';
import {FormControl, Validators} from '@angular/forms';

describe('ErrorComponent', () => {
  let component: ErrorComponent;
  let fixture: ComponentFixture<ErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ErrorComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('shouldShowError', () => {
    it('shouldn´t show errors if there are no errors', () => {
      (component as any).control = new FormControl('');
      expect(component.shouldShowErrors()).toBeFalsy();
    });

    it('should show errors if there are errors and is dirty', () => {
      (component as any).control = new FormControl('', Validators.required);
      (component as any).control.markAsDirty();
      expect(component.shouldShowErrors()).toBeTruthy();
    });

    it('should show errors if there are errors and is touched', () => {
      (component as any).control = new FormControl('', Validators.required);
      (component as any).control.markAsTouched();
      expect(component.shouldShowErrors()).toBeTruthy();
    });

    it('should return an Array of error Messages', () => {
      (component as any).control = new FormControl('', Validators.required);
      expect(Array.isArray(component.listOfErrors())).toBeTruthy();
    });

    it('should return an empty Array if there are no errorMessages', () => {
      (component as any).control = new FormControl('');
      expect(component.listOfErrors().length === 0).toBeTruthy();
    });

    it('should return an Array with empty String', () => {
      (component as any).control = new FormControl('');
      (component as any).control.setErrors({ test: 'test' });
      expect(component.listOfErrors().length === 1).toBeTruthy();
      expect(component.listOfErrors()[0] === '').toBeTruthy();
    });

    it('should return an Array with an error for minLength = 4', () => {
      (component as any).control = new FormControl('abc', Validators.minLength(4));
      expect(component.listOfErrors()[0]).toEqual('Es müssen mindestens 4 Zeichen eingegeben werden');
    });

    it('should return an Array with an error for maxLength = 4', () => {
      (component as any).control = new FormControl('abcde', Validators.maxLength(4));
      expect(component.listOfErrors()[0]).toEqual('Es dürfen maximal 4 Zeichen eingegeben werden');
    });

    it('should return an Array with an error for pattern ^abc$', () => {
      (component as any).control = new FormControl('abd', Validators.pattern('^abc$'));
      expect(component.listOfErrors()[0]).toEqual('Die Eingabe entspricht nicht dem Muster: ^abc$');
    });

    it('should return an Array with an error for email error', () => {
      (component as any).control = new FormControl('abc', Validators.email);
      expect(component.listOfErrors()[0]).toEqual('Die Eingabe enthält keine gültige Emailadresse.');
    });

  });
});
