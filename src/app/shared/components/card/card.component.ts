/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ok-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() cardBodyText: string;
  @Input() cardHeaderText: string;
  @Input() cardButtonText: string;
  @Input() cardButtonRoute: string;
  @Input() cardButtonId: string;
  @Input() disabled = false;

  constructor() { }

  ngOnInit() {
  }

}
