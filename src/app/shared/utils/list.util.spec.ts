/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { ListUtil } from '@shared/utils/list.util';

describe('listUtil', () => {
    describe('compare dates', () => {
        it('should compare dates and return positive number if dateA is smaller than dateB', () => {
            const dateA = new Date(2017, 11, 24, 10, 33, 30, 0);
            const dateB = new Date(2018, 11, 24, 10, 33, 30, 0);
            expect(ListUtil.compareDates(dateA, dateB)).toBe(1);
        });

        it('should compare dates and return negative number if dateA is greater than dateB', () => {
            const dateA = new Date(2018, 11, 24, 10, 33, 30, 0);
            const dateB = new Date(2017, 11, 24, 10, 33, 30, 0);
            expect(ListUtil.compareDates(dateA, dateB)).toBe(-1);
        });

        it('should compare dates and return 0 if dateA is equal to dateB', () => {
            const dateA = new Date(2018, 11, 24, 10, 33, 30, 0);
            const dateB = new Date(2018, 11, 24, 10, 33, 30, 0);
            expect(ListUtil.compareDates(dateA, dateB)).toEqual(0);
        });
    });

    it('should format a datestring to the format dd.mm.yyyy', () => {
        const params = {
            value: '1999-01-08T03:05:06.789Z'
        };
        expect(ListUtil.formatDate(params)).toEqual('08.01.1999');
    });

    it('should format a datestring to the format dd.mm.yyyy', () => {
        const params = {
            value: '2017-03-04T03:05:06.789Z'
        };
        expect(ListUtil.formatDate(params)).toEqual('04.03.2017');
    });

    it('should format a datestring to the format dd.mm.yyyy', () => {
        const params = {
            value: '2023-05-08T05:05:05.789Z'
        };
        expect(ListUtil.formatDate(params)).toEqual('08.05.2023');
    });

    it('should format a datestring to the format dd.mm.yyyy', () => {
        const params = {
            value: '2020-01-08T03:05:06.789Z'
        };
        expect(ListUtil.formatDate(params)).toEqual('08.01.2020');
    });

    it('should format a datestring to the format dd.mm.yyyy', () => {
        const params = {
            value: '2999-01-08T03:05:06.789Z'
        };
        expect(ListUtil.formatDate(params)).toEqual('08.01.2999');
    });

    it('should join an array to a string comma separated', () => {
        const array = [
            {
                functionName: 'LKW-Fahrer'
            }, {
                functionName: 'Netzmonteur'
            }, {
                functionName: 'Geschäftsführer'
            }
        ];
        expect(ListUtil.joinArrayElementsToString(array, 'functionName')).toEqual('LKW-Fahrer,Netzmonteur,Geschäftsführer');
    });

    it('should get the index 1 for the object with functionName "Netzmonteur"', () => {
        const arrayToBeSearched = [
            {
                functionName: 'LKW-Fahrer'
            }, {
                functionName: 'Netzmonteur'
            }, {
                functionName: 'Geschäftsführer'
            }
        ];
        const object = {
            functionName: 'Netzmonteur'
        };
        expect(ListUtil.getIndexOfElement(object, arrayToBeSearched)).toEqual(1);
    });

    it('should get the index -1 for the object with functionName "Lastwagenfahrer"', () => {
        const arrayToBeSearched = [
            {
                functionName: 'LKW-Fahrer'
            }, {
                functionName: 'Netzmonteur'
            }, {
                functionName: 'Geschäftsführer'
            }
        ];
        const object = {
            functionName: 'Lastwagenfahrer'
        };
        expect(ListUtil.getIndexOfElement(object, arrayToBeSearched)).toEqual(-1);
    });
});
