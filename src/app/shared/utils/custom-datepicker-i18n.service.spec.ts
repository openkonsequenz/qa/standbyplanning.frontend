/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { TestBed, inject } from '@angular/core/testing';

import { CustomDatepickerI18nService } from './custom-datepicker-i18n.service';

describe('CustomDatepickerI18nService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomDatepickerI18nService]
    });
  });

  it('should be created', inject([CustomDatepickerI18nService], (service: CustomDatepickerI18nService) => {
    expect(service).toBeTruthy();
  }));

  describe('getWeekdayShortName', () => {
    it('should return "Mo" for weekday 1', inject([CustomDatepickerI18nService], (service: CustomDatepickerI18nService) => {
      expect(service.getWeekdayShortName(1)).toBe('Mo');
    }));

    it('should return "Mi" for weekday 3', inject([CustomDatepickerI18nService], (service: CustomDatepickerI18nService) => {
      expect(service.getWeekdayShortName(3)).toBe('Mi');
    }));

    it('should return "So" for weekday 7', inject([CustomDatepickerI18nService], (service: CustomDatepickerI18nService) => {
      expect(service.getWeekdayShortName(1)).toBeTruthy('So');
    }));
  });

  describe('getMonthShortName', () => {
    it('should return "Jan" for month 1', inject([CustomDatepickerI18nService], (service: CustomDatepickerI18nService) => {
      expect(service.getMonthShortName(1)).toBe('Jan');
    }));

    it('should return "Jun" for weekday 6', inject([CustomDatepickerI18nService], (service: CustomDatepickerI18nService) => {
      expect(service.getMonthShortName(6)).toBe('Jun');
    }));

    it('should return "Dez" for weekday 12', inject([CustomDatepickerI18nService], (service: CustomDatepickerI18nService) => {
      expect(service.getMonthShortName(12)).toBeTruthy('Dez');
    }));
  });

  describe('getMonthFullName', () => {
    it('should return "Jan" for month 1', inject([CustomDatepickerI18nService], (service: CustomDatepickerI18nService) => {
      expect(service.getMonthFullName(1)).toBe('Jan');
    }));

    it('should return "Jun" for weekday 6', inject([CustomDatepickerI18nService], (service: CustomDatepickerI18nService) => {
      expect(service.getMonthFullName(6)).toBe('Jun');
    }));

    it('should return "Dez" for weekday 12', inject([CustomDatepickerI18nService], (service: CustomDatepickerI18nService) => {
      expect(service.getMonthFullName(12)).toBeTruthy('Dez');
    }));
  });

  describe('getDayAriaLabel', () => {
    it('should return "14.12.1991"', inject([CustomDatepickerI18nService], (service: CustomDatepickerI18nService) => {
      expect(service.getDayAriaLabel({ day: 14, month: 12, year: 1991 })).toBe('14.12.1991');
    }));

    it('should return "01.10.2018"', inject([CustomDatepickerI18nService], (service: CustomDatepickerI18nService) => {
      expect(service.getDayAriaLabel({ day: 1, month: 10, year: 2018 })).toBe('1.10.2018');
    }));

    it('should return "31.12.2018"', inject([CustomDatepickerI18nService], (service: CustomDatepickerI18nService) => {
      expect(service.getDayAriaLabel({ day: 31, month: 12, year: 2018 })).toBeTruthy('31.12.2018');
    }));
  });
});
