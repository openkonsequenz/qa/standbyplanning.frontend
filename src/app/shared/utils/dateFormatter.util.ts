/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { NgbDateStruct, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

// Formatter using "dd-MM-yyyy" string format:
export class NgbDateStringParserFormatter extends NgbDateParserFormatter {
    parse(value: string): NgbDateStruct {
        if (!value) { return null; }

        let parts = value.trim().split('-');
        let dateObj: NgbDateStruct;
        if (parts.length === 1) {
            parts = value.trim().split('.');
            dateObj = {
                day: parts.length > 0 ? parseInt(parts[0], 10) : null,
                month: parts.length > 1 ? parseInt(parts[1], 10) : null,
                year: parts.length > 2 ? parseInt(parts[2], 10) : null
            };
        } else {
            dateObj = {
                year: parts.length > 0 ? parseInt(parts[0], 10) : null,
                month: parts.length > 1 ? parseInt(parts[1], 10) : null,
                day: parts.length > 2 ? parseInt(parts[2], 10) : null,
            };
        }
        return dateObj;
    }

    format(date: NgbDateStruct): string {
        const pad = (n) => Number.isInteger(n) ? ('0' + n).substr(-2) : '';
        return date ? `${pad(date.day)}.${pad(date.month)}.${date.year}` : '';
    }
}
