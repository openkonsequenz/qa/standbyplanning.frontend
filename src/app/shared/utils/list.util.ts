import { FormUtil } from '@shared/utils/form.util';

/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

export const AGGRID_LOCALETEXT = {
    page: 'Seite',
    more: 'mehr',
    to: 'bis',
    of: 'von',
    next: 'nächste',
    last: 'letzte',
    first: 'erste',
    previous: 'Vorschau',
    // for set filter
    selectAll: 'Alle',
    searchOoo: 'suche...',
    blanks: 'leere',
    // for number filter
    equals: '=',
    lessThan: '<',
    greaterThan: '>',
    inRange: 'Zwischen',
    lessThanOrEqual: 'kleiner oder gleich',
    greaterThanOrEqual: 'Gr&ouml;&szlig;er oder gleich',
    filterOoo: 'filtern...',
    // for text filter
    contains: 'enth&auml;lt',
    notEqual: 'nicht gleich',
    notContains: 'enth&auml;lt nicht',
    startsWith: 'beginnt mit..',
    endsWith: 'endet mit..',
    // the header of the default group column
    group: 'Gruppierung',
    // tool panel
    columns: 'Spalten',
    pivotedColumns: 'laPivot Cols',
    pivotedColumnsEmptyMessage: 'Spalten hier hin ziehen',
    valueColumns: 'laValue Cols',
    valueColumnsEmptyMessage: 'Spalten hier hin ziehen',
    noRowsToShow: 'keine Daten vorhanden',

    pinColumn: 'Spalte fixieren',
    pinLeft: 'fixieren links',
    pinRight: 'fixieren rechts',
    noPin: 'nicht fixieren',
    autosizeThiscolumn: 'Diese Spalte automatisch anpassen',
    autosizeAllColumns: 'Alle Spalten automatisch anpassen',
    resetColumns: 'Spalten zurücksetzen',
    toolPanel: 'Werkzeugleiste',
    rowGroupColumnsEmptyMessage: 'Für Gruppierung, Spalte hier hinziehen',
    expandAll: 'Alle aufklappen',
    collapseAll: 'Alle zuklappen',
    groupBy: 'gruppieren nach',
    copy: 'Kopieren',
    copyWithHeaders: 'Kopieren mit Spaltenkopf'
};

export const DEFAULT_COL_DEF = {
    sortable: true,
    filter: true,
    resizable: true
};

export class ListUtil {
    static formatDate(params) {
        const date: Date = new Date(params.value);
        const day = FormUtil.addZero(date.getDate());
        const month = FormUtil.addZero(date.getMonth() + 1);
        const year = date.getFullYear();

        return `${day}.${month}.${year}`;
    }

    static formatDateWithTime(params) {
        const date: Date = new Date(params.value);
        const hours = FormUtil.addZero(date.getHours());
        const minutes = FormUtil.addZero(date.getMinutes());
        const seconds = FormUtil.addZero(date.getSeconds());

        return `${ListUtil.formatDate(params)} ${hours}:${minutes}:${seconds} Uhr`;
    }

    /**
     * Compare two dates and return
     *
     * Any number < 0 if the cell value is less than the filter date
     * 0 if the dates are the same
     * Any number > 0 if the cell value is greater than the filter date
     *
     * @param dateA is the date entered into the filter
     * @param dateB is the date in the cell of the column
     */
    static compareDates(dateA, dateB) {
        let dateAObj = new Date(dateA);
        dateAObj = new Date(dateAObj.getFullYear(), dateAObj.getMonth() + 1, dateAObj.getDate(), 0, 0, 0, 0);
        let dateBObj = new Date(dateB);
        dateBObj = new Date(dateBObj.getFullYear(), dateBObj.getMonth() + 1, dateBObj.getDate(), 0, 0, 0, 0);
        if (dateBObj < dateAObj) {
            return -1;
        } else if (dateBObj > dateAObj) {
            return 1;
        }
        return 0;
    }

    static joinArrayElementsToString(array: Array<any>, propertyToJoin: string) {
        return array.map(function (elem) {
            return elem[propertyToJoin];
        }).join(',');
    }

    /**
     *
     * @param objectToSearch An Object to search for in the array.
     * @param array The array to be searched in.
     */
    static getIndexOfElement(objectToSearch: Object, array: Array<any>) {
        return array.findIndex((element) => {
            for (const key in element) {
                if (element[key] === objectToSearch[key]) {
                    return true;
                }
            }
        });
    }
}
