/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReplaceDialogComponent } from './replace-dialog.component';
import { SharedModule } from '@shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { MessageService } from 'primeng/components/common/messageservice';
import { of } from 'rxjs';
import { MasterdataService } from '@masterdata/services/masterdata.service';
import { ReplaceMockObjects } from '@shared/testing/replace';

const replaceMockObjects = new ReplaceMockObjects;

export class MasterDataServiceMock {
  getStandbygroupUniqueUser() {
    return of(replaceMockObjects.USER_UNIQUE_LIST);
  }
  getUserlistDropdown() {
    return of(replaceMockObjects.USER_DROPDOWN_DATA_LIST);
  }
}

describe('ReplaceDialogComponent', () => {
  let component: ReplaceDialogComponent;
  let fixture: ComponentFixture<ReplaceDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReplaceDialogComponent],
      imports: [
        SharedModule,
        RouterTestingModule
      ],
      providers: [
        {
          provide: MasterdataService,
          useClass: MasterDataServiceMock
        },
        MessageService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplaceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('decide()', () => {
    it('should prepare the data and call decide', () => {
      component.form.patchValue({
        date: {
          validFrom: { day: 1, month: 1, year: 2018 },
          validTo: { day: 1, month: 1, year: 2018 }
        }
      });
      component.decide(true);
    });
    it('should validate form fields if decision is true and form fields are invalid', () => {
      component.form.patchValue({
        date: {
          validFrom: '',
          validTo: ''
        }
      });
      component.decide(true);
      expect(component.form.valid).toBeFalsy();
    });
  });

  describe('setDefaultDate', () => {
    it('should set validFrom to current day', () => {
      const date = new Date();
      component.setDefaultDate('validFrom');
      expect(component.form.get('date').get('validFrom').value).toEqual({
        day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear()
      });
    });

    it('should set validTo to current day + 15 years', () => {
      const date = new Date();
      component.setDefaultDate('validTo');
      expect(component.form.get('date').get('validTo').value).toEqual({
        day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear() + 15
      });
    });
  });
});
