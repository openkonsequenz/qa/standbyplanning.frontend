/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchiveDialogComponent } from './archive-dialog.component';
import { MessageService } from 'primeng/components/common/messageservice';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '@shared/shared.module';

describe('ArchiveDialogComponent', () => {
  let component: ArchiveDialogComponent;
  let fixture: ComponentFixture<ArchiveDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ArchiveDialogComponent],
      imports: [
        SharedModule,
        RouterTestingModule
      ],
      providers: [
        MessageService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchiveDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('decide()', () => {
    it('should prepare the data and call decide', () => {
      component.form.patchValue({ title: 'test', comment: 'test' });
      component.decide(true);
    });
  });
});
