/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';

import { DatepickerValidator } from '@shared/validators/datepicker.validator';
import { FormUtil } from '@shared/utils/form.util';
import { AbstractDialogComponent } from '@shared/abstract/abstract-dialog/abstract-dialog.component';

@Component({
  selector: 'ok-transfer-dialog',
  templateUrl: './transfer-dialog.component.html',
  styleUrls: ['./transfer-dialog.component.scss']
})
export class TransferDialogComponent extends AbstractDialogComponent implements OnInit {
  @Output()
  decision: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  saveButtonLabel = 'Verschieben';

  constructor(
    private fb: FormBuilder
  ) {
    super();
  }

  ngOnInit() {
    this.createForm();
    FormUtil.setInitialDate(this.form);
  }

  createForm() {
    this.form = this.fb.group({
      date: this.fb.group({
        validFrom: ['', Validators.required],
        validTo: ['', Validators.required],
      }, { validator: [DatepickerValidator.dateRangeTo('')] }),
      overwrite: false
    });
  }

  decide(decision) {
    const dataToTransmit = this.form.getRawValue();
    FormUtil.formatDates(dataToTransmit, ['validFrom', 'validTo']);
    super.decide(decision, dataToTransmit);
  }

  setDefaultDate(field: string) {
    FormUtil.setDefaultDate(this.form, field);
  }

}
