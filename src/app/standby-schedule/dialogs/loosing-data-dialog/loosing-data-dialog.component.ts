/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit } from '@angular/core';
import { AbstractDialogComponent } from '@shared/abstract/abstract-dialog/abstract-dialog.component';

@Component({
  selector: 'ok-loosing-data-dialog',
  templateUrl: './loosing-data-dialog.component.html',
  styleUrls: ['./loosing-data-dialog.component.scss']
})
export class LoosingDataDialogComponent extends AbstractDialogComponent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }
}
