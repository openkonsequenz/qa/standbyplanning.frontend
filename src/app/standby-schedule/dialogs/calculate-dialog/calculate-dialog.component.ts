/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { UserObject } from '@shared/model/UserObject';
import { FormUtil } from '@shared/utils/form.util';
import { ModalTransferObject } from '@shared/model/ModalTransferObject';
import { DatepickerValidator } from '@shared/validators/datepicker.validator';
import { AbstractDialogComponent } from '@shared/abstract/abstract-dialog/abstract-dialog.component';

@Component({
  selector: 'ok-calculate-dialog',
  templateUrl: './calculate-dialog.component.html',
  styleUrls: ['./calculate-dialog.component.scss']
})
export class CalculateDialogComponent implements OnInit {
  @Output()
  decision: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  public form: FormGroup;

  modalAction = new Subject<ModalTransferObject>();

  listOfStandbyUsers: UserObject[];

  constructor(
    private fb: FormBuilder,
    private masterDataService: MasterdataService,
    public activeModal: NgbActiveModal
  ) {
    this.createForm();
  }

  ngOnInit() {
    const standbyGroupId = this.form.getRawValue().standbyGroupId;

    if (standbyGroupId) {
      this.masterDataService.getStandbygroupUniqueUser(standbyGroupId).subscribe((userlistRes: UserObject[]) => {
        this.listOfStandbyUsers = userlistRes;
      });
    }
  }

  createForm() {
    this.form = this.fb.group({
      date: this.fb.group({
        validFrom: ['', Validators.required],
        validTo: ['', Validators.required],
      }, { validator: [DatepickerValidator.dateRangeTo('')] }),
      startIdOfUser: ['', Validators.required],
      standbyGroupId: '',
      standbyListId: ''
    });
  }

  setDefaultDate(field: string) {
    FormUtil.setDefaultDate(this.form, field);
  }

  calculate() {
    if (FormUtil.validate(this.form)) {
      this.modalAction.next({
        action: 'calculate',
        data: this.form.getRawValue()
      });
    }
  }
}
