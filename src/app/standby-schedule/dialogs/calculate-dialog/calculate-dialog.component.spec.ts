/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Mettenmeier GmbH - initial implementation
 *   Basys GmbH - automatic report generation implementation
 ********************************************************************************/

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { CalculateDialogComponent } from './calculate-dialog.component';
import { SharedModule } from '@shared/shared.module';
import { ModalTransferObject } from '@shared/model/ModalTransferObject';
import {MasterdataService} from '@masterdata/services/masterdata.service';
import {UserObject} from '@shared/model/UserObject';
import {of} from 'rxjs';

describe('CalculateDialogComponent', () => {
  let component: CalculateDialogComponent;
  let fixture: ComponentFixture<CalculateDialogComponent>;
  const user: UserObject[] = [];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CalculateDialogComponent],
      imports: [
        NgbModule.forRoot(),
        SharedModule
      ],
      providers: [
        NgbActiveModal
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculateDialogComponent);
    component = fixture.componentInstance;
    const masterService: MasterdataService = TestBed.get(MasterdataService);
    spyOn(masterService, 'getStandbygroupUniqueUser').and.returnValue(of(user));
    component.form.patchValue({ standbyGroupId: 1 });

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should send calculation data to parent component if the form is valid', () => {
    component.form.patchValue({
      date: {
        validFrom: '2023-05-08T05:05:05.789Z',
        validTo: '2023-05-08T05:05:05.789Z'
      }
    });
    component.calculate();
    component.modalAction.subscribe((transferData: ModalTransferObject) => {
      expect(transferData.action).toBe('calculate');
    });
  });

  describe('setDefaultDate', () => {
    it('should set validFrom to current day', () => {
      const date = new Date();
      component.setDefaultDate('validFrom');
      expect(component.form.get('date').get('validFrom').value).toEqual({
        day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear()
      });
    });

    it('should set validTo to current day + 15 years', () => {
      const date = new Date();
      component.setDefaultDate('validTo');
      expect(component.form.get('date').get('validTo').value).toEqual({
        day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear() + 15
      });
    });
  });

  describe('calculate()', () => {
    it('should calculate if form is valid', () => {
      component.setDefaultDate('validFrom');
      component.setDefaultDate('validTo');
      component.form.patchValue({ startIdOfUser: 1 });
      component.calculate();
    });

    it('shouldn´t calculate if form is invalid', () => {
      component.form.patchValue({ startIdOfUser: '' });
      component.calculate();
    });
  });
});
