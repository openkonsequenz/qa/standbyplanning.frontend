/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, Injector } from '@angular/core';
import { AbstractFormComponent } from '@shared/abstract/abstract-form/abstract-form.component';
import { Subscription } from 'rxjs';
import { Params } from '@angular/router';
import { PlanningService } from '@schedule/services/planning.service';
import { PlanBodyObject } from '@shared/model/PlanBodyObject';
import { ProtocolDialogComponent } from '@schedule/dialogs/protocol-dialog/protocol-dialog.component';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { LoosingDataDialogComponent } from '@schedule/dialogs/loosing-data-dialog/loosing-data-dialog.component';

@Component({
  selector: 'ok-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.scss']
})
export class ArchiveComponent extends AbstractFormComponent implements OnInit {
  plan: PlanBodyObject;
  param$: Subscription;
  archive$: Subscription;
  id: number;

  modalProtocolRef: NgbModalRef;
  archiveModalWarningRef: NgbModalRef;
  constructor(
    private planningService: PlanningService,
    private injector: Injector
  ) {
    super(injector);
  }

  ngOnInit() {
    this.createForm();
    this.param$ = this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      if (this.id) {
        this.archive$ = this.planningService.getArchive(this.id).subscribe(
          data => {
            this.plan = JSON.parse(data.jsonPlan);
            data.modificationDate = this.ngbDateParserFormatter.parse(data.modificationDate);
            data.date = {
              validFrom: this.ngbDateParserFormatter.parse(data.validFrom),
              validTo: this.ngbDateParserFormatter.parse(data.validTo)
            };
            this.form.patchValue(data);
          }
        );
      }
    });
  }

  createForm() {
    this.form = this.fb.group({
      id: '',
      statusName: { value: '', disabled: true },
      comment: { value: '', disabled: true },
      title: { value: '', disabled: true },
      modificationDate: { value: '', disabled: true },
      modifiedBy: { value: '', disabled: true },
      modifiedCause: { value: '', disabled: true },
      date: this.fb.group({
        validFrom: { value: '', disabled: true },
        validTo: { value: '', disabled: true }
      })
    });
  }

  importIntoPlanning() {
    this.archiveModalWarningRef = this.modalService.open(LoosingDataDialogComponent);
    this.archiveModalWarningRef.componentInstance.decision.subscribe(resUserModal => {
      if (resUserModal) {
        this.planningService.importIntoPlanning(this.id).subscribe((importPlanningRes) => {
          this.modalProtocolRef = this.modalService.open(ProtocolDialogComponent, { windowClass: 'modal-xl', backdrop: 'static' });
          this.modalProtocolRef.componentInstance.protocol = importPlanningRes;
        });
      }
      this.archiveModalWarningRef.close();
    });
  }

}
