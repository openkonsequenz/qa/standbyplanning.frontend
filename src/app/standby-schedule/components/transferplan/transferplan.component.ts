/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { PlanningService } from '@schedule/services/planning.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TransferDialogComponent } from '@schedule/dialogs/transfer-dialog/transfer-dialog.component';
import { ProtocolDialogComponent } from '@schedule/dialogs/protocol-dialog/protocol-dialog.component';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'ok-transferplan',
  templateUrl: './transferplan.component.html',
  styleUrls: ['./transferplan.component.scss']
})
export class TransferplanComponent implements OnInit {
  /**
  * Bodies
  */
  sourceBodies: Array<any> = [];
  targetBodies: Array<any> = [];

  sourceColumnDefsBodies = [];
  targetColumnDefsBodies = [];
  bodie$: Subscription;
  bodyModalRef: NgbModalRef;

  columnDefsBodies = [
    { headerName: 'Gruppenname', field: 'groupName' },
    { headerName: 'Listenname', field: 'listName' }
  ];

  modalProtocolRef: NgbModalRef;
  constructor(
    private masterDataService: MasterdataService,
    private planningService: PlanningService,
    private modalService: NgbModal,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.getBodies();
  }

  getBodies() {
    this.bodie$ = this.masterDataService.getStandbygroupsAndLists().subscribe((resBodies) => {
      this.sourceBodies = resBodies;
      this.targetBodies = resBodies;
    });
  }

  /**
   * MOVE FUNCTIONS USER
   */

  moveBodies(dataToMove, status: number) {
    this.bodyModalRef = this.modalService.open(TransferDialogComponent, { size: 'lg' });
    this.bodyModalRef.componentInstance.decision.subscribe(resBodyModalData => {
      if (resBodyModalData) {
        resBodyModalData.lsTransferGroup = dataToMove;
        resBodyModalData.statusId = status;
        this.planningService.transferBodies(resBodyModalData).subscribe((resProtocoll) => {
          this.modalProtocolRef = this.modalService.open(ProtocolDialogComponent, { windowClass: 'modal-xl', backdrop: 'static' });
          this.modalProtocolRef.componentInstance.protocol = resProtocoll;

          this.messageService.add({ severity: 'success', summary: 'Daten wurden gespeichert', detail: '' });
        });
      }
      this.bodyModalRef.close();
    });
  }
}
