/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy, Injector } from '@angular/core';

import { AGGRID_LOCALETEXT, ListUtil } from '@shared/utils/list.util';
import { Subscription } from 'rxjs';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PlanningService } from '@schedule/services/planning.service';
import { Router } from '@angular/router';
import { ArchiveObject } from '@shared/model/ArchiveObject';
import { Validators } from '@angular/forms';
import { AbstractFormComponent } from '@shared/abstract/abstract-form/abstract-form.component';
import { FormUtil } from '@shared/utils/form.util';

@Component({
  selector: 'ok-archivelist',
  templateUrl: './archivelist.component.html',
  styleUrls: ['./archivelist.component.scss']
})
export class ArchivelistComponent extends AbstractFormComponent implements OnInit, OnDestroy {
  columnDefs = [
    { headerName: 'Ebene', field: 'statusName' },
    { headerName: 'Titel', field: 'title' },
    { headerName: 'Kommentar', field: 'comment' },
    { headerName: 'Bearbeiter', field: 'modifiedBy' },
    {
      headerName: 'Startdatum', field: 'validFrom', filter: 'agDateColumnFilter', valueFormatter: ListUtil.formatDate,
      filterParams: {
        comparator: ListUtil.compareDates
      }
    },
    {
      headerName: 'Enddatum', field: 'validTo', filter: 'agDateColumnFilter', valueFormatter: ListUtil.formatDate,
      filterParams: {
        comparator: ListUtil.compareDates
      }
    },
    { headerName: 'Bearbeitungsgrund', field: 'modifiedCause' },
    {
      headerName: 'Änderungsdatum', field: 'modificationDate', filter: 'agDateColumnFilter', valueFormatter: ListUtil.formatDateWithTime,
      filterParams: {
        comparator: ListUtil.compareDates
      }
    }
  ];
  localeText = AGGRID_LOCALETEXT;

  rowData = [];
  archiveData$: Subscription;
  modalRef: NgbModalRef;
  formUtil = FormUtil;

  constructor(
    private planningService: PlanningService,
    private injector: Injector
  ) {
    super(injector);
  }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      date: this.fb.group({
        dateIndex: ['', Validators.required]
      })
    });
  }

  onGridReady(params) {
    this.getArchiveData();
    params.api.sizeColumnsToFit();
  }

  getArchiveData() {
    this.archiveData$ = this.planningService.getArchiveData().subscribe((res: ArchiveObject[]) => {
      this.rowData = res;
    });
  }

  rowClicked(event) {
    this.router.navigate(['planung/archiv', event.data.id]);
  }

  search() {
    if (FormUtil.validate(this.form)) {
      const formValue = this.form.getRawValue();
      FormUtil.formatDates(formValue, ['dateIndex']);
      this.planningService.searchArchiveData(formValue).subscribe((searchRes) => {
        this.rowData = searchRes;
      });
    }
  }

  clear() {
    this.form.reset();
    this.getArchiveData();
  }

  ngOnDestroy() {
    if (this.archiveData$) {
      this.archiveData$.unsubscribe();
    }
  }

}
