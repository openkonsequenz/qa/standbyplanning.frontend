/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { StandbyScheduleModule } from './standby-schedule.module';

describe('StandbyScheduleModule', () => {
  let standbyScheduleModule: StandbyScheduleModule;

  beforeEach(() => {
    standbyScheduleModule = new StandbyScheduleModule();
  });

  it('should create an instance', () => {
    expect(standbyScheduleModule).toBeTruthy();
  });
});
