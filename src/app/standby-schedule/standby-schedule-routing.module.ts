/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OverviewComponent } from '@schedule/components/overview/overview.component';
import { RoleGuard } from '@core/guards/role.guard';
import { AuthGuard } from '@core/guards/auth.guard';
import { PlanninglistComponent } from '@schedule/components/planninglist/planninglist.component';
import { TransferplanComponent } from '@schedule/components/transferplan/transferplan.component';
import { ArchivelistComponent } from '@schedule/components/archivelist/archivelist.component';
import { ArchiveComponent } from '@schedule/components/archive/archive.component';
import { CanDeactivateGuard } from '@core/guards/can-deactivate.guard';

const routes: Routes = [
  {
    path: 'planung',
    component: OverviewComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin', 'BP_Gruppenleiter']
    }
  }, {
    path: 'planung/planebene',
    component: PlanninglistComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin']
    }
  }, {
    path: 'planung/transfer',
    component: TransferplanComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin']
    }
  }, {
    path: 'planung/istebene',
    component: PlanninglistComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin', 'BP_Gruppenleiter']
    }
  }, {
    path: 'planung/archiv',
    component: ArchivelistComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin']
    }
  },
  {
    path: 'planung/archiv/:id',
    component: ArchiveComponent,
    canDeactivate: [CanDeactivateGuard],
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['BP_Sachbearbeiter', 'BP_Admin']
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StandbyScheduleRoutingModule { }
